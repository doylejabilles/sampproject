<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'maps');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '?iFtXut}+@ry|wZJwY~|CT`IM m{iGn>x >%!e=z3bIzs{yl2~<O@Qt_udaNZt:-');
define('SECURE_AUTH_KEY',  '98Y ]gg{7kWsWXLYe<Y-..UpMpi7WdQbr(gJ1;S5MaA,0FQv4L)Du6#sNs<0]#.[');
define('LOGGED_IN_KEY',    'S:0i?a*fI/l 4?`A)h#4aPnDW <T};)1ohLN5};^g@.R4m0qh0+IN=Yo*=@^,[-,');
define('NONCE_KEY',        '}I+~-J1lZi)Z)jNyFF=tiKG^-XR7+t;qsfg>ml8?ECx^ #eM+)vH>JJ-5uR8XMdP');
define('AUTH_SALT',        'NgbuDu@B=oq,%B8Vb6Ne%bpCQk0W3PUq)6EI<VL&0x1slIlfW%W9!GAm%L-;MQCQ');
define('SECURE_AUTH_SALT', 'Av5O7sbb+o0|OJ8&4yg/:*@_gb|3{=Vs2g9s+jc]v*,t70zu24*<+~(w|pVF!,>s');
define('LOGGED_IN_SALT',   'R2vl4G.hx3-hmJY:|*mF+Labvf]K-Q`=YH7fHs0|Jra6ndB9KsG.aV1thp>I=+-6');
define('NONCE_SALT',       '?et>JR]t!?k/|`B,|p{CNl|;$<bnE_|_:4`qiL|Wu}X0a0dr3^j*r^9u@=NS9waB');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'marketingadmissions_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
