<?php
	@session_start();
	get_includes('head');
	get_includes('header');
	get_includes('nav');
	if(is_front_page()){  get_includes('banner'); }
?>

	<section class="grid container clearfix">

		<div class="page-content float-right align-justify">
			<h1 class="entry-title"><?php _e( 'Not Found', 'twentyten' ); ?></h1>
			<div class="entry-content">
				<p><?php _e( 'Apologies, but the page you requested could not be found. Perhaps searching will help.', 'twentyten' ); ?></p>
				<?php get_search_form(); ?>
			</div><!-- .entry-content -->
			
			<script type="text/javascript">
				// focus on search field after it has loaded
				document.getElementById('s') && document.getElementById('s').focus();
			</script>
		</div>
		
		<?php get_includes('sidebar'); ?>	
		  
	</section>

<?php get_includes('footer'); ?>