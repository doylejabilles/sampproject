<?php
	@session_start();
	get_includes('head');
	get_includes('header');
	get_includes('nav');
	if(is_front_page()){  get_includes('banner'); }
?>

	<section class="grid container clearfix">

		<div class="page-content float-right align-justify">

			<h1 class="page-title"><?php
					printf( __( 'Category Archives: %s', 'twentyten' ), '<span>' . single_cat_title( '', false ) . '</span>' );
				?></h1>
				<?php
					$category_description = category_description();
					if ( ! empty( $category_description ) )
						echo '<div class="archive-meta">' . $category_description . '</div>';

				/* Run the loop for the category page to output the posts.
				 * If you want to overload this in a child theme then include a file
				 * called loop-category.php and that will be used instead.
				 */
				get_template_part( 'loop', 'category' );
				?>
		</div>
		
		<?php get_includes('sidebar'); ?>	
		  
	</section>

<?php get_includes('footer'); ?>