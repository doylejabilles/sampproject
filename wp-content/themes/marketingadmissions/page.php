<?php
	@session_start();
	get_includes('head');
	get_includes('header');
	get_includes('nav');
	if(is_front_page()){  get_includes('banner'); }
?>

	<section class="grid container clearfix">

		<div class="page-content float-right align-justify">
			<?php get_template_part( 'loop', 'page' );
			if(is_front_page()){  get_includes('bottom'); } ?>	
		</div>
		
		<?php get_includes('sidebar'); ?>	
		  
	</section>

<?php get_includes('footer'); ?>