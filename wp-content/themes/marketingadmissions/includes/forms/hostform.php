<?php
	get_common(array('config', 'Form', 'DB', 'Session'));
	get_model(array('Company', 'CompanyDAO', 'Member', 'MemberDAO', 'Admin'));

	
	$errors = array();
	$error = '';
	$success = '';
	
	$regexp = "/^[a-z0-9]+([_\\.-][a-z0-9]+)*@([a-z0-9]+([\.-][a-z0-9]+)*)+\\.[a-z]{2,}$/i";
	
	// if($smtp) {
		// // include 'wp-content/themes/marketingadmissions/forms/form_details.php';
		// // include 'wp-content/themes/marketingadmissions/forms/phpmailer/sendmail.php';
	// }
	
	
?>

<style type="style/css">
#result { background-color:#fff ; width: 613px; margin-bottom: 10px; font-size: 25px; font-family: arial; color: #1E90FF; font-style: italic; border-radius: 3px; border:solid 2px #fff; }
.member-list-content { color: #084D52 }
.member-list-content h1{ color: #084D52 }
.member-list-content a{ color: #084D52 }
.member-list-content label{ color: #084D52 }
.member-list-content a:hover{ color: #1AB7DF }
</style>
 
<!-- Modal -->

<div class="modal fade member-list-content" id="beahost" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><strong>Register as Host</strong></h4>
      </div>
      <div class="modal-body">
		<div class="row clearfix" style="margin: 0px; padding: 0px;">
			<!--<div id="result"><?php //echo (!empty($error)) ? Form::error($error) : ''; echo (!empty($success)) ? Form::success($success) : ''; ?></div>-->
			<div class="alert alert-info col-md-12">
				<form role="form" method="POST" id="host">
					<input type="hidden" name="bloginfo" value="<?php echo bloginfo('template_url'); ?>">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label><span class="required">*</span>Company</label>
								<input type="text" class="form-control validate[required]" name="Company">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label><span class="required">*</span>Address</label>
								<input type="text" class="form-control validate[required]" name="Address">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label><span class="required">*</span>Phone</label>				
								<input type="text" class="form-control validate[required]" name="Phone">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label><span class="required">*</span>Fax</label>
								<input type="text" class="form-control validate[required]" name="Fax">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label><span class="required">*</span>Email</label>
								<input type="text" class="form-control validate[required,custom[email]]" name="Email">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label><span class="required">*</span>Contact Person</label>
								<input type="text" class="form-control validate[required]" Name="Contact Person">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label><span class="required">*</span>Mobile</label>
								<input type="text" class="form-control validate[required]" name="Mobile">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="control-label">
							<label>Is parking available?</label><br/>
								<input type="radio" name="parking" class="parking-button" value="yes">Yes<br/>
								<input type="radio" name="parking" class="parking-button" value="no">No
							</div>
						</div>
						<div class="form-group">
							<label>Parking instructions</label>
							<textarea type="text" row="10" name="Parking instructions" id="parking-textarea" class="form-control" style="display:none"></textarea>
						</div>
					</div>
					<div class="form-group" style="text-align: center;">
						<button type="submit" class="btn btn-default" name="host-btn">Submit</button>
					</div>
				</form>
			</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" id="exit-btn" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>






