			<footer class="page-footer">
				<div class="container clearfix">
					
					<div class="nav-footer">
						<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary', 'link_before' => '', 'link_after' => '' ) ); ?>

					</div>					
					
					<div id="copyright" class="align-center">
							&copy; Copyright
							<?php
								$start_year = '2014';
								$current_year = date('Y');
								$copyright = ($current_year == $start_year) ? $start_year : $start_year.' - '.$current_year;
								echo $copyright;
							?>
							&nbsp;&bull;&nbsp; 
							Marketing &amp; Admissions Professionals for Seniors
							&nbsp;&bull;&nbsp;
							Non Profit Professional Organization : <a href="http://yourity.com" target="_blank">Yourity.com</a><br/>
							<div style="text-align: center">Tax ID: 46-2296132&nbsp;&bull;&nbsp;Total Visitors Today: <script language="Javascript" src="<?php bloginfo('template_url'); ?>/counter.php?page=counter" type="" ></script></div>
					</div>

				</div> 
			</footer>
		
		</div> <!-- End Protect Me -->
		
		<?php include "ie.php" ?>
		
		<!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->
		<script type="text/javascript">
			//<![CDATA[
			window.jQuery || document.write('<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/vendor/jquery-v1.10.2.min.js"><\/script>')
			//]]>
		</script>
		
		<!--
		Solved HTML5 & CSS IE Issues
		-->
	    <script src="<?php bloginfo('template_url'); ?>/js/vendor/bootstrap.min.js"></script>
	    <script src="<?php bloginfo('template_url'); ?>/js/vendor/modernizr-custom-v2.7.1.min.js"></script>
        
		<!--
		Dang Iframe
		-->
	    <script src="<?php bloginfo('template_url'); ?>/js/vendor/calcheight.js"></script>
        
		
		<!--
		Solved Psuedo Elements IE Issues
		-->
		<script src="<?php bloginfo('template_url'); ?>/js/vendor/selectivizr-min.js"></script>

		<!--
		Alternative Responsive Slider
		-->
		<script src="<?php bloginfo('template_url'); ?>/js/vendor/responsiveslides.min.js"></script>
		
		<!--
		All Actions Events
		-->
		<script src="<?php bloginfo('template_url'); ?>/js/plugins.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/main.js"></script>
		
		<?php wp_footer(); ?>
    </body>
</html>