
<?php
	
	get_common(array('config', 'Form', 'DB', 'Session'));
	get_model(array('Event', 'EventDAO', 'CompanyDAO', 'Company', 'Admin'));
	
	$e_var = new Event();
	$e_dao = new EventDAO();
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	$a_var = new Admin();
	$events = $c_dao->getEvent('event', '', 'id', 'DESC', '', 0, 3);
	
?>

<div class="page-banner container clearfix">
	
	<ul class="rslides float-right">
		<li><img src="<?php bloginfo('template_url'); ?>/images/slides/1.png" width="657" alt="Slides"/></li>
		<li><img src="<?php bloginfo('template_url'); ?>/images/slides/2.png" width="657" alt="Slides"/></li>
		<li><img src="<?php bloginfo('template_url'); ?>/images/slides/3.png" width="657" alt="Slides"/></li>
		<li><img src="<?php bloginfo('template_url'); ?>/images/slides/4.png" width="657" alt="Slides"/></li>
		<li><img src="<?php bloginfo('template_url'); ?>/images/slides/5.png" width="657" alt="Slides"/></li>
		<li><img src="<?php bloginfo('template_url'); ?>/images/slides/6.png" width="657" alt="Slides"/></li>
		<li><img src="<?php bloginfo('template_url'); ?>/images/slides/7.png" width="657" alt="Slides"/></li>
	</ul>
	
	<div class="upcoming-events float-left">
		<h2>Upcoming Events</h2>
		<div class="date-container">
			<div class="date">
			<?php foreach ($events as $e ):
				if ( strlen($e->event_name)>40 ) {
					$t_event = substr($e->event_name, 0, 40);
				} else {
					$t_event = $e->event_name;
				}
			?>
				<div class="upcoming-events-calendar-container" style="width:185px; height: 60px;">
					<div class="calendar float-left">
						<span><?php echo date("M", strtotime($e->event_date)); ?></span>
						<strong><?php echo date("d", strtotime($e->event_date)); ?></strong>
					</div>
					<div class="upcoming-events-content">
						<p><a href="getid?id=<?php echo $e->id;?>" class="get_event_id" id="<?php echo $e->id;?>" style="text-decoration:none; font-size: 13px; font-weight: bold;" > <p style="color:#084D52;"><?php echo $t_event; ?> </p></a>
					</div>
				</div>
			<?php endforeach; ?>	
			</div>
			<div class="clearfix" style="font: bold 20px 'CenturyGothic-Bold'; color: #000; text-align: center; display: block; margin-top: 10px;">
				<a href="charity-organization-event-registration">View Events &raquo;</a>
			</div>
		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade bs-example-modal-lg" id="getid" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content alert-info">
				<div class="modal-header">
					<button class="close close_btn" data-dismiss="modal" type="button">
						<span aria-hidden="true">X</span>
						<span class="sr-only">Close</span>
					</button>
				</div>
				<div class="modal-body" id="modal_content_style">
					<h1 class="event_name"></h1>
					<span style="font-weight: bold; color: #067B3D;">Where:</span>&nbsp;&nbsp;<div class="event_location" style="color:#084D52"></div>
					<span style="font-weight: bold; color: #067B3D;">Sponsor(s):</span>&nbsp;&nbsp;<div class="sponsor" style="color:#084D52"></div>
					<span style="font-weight: bold; color: #067B3D;">When:</span>&nbsp;&nbsp;<div class="event_date" style="color:#084D52"></div>
					<span style="font-weight: bold; color: #067B3D;">Time:</span>&nbsp;&nbsp;<div class="event_time" style="color:#084D52"></div>
					<span style="font-weight: bold; color: #067B3D;">Cost:</span>&nbsp;&nbsp;<div class="price" style="color:#084D52"></div>
					<span style="font-weight: bold; color: #067B3D;">Contact:</span>&nbsp;&nbsp;<div class="event_contact" style="color:#084D52"></div>
				</div>	
			</div>
		</div>
	</div>
	
</div>

