<aside class="page-sidebar float-left">

	<?php
	
		include 'wp-content/themes/marketingadmissions/includes/forms/hostform.php';
		
	?>
	
	<div class="row members">
		<h2>Featured Members</h2>
		
		<div class="member align-top inline-block">
			<a href="charity-organization-featured-member"><img src="<?php bloginfo('template_url'); ?>/images/members/member-1.jpg" width="85" alt="Members"/>
			<span>The Oaks of Pasadena</span></a>
		</div>
		
		<div class="member align-top inline-block">
			<a href="http://www.thebegroup.org/communities/royal-oaks"><img src="<?php bloginfo('template_url'); ?>/images/members/logo-be.jpg" width="85" alt="Members"/>
			<span>Royal <br/> Oaks</span></a>
		</div>
		
		<div class="member align-middle inline-block" style="margin-top:38px;">
			<a href="http://alliancenursingrehab.com/welcome.php"><img src="<?php bloginfo('template_url'); ?>/images/members/logo-alliance.jpg" width="170" alt="Members"/>
			<span>Alliance Nursing and Rehabilitation</span></a>
		</div>
		
		<div class="member align-middle inline-block" style="margin-top:35px;">
			<a href="http://silveradocare.com/silverado-locations/california/alhambra/the-huntington"><img src="<?php bloginfo('template_url'); ?>/images/members/logo-silverado.jpg" width="170" alt="Members"/>
			<span>Silverado</span></a>	
		</div>
		
		<div>
		<button class="btn btn-default btn-sm" data-toggle="modal" data-target="#beahost" style="margin-top: 15px">Be a Host</button>
		</div>
	</div>
	
	<!--<div class="be-a-host">
		<button class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Be a Host</button>
		
	</div>-->
	
	<div class="row contact-info">
		<h2>Contact Information</h2>
		<?php dynamic_sidebar("contact-info") ?>
		
	</div>
	
	<div class="row tabs">
		<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'secondary', 'link_before' => '', 'link_after' => '' ) ); ?>
		
	</div>
	
	<!--<div class="tax_id">
		Tax ID <span>46-2296132</span>
	</div>-->
	
	<!--<div align="center" style="padding-top:5px; padding-bottom:10px;">				
		<div style="text-align:center; font-family:Arial, Trebuchet MS,'Times New Roman', Times, serif; font-size:14px; color:#000; margin:0; border:1px solid #369CFF; padding:0; line-height:normal; font-weight:bold; border-radius: 3px;">TOTAL VISITORS TODAY:  <span style="color:#369CFF; font-weight:bold;"><script language="Javascript" src="<?php //bloginfo('template_url'); ?>/counter.php?page=counter" type="" ></script></span>
		</div>
	</div>-->
	
	
</aside>