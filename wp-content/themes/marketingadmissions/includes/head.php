<?php
	// backend purposes	
	$active = ( isset($_SESSION['ACTIVE']) && ($_SESSION['ACTIVE'] == 1) ) ? true : false;
	$type = ( isset($_SESSION['TYPE']) && ($_SESSION['TYPE'] == "member") ) ? 'member' : 'admin';
	
	if(is_page('charity-organization-login')) {
		if($active == true && $type == 'member' ) {
			echo "<script>window.parent.location = 'charity-organization-my-profile'</script>";
		}
		elseif($active == true && $type == 'admin' ) {
			echo "<script>window.parent.location = 'charity-organization-admin'</script>";
		}
	}
	if(is_page('charity-organization-admin')) {
		if($active == true && $type == 'member' ) {
			echo "<script>window.parent.location = 'charity-organization-my-profile'</script>";
		}
	}	
	if(is_page('charity-organization-my-profile') || is_page('charity-organization-admin')) {
		if($active != true) {
			echo "<script>window.parent.location = 'charity-organization-login'</script>";
		}
	}
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<!-- Le Meta Config -->
		<meta charset="utf-8">
        <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<title>Marketing &amp; Admissions Professionals for Seniors</title>
		
		<meta name="description" content="Meta Description Goes Here">
		<meta name="keywords" content="Meta Keywords Goes Here">
		<meta name="author" content="Author Goes Here">
		
		<!-- Le Favicons -->
		<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.ico">
		<link rel="apple-touch-icon" href="<?php bloginfo('template_url'); ?>/apple-touch-icon-iphone-precomposed.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_url'); ?>/apple-touch-icon-72x72-precomposed.png" />
		<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_url'); ?>/apple-touch-icon-114x114-precomposed.png" />
		<link rel="apple-touch-icon-precomposed" href="<?php bloginfo('template_url'); ?>/apple-touch-icon-iphone-precomposed.png"/>

		<link href="<?php bloginfo('template_url'); ?>/systemapplication/bootstrap3.1.1/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php bloginfo('template_url'); ?>/systemapplication/bootstrap3.1.1/css/bootstrap-theme.css" rel="stylesheet">
		
		<!-- Le Assets -->
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/normalize.css">
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/helper.css">		 		 
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/font.face.css">		 		 
	
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css">
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/media.css">
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/systemapplication/css/mycss.css">
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/datepicker.css">
		
		<!-- jQuery library -->
		<script src="<?php bloginfo('template_url'); ?>/js/jQuery.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/datepicker.js"></script>
		
		<!-- jQuery validationEngine -->
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/validationEngine.jquery.css">
		<script src="<?php bloginfo('template_url'); ?>/js/jquery.validationEngine.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/jquery.validationEngine-en.js"></script>
		
		<!-- 
		Le Font Awesome Icons 
		http://www.fortawesome.github.io/Font-Awesome/ 
		-->
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/font-awesome.min.css">		 	
		
		
		<!-- 
		Alternative Responsive Slider 
		-->		
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/rslides.css">	
		
		<!--[if IE]>
		 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		 <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<![endif]-->
		
		<?php wp_head(); ?>
	</head>
	
	<body id="maincontainer">
	 <div class="protect-me">