
<div class="nav-container container">
	<a class="nav-toggle-button "><i class="fa fa-navicon fa-fw">&nbsp;</i> Menu</a>

	<nav class="page-nav clear dropdown">
		<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary', 'after' => '<span><i class="fa fa-2x">&nbsp;&nbsp;&nbsp;&nbsp;</i></span>' ) );  ?>
	</nav>
</div> 