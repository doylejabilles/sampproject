<header class="page-header clearfix">
	<div class="container clearfix">
		<div class="logo-wrapper" style="text-align:center;">
			<a href="index.php" class="cb-logo">
				<img src="<?php bloginfo('template_url'); ?>/images/logo.png" width="700" alt="Marketing & Admissions Professionals for Seniors"/>
			</a>
		</div>
		
		<!--<div class="aside float-left">
			Marketing &amp; Admissions Professionals <br/>for Seniors
		</div>-->
		
		<?php if(!is_page('charity-organization-members-directory')): ?>
		<!--<div class="searchbox">
			<form method="POST" action="charity-organization-members-directory">
				<label class="form-label">Search </label>
				<input class="validate[required] text-input form-input" type="text" id="searchval" name="searchval" placeholder="Type Company or Contact Person">
				<input class="form-btn" type="submit" name="Submit" value="Search">
			</form>
		</div>-->
		<?php endif; ?>
		
		<!--<div class="social">
			<a href="http://facebook.com" target="_blank">
				<img src="<?php //bloginfo('template_url'); ?>/images/icon-fb.png" alt="Social"/>
			</a>
			
			<a href="http://twitter.com" target="_blank">
				<img src="<?php //bloginfo('template_url'); ?>/images/icon-tw.png" alt="Social"/>
			</a>
		</div>-->
		
	</div>
</header>