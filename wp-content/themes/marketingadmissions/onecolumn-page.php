<?php
	@session_start();
	get_includes('head');
	get_includes('header');
	get_includes('nav');
	if(is_front_page()){  get_includes('banner'); }
?>

	<section class="grid container clearfix">

		<div class="page-content float-right align-justify">

			<?php
			/* Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			 get_template_part( 'loop', 'page' );
			?>
		</div>
		
		<?php get_includes('sidebar'); ?>	
		  
	</section>

<?php get_includes('footer'); ?>