$(document).ready(function() {
	
	/* jQuery validation  */
	$("#search-members-form, #search-members-form-admin, #login-form, #registration-form, #myprofile, #host, #eventregistration, #mailing_search_form, #edit_admin_info_form, #update_status_form, #update_status_form2").validationEngine();
	$("#search-members-form, #search-members-form-admin, #login-form, #registration-form, #myprofile, #host, #eventregistration, #mailing_search_form, #edit_admin_info_form, #update_status_form, #update_status_form2").bind("jqv.field.result", function(event, field, errorFound, prompText){ console.log(errorFound) })
	
	/* ajax calls */
	$("#search-members-form").submit(function(e) {
		e.preventDefault();
		var searchval = $("#searchval").val();
		var blogInfo = $(this).attr("blog-info");
		if(searchval!=''){
			var url = $(this).attr("url");
			var formdata = $(this).serialize();
			$.ajax({
				type: "POST",
				url: url,
				data: formdata,
				beforeSend: function () {
					$(".member-list").html('<img src="'+blogInfo+'/images/loader.gif">');
				},
				success: function(result) {
					$(".member-list").html(result);
				}
			});
		}
		return false;
    });
	
	//autocomplete 
	//tiwasunon
	// $('.autocomplete').autocomplete({
		// source: function(){
			// $.ajax {(
				// dataType: 'JSON',
				// $.get('wp-content/themes/marketingadmissions/systemapplication/ajaxautocomplete.php', function(results){
					
				// });
			// });
		// }
	// });
	
	/* ajaxadminmembersearch.php */
	
	$("#search-members-form-admin").submit(function(e) {
		e.preventDefault();
		var search_val = $("#search_val").val();
		var bloginfo = $(this).attr("blog-info");
		if(search_val!='') {
			var url = $(this).attr('url');
			var formdata = $(this).serialize();
			$.ajax({
				type: "POST",
				url: url,
				data: formdata,
				beforeSend: function() {
					$(".admin_member_list").html('<img src="'+bloginfo+'/images/loader.gif">');
				},
				success: function(result) {
					$(".admin_member_list").html(result);
					$(".admin_member_list").fadeIn();
				}
			});
		}
		return false;
	});
	
	$("#mailing_search_form").submit(function(e) {
		e.preventDefault();
		var searchmail = $("#searchmail").val();
		var blogInfo = $(this).attr("blog-info");
		if(searchmail!=''){
			var url = $(this).attr("url");
			var formdata = $(this).serialize();
			$.ajax({
				type: "POST",
				url: url,
				data: formdata,
				beforeSend: function () {
					$(".mailing-list").html('<img src="'+blogInfo+'/images/loader.gif">');
				},
				success: function(result) {
					$(".mailing-list").html(result);
					$(".mailing-list").fadeIn();
				}
			});
		}
		return false;
    });
	
	
	$("#checkme").on('change', function() {		
		if($(this).is(':checked'))
			$("#registrationSubmit").removeAttr('disabled');
		else
			$("#registrationSubmit").attr('disabled', 'disabled');
	});
	
	$("#checkme").on('change', function(){
		if($(this).is(":checked")) {
			$("#continue-submit").removeAttr("disabled");
			$("#registration-submit").removeAttr("disabled");
		}else{
			$("#continue-submit").attr("disabled", "disabled");
			$("#registration-submit").attr("disabled", "disabled");
		}
	})
	
	// $("input[name=optionsRadios]:radio").on('change', function(){
		// var paymentoption = $(this).val();
		// if(paymentoption=="Credit_Card"){
			// $("#continue-submit").show();
			// $("#registration-submit").hide();
		// }else{
			// $("#continue-submit").hide();
			// $("#registration-submit").show();
		// }
	// });
	
	$('#payment_status_value').change(function(){
		var a = $(this).val();
		
		if( a == "Paid" ) {
			$('#paypalImage').show();
			$('#trans_nu').show();
		} else {
			$('#paypalImage').hide();
			$('#trans_nu').hide();
		}
		
	});
	
	$(".parking-button").on('change', function() {
		var value = $(this).val();
		if(value == "yes") {
			$("#parking-textarea").show();
		} else {
			$("#parking-textarea").hide();
		}
	});
	
	$(".deleteinfo").on('click', function(e) {
		$(this).parent().parent().addClass("delete-this");
		e.preventDefault();
		var id = $(this).attr('id');
		var url = $(this).attr("href");
		var blogInfo = $(this).attr("blog-info");
		if(id > 0) {
			if(confirm("Are you sure you want to delete?"))
			{
				$.ajax({
					type: "POST",
					url: url,
					data: "id=" + id + "&action=delete",
					beforeSend: function () {
						$("#notify").html('<img src="'+blogInfo+'/images/loader.gif">');
					},
					success: function(result) {
						$("#notify").html(result);
						$(".delete-this").remove();
						$(".delete-this").removeClass(".delete-this");
					}
				});
			}
		}
	}); 
	//
	//$(".delete_resource").on('click', function(e) {
	// $(".resources_list").on('click', ".delete_resource", function(e) {
	$(".delete_resource").on('click', function(e) {
		$(this).parent().addClass("delete-this");
		e.preventDefault();
		var id = $(this).attr('id');
		var url = $(this).attr("href");
		var blogInfo = $(this).attr("blog-info");
		if(id > 0) {
			if(confirm("Are you sure you want to delete?"))
			{
				$.ajax({
					type: "POST",
					url: url,
					data: "id=" + id + "&action=delete",
					beforeSend: function () {
						$("#notify").html('<img src="'+blogInfo+'/images/loader.gif">');
					},
					success: function(result) {
						$("#notify").html(result);
						$(".delete-this").remove();
						$(".delete-this").removeClass(".delete-this");
					}
				});
			}
		}
	});
	
	// $(".deletemember").on('click', function(e) {
	$(".admin_member_list").on("click", ".deletemember", function(e){
		$(this).parent().parent().addClass("delete-this");
		e.preventDefault();
		var id = $(this).attr('id');
		/*var url = $(this).attr("href");*/
		if(id > 0) {
			if(confirm("Are you sure you want to delete?"))
			{
				$.ajax({
					type: "POST",
					url: "wp-content/themes/marketingadmissions/systemapplication/ajaxadmin.php",
					data: "id=" + id + "&action=delete",
					beforeSend: function () {
						$("#notify4").html('<img src="wp-content/themes/marketingadmissions/images/loader.gif">');
					},
					success: function(result) {
						$("#notify4").html(result);
						$(".delete-this").remove();
						$(".delete-this").removeClass(".delete-this");
					}
				});
			}
		}
	}); 
	
	$(".deletemail").on('click', function(e) {
		$(this).parent().parent().addClass("delete-this");
		e.preventDefault();
		var id = $(this).attr('id');
		var url = $(this).attr("href");
		var blogInfo = $(this).attr("blog-info");
		if(id > 0) {
			if(confirm("Are you sure you want to delete?"))
			{
				$.ajax({
					type: "POST",
					url: url,
					data: "id=" + id + "&action=delete",
					beforeSend: function () {
						$("#notify3").html('<img src="'+blogInfo+'/images/loader.gif">');
					},
					success: function(result) {
						$("#notify3").html(result);
						$(".delete-this").remove();
						$(".delete-this").removeClass(".delete-this");
					}
				});
			}
		}
	});
	
	$(".deletefacility").on('click', function(e) {
		$(this).parent().addClass("delete-this");
		e.preventDefault();
		var id = $(this).attr('id');
		var url = $(this).attr("href");
		var blogInfo = $(this).attr("blog-info");
		if(id > 0) {
			if(confirm("Are you sure you want to delete?"))
			{
				$.ajax({
					type: "POST",
					url: url,
					data: "id=" + id + "&action=delete",
					beforeSend: function () {
						$("#notify2").html('<img src="'+blogInfo+'/images/loader.gif">');
					},
					success: function(result) {
						$("#notify2").html(result);
						$(".delete-this").remove();
						$(".delete-this").removeClass(".delete-this");
					}
				});
			}
		}
	});
	
	$(".approve").on('click', function(e) {
		e.preventDefault();
		var id = $(this).attr("id");
		var url = $(this).attr("href");
		var blogInfo = $("#bloginfo").attr("bloginfo");
		// alert(blogInfo);
		$(this).parent().addClass("update-this");
		$.ajax({
			type: "POST",
			url: url,
			data: {id:id},
			beforeSend: function () {
				$(".update-this").html('<img src="'+blogInfo+'/images/loader.gif" alt="">');
			},
			success: function(result) {
				$(".update-this").html('');
			}
		});
	});
	
	
	//$(".approved").on('click', function(e) {
	$(".admin_member_list").on("click", ".approved", function(e){
		e.preventDefault();
		var id = $(this).attr("id");
		//var url = $(this).attr("href");
		//var blogInfo = $("#bloginfo").attr("bloginfo");
		// alert(blogInfo);
		$(this).parent().addClass("update-this");
		$.ajax({
			type: "POST",
			url: "wp-content/themes/marketingadmissions/systemapplication/ajaxapprove.php",
			data: {id:id},
			beforeSend: function () {
				$(".update-this").html('<img src="wp-content/themes/marketingadmissions/images/loader.gif" alt="">');
			},
			success: function(result) {
				$(".update-this").html('');
			}
		});
	});
	
	
	$(".get_event_id").on('click', function(e){
		e.preventDefault();
		
		$('#getid').modal({
			show: true,
			backdrop: 'static'
		});
		
		var id = $(this).attr("id");
		$.ajax({
			type: "GET",
			url: "wp-content/themes/marketingadmissions/systemapplication/geteventajax.php",
			data: "id="+id,
			dataType: 'json',
			// beforeSend: function () {
				// $(".update-this").html('<img src="wp-content/themes/marketingadmissions/images/loader.gif" alt="">');
			// },
			success: function(data) {
				//alert(data);
				//$('#getid').modal(show);
				$(".event_name").append(data.event_name);
				$(".event_location").append(data.event_location);
				$(".sponsor").append(data.sponsor);
				$(".event_date").append(data.event_date);
				$(".event_time").append(data.event_time);
				$(".price").append(data.price);
				$(".event_contact").append(data.event_contact);
			}
		});
	});
	
	$(".close").on('click', function(e){
		$('#getid').modal('hide');
		$('.event_name').empty();
		$('.event_location').empty();
		$(".sponsor").empty();	
		$(".event_date").empty();	
		$(".event_time").empty();
		$(".price").empty();		
		$(".event_contact").empty();	
	});
	
	
	$("#host").on("submit", function(e){
		e.preventDefault();
		var formdata = $(this).serialize();
		if($(this).validationEngine('validate')){
			
			$.ajax({
			type: "POST",
			url: "wp-content/themes/marketingadmissions/includes/forms/ajaxhostform.php",
			data: formdata,
			beforeSend: function () {
				// $(".update-this").html('<img src="wp-content/themes/marketingadmissions/images/loader.gif" alt="">');
			},
			success: function(data) {
				$("#result").html(data);
				alert("Your message has been submitted.  We will get in touch with you as soon as possible.");
				$('#beahost').modal('hide')
			}
		});
		
		}
	});
	
	$('#edit_admin_info_form').on('submit', function(e) {
		e.preventDefault();
		var formdata = $(this).serialize();
		
		$.ajax({
			type: "POST",
			url: "wp-content/themes/marketingadmissions/systemapplication/ajaxEditAdminInfo.php",
			data: formdata,
			beforeSend: function() {
				$(".update_loader").show();
			},
			success: function(result) {
				$(".update_loader").hide();
				console.log(result);
				alert("Info has been updated!");
				$("#edit-admin-info-modal").modal("hide");
			}
		});
	});
	
	
	$(".check_this").on('change', function(){
		if($(this).is(":checked")) {
			$(".other_shows").show();
		} else {
			$(".other_shows").hide();
		}
	});
	
	$("#update_status_form").on('submit', function(e){
		e.preventDefault(e);
		var formdata = $(this).serialize();
		
		$.ajax({
			type: "POST",
			url: "wp-content/themes/marketingadmissions/systemapplication/ajaxUpdateStatusForm.php",
			data: formdata,
			dataType: "json",
			beforeSend: function() {
				$(".loader").show();
			},
			success: function(result) {
				console.log(result);
				if(result.edited){
					if(result.companies.status == 0) {
						$("#company_status_line").html('').append('Unpaid');
					} else {
						$("#company_status_line").html('').append('Paid');
					} 
					
				$("#company_status_line").append('<a id="'+result.companies.company_id +'" class="table_status_form" href="javascript:;" data-toggle="modal" data-target="#UpdateStatus"><img src="wp-content/themes/marketingadmissions/images/update_status_icon.png" title="Update Status"></a>')
				
				$(".loader").hide();
				$("#UpdateStatus").modal('hide'); 
				
				$("$company_status_line").removeAttr("id");
				}
			}
		});
	});
	
	$(".table_status_form").on('click', function(){
		$(this).parent().attr("id", "company_status_line");
		var company_id = $(this).attr("id");
		$(".update_form_id").val(company_id);
	});
	
	
	$("#update_status_form2").on('submit', function(e){
		e.preventDefault(e);
		var formdata = $(this).serialize();
		
		$.ajax({
			type: "POST",
			url: "wp-content/themes/marketingadmissions/systemapplication/ajaxUpdateStatusForm2.php",
			data: formdata,
			dataType: "json",
			beforeSend: function() {
				$(".loader").show();
			},
			success: function(result) {
				console.log(result);
				if(result.edited){
					if(result.companies.status == 0){
						$("#company_status_line2").html('').append('Unpaid');
					}else{
						$("#company_status_line2").html('').append('Paid');
					}
					
				$("#company_status_line2").append('&nbsp;<a id="'+ result.companies.company_id +'" class="table_stat us_form_2" href="javascript:;" data-toggle="modal" data-target="#UpdateStatus2"><img src="wp-content/themes/marketingadmissions/images/update_status_icon.png" title="Update Status"></a>');
				
				$(".loader").hide();
				$("#UpdateStatus2").modal('hide'); 
				
				$("#company_status_line2").removeAttr("id");
				}
			}
		});
	});
	
	$(".admin_member_list").on('click', ".table_status_form_2", function(){
		$(this).parent().attr("id", "company_status_line2");
		var company_id = $(this).attr("id");
		$(".update_form_id2").val(company_id); 
	});
	
});
