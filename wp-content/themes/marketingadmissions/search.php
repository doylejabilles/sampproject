<?php
	@session_start();
	get_includes('head');
	get_includes('header');
	get_includes('nav');
	if(is_front_page()){  get_includes('banner'); }
?>

	<section class="grid container clearfix">

		<div class="page-content float-right align-justify">
			<?php if ( have_posts() ) : ?>
					<h1 class="page-title"><span><?php printf( __( 'Search Results for: %s', 'twentyten' ), '<span>' . get_search_query() . '</span>' ); ?></span></h1>
					<div class="search_results">
					<?php
					/* Run the loop for the search to output the results.
					 * If you want to overload this in a child theme then include a file
					 * called loop-search.php and that will be used instead.
					 */
					 get_template_part( 'loop', 'search' );
					?>
					</div>
				<?php else : ?>
					<div id="post-0" class="post no-results not-found">
						<h1 class="entry-title"><span><?php _e( 'Nothing Found', 'twentyten' ); ?></span></h1>
						<div class="entry-content">
							<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'twentyten' ); ?></p>
							<?php get_search_form(); ?>
						</div><!-- .entry-content -->
					</div><!-- #post-0 -->
				<?php endif; ?>
		</div>
		
		<?php get_includes('sidebar'); ?>	
		  
	</section>

<?php get_includes('footer'); ?>