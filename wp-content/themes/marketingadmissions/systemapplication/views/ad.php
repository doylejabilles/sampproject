<?php

	get_common(array('config', 'Form', 'DB', 'Session'));
	get_model(array('Company', 'CompanyDAO', 'Member', 'MemberDAO', 'Admin'));
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$m_var = new Member();
	$m_dao = new MemberDAO();
	
	$company_id = Session::get('ID');
	$company = $c_dao->getRow("companies", "company_id=$company_id");
	$member = $m_dao->get("members", 'company_id='.$company_id);
	
	$filename = "";
	$filename1 = "";
	
	$banner = $c_dao->getRow("ads", "companyid=".$company_id);
	
?>

<div id="bannerad" class="tab-pane fade"> 
	<p>
		<div class="row alert alert-info clearfix autobiography">
			<div class="col-md-12 text-center" style="margin-bottom: 20px;">
			<?php
			if(!empty($banner)) { ?>
				<img src="<?php bloginfo('template_url'); ?>/systemapplication/uploads/ads/<?php echo $banner->ads; ?>" class="img-responsive img-thumbnail" alt="Responsive image">
				<!--<div class="profile-photo"></div>-->
			<?php
			} ?>
			</div>
			<form role="form" id="ads_banner" method="POST" enctype="multipart/form-data" >
				<div class="form-group">
					<div class="col-md-12">
						<label for="file">Upload your Ad</label>
						<input type="file" name="ads" name="image" id="ads"><br>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<input type="submit" name="btnads" class="btn btn-primary" value="Submit Ad">
					</div>
				</div>
			</form>
		</div>
	</p>
</div>