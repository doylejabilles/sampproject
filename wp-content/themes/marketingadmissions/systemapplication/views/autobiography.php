<?php

	
	get_common(array('config', 'Form', 'DB', 'Session'));
	get_model(array('Company', 'CompanyDAO', 'Member', 'MemberDAO', 'Admin'));
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$m_var = new Member();
	$m_dao = new MemberDAO();
	
	$company_id = Session::get('ID');
	$company = $c_dao->getRow("companies", "company_id=$company_id");
	$member = $m_dao->get("members", 'company_id='.$company_id);
	
	$bio = $c_dao->getRow("autobiography", "companyid=".$company_id);
	
	// include 'wp-content/themes/marketingadmissions/systemapplication/myprofile.php';
	

?>

<div id="autobiography" class="tab-pane fade"> 
	<p>
		<div class="row alert alert-info clearfix autobiography">
			<form role="form" action="" method="post">
				<div class="form-group">
					<label>About us</label>
					<textarea name="autobiography" class="form-control" rows="10" placeholder="About us"><?php echo $bio->autobiography;?></textarea>
				</div>
				<input type="submit" class="btn btn-default" name="btnbiography" value="Submit">
			</form>
		</div>
	</p>
</div>