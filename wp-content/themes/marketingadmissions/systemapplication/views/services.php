<?php

	
	get_common(array('config', 'Form', 'DB', 'Session'));
	get_model(array('Company', 'CompanyDAO', 'Member', 'MemberDAO', 'Admin'));
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$m_var = new Member();
	$m_dao = new MemberDAO();
	
	$company_id = Session::get('ID');
	$company = $c_dao->getRow("companies", "company_id=$company_id");
	$member = $m_dao->get("members", 'company_id='.$company_id);
	
	$errors = array();
	$error = '';
	$success = '';

	if(isset($_POST['btnservices'])) {
		$c_var->company_id = $company_id;
		$c_var->services = Form::data('services');		
		$c_dao->updateServices($c_var, 'companies');
		
		echo '<script>alert("Services has been updated!");window.location = "charity-organization-my-profile";</script>';
	}
	
?>

<div id="services" class="tab-pane fade"> 
	<p>
		<div class="row alert alert-info clearfix autobiography">
			<form role="form" action="" method="post">
				<div class="form-group">
					<label>Services</label>
					<textarea name="services" class="form-control" rows="10" placeholder="Services"><?php echo $company->services;?></textarea>
				</div>
				<input type="submit" class="btn btn-default" name="btnservices" value="Submit">
			</form> 
		</div>
	</p>
</div>