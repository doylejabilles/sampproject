<?php

	get_common(array('config', 'Form', 'DB', 'Session'));
	get_model(array('Company', 'CompanyDAO', 'Admin', 'AdminDAO'));
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$a_var = new Admin();
	$a_dao = new AdminDAO();
	// print_r($a_var); exit();
	
	$id = Session::get('ID');
	$admin = $c_dao->getRow("admin", "id='$id'");
	// print_r($admin); exit();
	
	if(isset($_POST['submit-admin-update'])) {
		$c_var->id = Session::get('ID');
		
		$c_var->name = Form::data('name');
		$c_var->username = Form::data('username');
		$c_var->password = Form::data('password');
		$c_var->repeat_password = Form::data('repeat_password');
		$c_var->email_address = Form::data('email_address');
		
		if(empty($a_var->password) || empty($a_var->repeat_password)) {
			echo '<script>alert("Please fill in all the important fields!");</script>';
		} elseif( $a_var->password != $a_var->repeat_password ) {
			echo '<script>alert("Your password did not match!");</script>';
		} else {
			$c_dao->updateLogin($c_var, 'admin');
			// echo '<script>window.location="charity-organization-admin";</script>';
		}
	}

?>
<div class="modal fade" id="edit-admin-info-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel" style="font-weight: bold;">Edit Info</h4>
      </div>
      <div class="modal-body alert-info col-md-12">
		<div class="update_loader" style="display: none;">
			<img src="wp-content/themes/marketingadmissions/images/loader.gif">
		</div>
		<form role="form" id="edit_admin_info_form" method="POST">
			<div class="form-group">
				<label for="name">Full Name</label>
				<input type="text" class="form-control" name="name" value="<?php echo $admin->name; ?>">
			</div>
			<div class="form-group">
				<label for="username">Username</label>
				<input type="text" class="form-control" name="username" value="<?php echo $admin->username; ?>">
			</div>
			<div class="form-group">
				<label for="email_address"><span class="required">*</span>Email Address</label>
				<input type="text" class="form-control validate[required]" name="email_address" value="<?php echo $admin->email_address; ?>">
			</div>
			<div class="form-group">
				<label for="password"><span class="required">*</span>Password</label>
				<input type="text" class="form-control validate[required]" name="password" value="<?php echo $admin->password; ?>">
			</div>
			<div class="form-group">
				<label for="repeat_password"><span class="required">*</span>Repeat Password</label>
				<input type="text" class="form-control validate[required]" name="repeat_password" value="<?php echo $admin->repeat_password; ?>">
			</div>
			<div class="form-group" style="text-align: center">
				<button type="submit" id="" name="submit-admin-update" class="btn btn-default">Save changes</button>
			</div>
		</form>
      </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>