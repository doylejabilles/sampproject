<?php	
	get_common(array('config', 'Form', 'DB', 'Session'));
	get_model(array('Company', 'CompanyDAO', 'Admin', 'AdminDAO'));
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$a_var = new Admin();
	$a_var = new AdminDAO();
	
	//echo $userid = $_SESSION['ID'];
	
	$id = Session::get('ID');
	$admin = $c_dao->getRow("admin", "id='$id'");
	
?>
<div class="modal fade" id="UpdateStatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Status Update</h4>
      </div>
      <div class="modal-body">
	  <div class="loader" style="display:none;">
		<img src="wp-content/themes/marketingadmissions/images/loader.gif" alt="">
	  </div>
        <form role="form" method="POST" id="update_status_form">
			<input type="hidden" name="company_id" value="" class="update_form_id">
			<label><span class="required">*</span>Status</label>
			<div class="form-group">
				<select class="form-control validate[required]" name="status">
				  <option value="0">Unpaid</option>
				  <option value="1">Paid</option>
				</select>
			</div>
			<div class="form-group" style="text-align:center;">
				<input type="submit" class="btn btn-default" name="btn-update-status" value="Submit">
			</div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>