<?php
	
	include 'wp-content/themes/marketingadmissions/forms/form_details.php';
	if($smtp)
	include 'wp-content/themes/marketingadmissions/forms/phpmailer/sendmail.php';

	get_common(array('config', 'Form', 'DB', 'Session'));
	get_model(array('Company', 'CompanyDAO', 'Member', 'MemberDAO', 'Admin'));
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$m_var = new Member();
	$m_dao = new MemberDAO();

	
	$errors = array();
	$error = '';
	$success = '';
	
	$regexp = "/^[a-z0-9]+([_\\.-][a-z0-9]+)*@([a-z0-9]+([\.-][a-z0-9]+)*)+\\.[a-z]{2,}$/i";
	
	$company = $c_dao->getRow("companies", 'company_id='.$_GET['id']);
	$company_id = $company->company_id;
	$member = $m_dao->get("members", 'company_id='.$company_id);
	$pic = $c_dao->get("pictures", 'companyid='.$company_id);
	
	// var_dump($member);exit();
	// print_r($member);exit();
	
	if( isset($_POST['btnedit']) ) {
		$c_var->company_id = $_GET['id'];
		
		$c_var->account_type = Form::data('account_type');
		$c_var->company_name = Form::data('company_name');
		$c_var->company_address = Form::data('company_address');
		$c_var->company_contactno = Form::data('company_contactno');
		$c_var->username = Form::data('username');
		$c_var->city = Form::data('city');
		$c_var->state = Form::data('state');
		$c_var->zip = Form::data('zip');
		$c_var->category = Form::data('category');
		$c_var->capacity = Form::data('capacity');
		$temp_companytype = $_POST['company_type'];
		$c_var->phone_number = Form::data('phone_number');
		$c_var->fax_number = Form::data('fax_number');
		$c_var->website = Form::data('website');
		$c_var->email_address = Form::data('email_address');
		$c_var->alz = Form::data('alz');
		$c_var->other = Form::data('other');
		$c_var->description = Form::data('description');
		$c_var->res_qual = Form::data('res_qual');
		$c_var->private1 = Form::data('private1');
		$c_var->ssi = Form::data('ssi');
		$c_var->medi_cal = Form::data('medi_cal');
		$c_var->medicare = Form::data('medicare');
		$c_var->other2 = Form::data('other2');	
		
		// $m_var->fname = Form::data('fname');
		// $m_var->mname = Form::data('mname');
		// $m_var->lname = Form::data('lname');
		// $m_var->cemail = Form::data('cemail');
		// $m_var->title = Form::data('title');
		
		$m_var->fname = $_POST['fname'];
		$m_var->mname = $_POST['mname'];
		$m_var->lname = $_POST['lname'];
		$m_var->cemail = $_POST['cemail'];
		$m_var->title = $_POST['title'];
		
		// var_dump($c_var->company_type); exit();
			
			$c_var->company_type = "";
			foreach($temp_companytype as $t):
				$c_var->company_type .= $t.', ';
			endforeach;
		
		
		// print_r($c_var);
		// print_r($m_var);exit();
		
		//validation of required fields
		if( empty($c_var->company_name) || empty($c_var->company_address) || empty($c_var->company_contactno) || empty($c_var->city) || empty($c_var->state) || empty($c_var->email_address) ) {
			$errors[] = 'Please fill in required fields';
			// echo '<pre>'; print_r($c_var);exit(); echo '</pre>';
		}
	
		// validates if no contact person is entered. should input at least 1 contact person
		elseif( empty($m_var->fname) || empty($m_var->lname) || empty($m_var->cemail) ) {
			$errors[] = 'Please enter at least one contact person.';
			// print_r($m_var);exit();
		}	
		
		
		//checking if the email is address is valid
		if( !preg_match($regexp, $c_var->email_address) ) { 
			$errors[] = 'Invalid Email address.';
	    } 
		
		//counting all the errors
		if( count($errors) > 0 ) {
			$error = implode('<br/>', $errors);	
		}
		else{
			$c_dao->updateProfile($c_var, "companies");
			//echo $member[1]->fname; exit();
			if(!empty($member[1]->fname)){
				//If there are 2 contact persons
				$member_ids = array($member[0]->member_id, $member[1]->member_id);
				for($i = 0; $i < 2; $i++) {
					// echo $_POST['fname'][$i].' '.$_POST['lname'][$i].' '.$_POST['title'][$i].' '.$_POST['cemail'][$i].'<br>'; 
					if(!empty($_POST['fname'][$i]) && !empty($_POST['lname'][$i]) && !empty($_POST['cemail'][$i]) ) {
						$m_var->member_id = $member_ids[$i];
						$m_var->company_id = $company_id;
						$m_var->fname = $_POST['fname'][$i];
						$m_var->mname = $_POST['mname'][$i];
						$m_var->lname = $_POST['lname'][$i];
						$m_var->title = $_POST['title'][$i];
						$m_var->cemail = $_POST['cemail'][$i];
						$m_dao->updateMember($m_var, "members");
					}else{
						$m_dao->delete('members', "WHERE member_id=".$member_ids[$i]);
					}
				}
			}else{
				//If there is only one contact person
				$member_id = $member[0]->member_id;
				if(!empty($_POST['fname'][0]) && !empty($_POST['lname'][0]) && !empty($_POST['cemail'][0]) ) {
					$m_var->member_id = $member_id;
					$m_var->company_id = $company_id;
					$m_var->fname = $_POST['fname'][0];
					$m_var->mname = $_POST['mname'][0];
					$m_var->lname = $_POST['lname'][0];
					$m_var->title = $_POST['title'][0];
					$m_var->cemail = $_POST['cemail'][0];
					$m_dao->updateMember($m_var, "members");
				}
				// echo $_POST['fname'][1].' '.$_POST['lname'][1].' '.$_POST['title'][1].' '.$_POST['cemail'][1];exit();
				if(!empty($_POST['fname'][1]) && !empty($_POST['lname'][1]) && !empty($_POST['cemail'][1]) ) {
					$m_var->company_id = $company_id;
					$m_var->fname = $_POST['fname'][1];
					$m_var->mname = $_POST['mname'][1];
					$m_var->lname = $_POST['lname'][1];
					$m_var->title = $_POST['title'][1];
					$m_var->cemail = $_POST['cemail'][1];
					$m_dao->addMember($m_var, "members");
				}
			}
			
				//$success = "Profile has been successfully updated!";
			// echo '<script>alert("Profile has been successfully updated!"); location.reload();</script>';
			// echo '<script>location.reload();</script>';
			
			
			//sending email notification
			// $profile_update = $c_var->email_address = $_POST['email_address'];
			// $profile_update2 = $m_var->cemail = $_POST['cemail'][0];
			
			$body = 'The admin has updated your profile, please contact the admin regarding the changes in your profile';
			
			$subject = $dcomp . "[ Account Updated ]";		
			
			/************** for smtp ***********/
			if($smtp) { 
				$mail = new SendMail($host, $username, $password);
				$to_email = 'doylace@gmail.com';
				//$to_email = $c_var->email_address = $_POST['email_address'];
				$cc = $m_var->cemail = $_POST['cemail'][0];
				$trysend = $mail->sendNow($to_email, $to_name, $cc, $bcc, $from_email, $from_name, $subject, $body);
				if ($trysend == 'ok')
					$sent = true;
				else
					$sent = false;
			}else {
			/************** for mail function ***********/
				$headers= 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= "From: ".$from_name." <".$from_email.">\n";
				$headers .= "Cc: ".$cc."\n";
				$headers .= "Bcc: ".$bcc;
				if(mail($to_email, $subject, $body, $headers)) {
					$sent = true;				
				}else {
					$sent = false;
				}
			}
			
			if($sent) {
					// $prompt_message = '<div id="success">Status update has been successfully sent to your email.</div>';
					 // echo '<script>alert("Profile has been successfully updated!</script>';
					 echo '<script>alert("An email notication has been sent to this member.");location.reload();</script>';
					 // echo '<script>location.reload(); </script>';
					unset($_POST);
			}else {
					$prompt_message = '<div id="error">Failed to send email.</div>';				
			}
		}
		
	}
	
/** Declaration for state **/
	$states = array('Please select a state.','Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District Of Columbia','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Puerto Rico','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virgin Islands','Virginia','Washington','West Virginia','Wisconsin','Wyoming');
	
	
?>

<style>
.edit-profile { height: auto; width: auto; margin: 15px auto; padding:15px; }
</style>
<?php foreach ($pic as $p): ?>
<div class="col-md-6"><!-- Uploaded profile must display here -->
	<div class="text-right">
		<img src="<?php bloginfo('template_url'); ?>/systemapplication/uploads/<?php echo $p->pic; ?>" class="img-responsive img-thumbnail" alt="Responsive image">
	</div>
</div>
<?php endforeach; ?>
<div class="clearfix edit-profile"> 
	<?php echo $prompt_message; ?>
	<div class="col-md-11" id="notify"><?php echo (!empty($error)) ? Form::error($error) : ''; echo (!empty($success)) ? Form::success($success) : ''; ?></div>
	<ul role="tablist" class="nav nav-tabs" id="myTab">
		<li class="active"><a data-toggle="tab" role="tab" href="#profile">Profile</a></li>
		<li class=""><a data-toggle="tab" role="tab" href="#autobiography">About Us</a></li>
		<!--<li class=""><a data-toggle="tab" role="tab" href="#bannerad">Your Ad</a></li>-->
		<li class=""><a data-toggle="tab" role="tab" href="#ourfacility">Gallery </a></li>
	</ul>
	<div class="tab-content" id="myTabContent">
		
			<!-- Profile -->
		<?php echo get_subpage('adminviews/profile'); ?>	
		
			<!-- About us -->
		<?php echo get_subpage('adminviews/autobiography'); ?>
		
			<!-- Gallery -->
		<?php echo get_subpage('adminviews/facility'); ?>
		
	</div>
</div>	