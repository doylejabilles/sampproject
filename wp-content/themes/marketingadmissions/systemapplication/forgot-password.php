<?php
	get_common(array('config', 'Form', 'DB', 'Session'));
	get_model(array('Company', 'CompanyDAO', 'Member', 'MemberDAO', 'Admin'));

	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$m_var = new Member();
	$m_dao = new MemberDAO();
	
	$regexp = "/^[a-z0-9]+([_\\.-][a-z0-9]+)*@([a-z0-9]+([\.-][a-z0-9]+)*)+\\.[a-z]{2,}$/i";
	
	$errors = array();
	$error = '';
	$success = '';
	$company = '';
	$company_name = '';
	$email = '';

	if($smtp) {
		include 'phpmailer/sendmail.php';
	}
	
	$btnreset = Form::data('reset_password');
	if(isset($btnreset) && $btnreset == 'Submit') {
	
		//$c_var->email_address = Form::data('email_address');
		$email_address = $_POST['email_address'];
		$c_var->company_name = Form::data('company_name');
		$secode = Form::data('secode');
		
		//fields that must be inputted
		if( empty($secode) || empty($email_address) || empty($c_var->company_name)  ) {
			$errors[] = 'Please fill out all the fields';
			$error = implode('<br/>', $errors);
			//echo '<script>$("#forgotpassword_email").focus()</script>';
		}
		else {
			//check if email address format is correct
			if( !empty($email_address) && !preg_match($regexp, $email_address) ) { 
				$errors[] = 'Invalid Email address.'; 
			}
			
			if( $email_address && count($errors) > 0 ) {
				$error = implode('<br/>', $errors);
				//echo '<script>$("#forgotpassword_email").focus()</script>';
			} else {				
				//$company = $c_dao->getRow('companies', 'email_address="'. $email_address . '"'); // email_address="<value>"
				$company = $c_dao->getRow('companies', 'company_name="'. $c_var->company_name . '"');
				$email = $email_address;
			} 
			
		}
		
		if( isset($company) && !empty($email) || !empty($company) ){ 
			$sq = "'";
			$sent = false;
			/** send result to user's email **/
			$subject = 'Password Recovery';
			$body =  'Your Username is: ' . $company->username . '<br/>' . 'Your password is: '.$company->password;
			// $to_email = $company->email_address;
			$to_email = $email;
			//$to_name = ucfirst($company->l_name).', '.ucfirst($user->f_name).' '.ucfirst($user->m_name);
			$to_name = $company->company_name;
			$from_email = 'info@maps-sgv.org';
			$from_name = 'Marketing & Admissions Professionals for Seniors';
			
			// echo '<pre>'; print_r($body); '<pre>';
			// echo '<pre>'; print_r($to_email); '<pre>';
			// echo '<pre>'; print_r($to_name); '<pre>'; exit();
			
			
			/************** for smtp ***********/
			if($smtp) { 
				$mail = new SendMail($host, $username, $password);
				$trysend = $mail->sendNow($to_email, $to_name, $cc, $bcc, $from_email, $from_name, $subject, $body);
				if ($trysend == 'ok')
					$sent = true;
				else
					$sent = false;
			}else {
			
			/************** for mail function ***********/
				$headers= 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= "From: ".$from_name." <".$from_email.">\n";
				$headers .= "Cc: ".$cc."\n";
				if(!empty($bcc)){
					$headers .= "Bcc: ".$bcc;
				}
				if(mail($to_email, $subject, $body, $headers)) {
					$sent = true;				
				}else {
					$sent = false;
				}
			}
		
			if($sent){
				//Form::success('A notification is sent to you email.');
				echo '<script>alert("A notifcation is sent to your email!");</script>';
			}else{
				Form::error('Something wen'.$sq.'t wrong. Please try again.');
			}
			
		}else{
			Form::error('Email not found.');
			echo '<script>$("#forgotpassword_email").focus()</script>';
		}

	}
		
?>
<style>
.member-list-content { color: #084D52 }
.member-list-content h1{ color: #084D52 }
.member-list-content a{ color: #084D52 }
.member-list-content label{ color: #084D52 }
.member-list-content a:hover{ color: #1AB7DF }
</style>
<div class="row clearfix member-list-content">
	<div id="notify"><?php echo (!empty($error)) ? Form::error($error) : ''; echo (!empty($success)) ? Form::success($success) : ''; ?></div>
	<div class="col-md-8 alert alert-info">
	<form role="form" id="forgot-password" method="POST">
		<div class="form-group">
		<span class="required">*</span><label>Contact Email</label>
			<input type="text" name="email_address" class="form-control" placeholder="Contact Email">
		</div>
		<div class="form-group">
		<span class="required">*</span><label>Company Name</label>
			<input type="text" name="company_name" class="form-control validate[required]" placeholder="Contact Email">
		</div>
		<div class="form-group">
			<span class="required">*</span><label>Security Code</label>
			<input type="text" name="secode" class="form-control validate[required]" placeholder="Security Code">
		</div>
		<div class="form-group">
			<img src="<?php bloginfo('template_url');?>/forms/securitycode/SecurityImages.php" border="0" />	
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-default" id="forgot-password" name="reset_password" value="Submit">Submit</button>
		</div>
	</form>
		<div style="text-align: center; color: #000;">
			For instructions <a href="javascript:;" data-toggle="modal" data-target="#instructions">Click Here!</a>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="instructions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content alert-info">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel" style="color: #084D52;"><strong>Instruction</strong></h4>
      </div>
      <div class="modal-body member-list-content">
        <ul>
			<li>Please enter the email address where you want to send your password.</li>
			<li>Please enter either the <i>Company name</i>, if the company has special character and spaces make sure that you also input it.<br/>
			<strong>Tip:</strong> You can Search in the <a href="charity-organization-members-directory" target="_blank">Member's Directory page</a>.</li>
			<li>Then enter the security code that is shown in the form.</li>
			<li>Once done, your password will be sent to your email address.</li>
		</ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
