<?php	
	
	require_once('common/config.php');
	require_once('common/Form.php');
	require_once('common/DB.php');
	require_once('common/Session.php');
	require_once('model/Company.php');
	require_once('model/CompanyDAO.php');
	require_once('model/Member.php');
	require_once('model/MemberDAO.php');
	require_once('model/Admin.php');
	require_once('model/AdminDAO.php');
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$a_var = new Admin();
	$a_dao = new AdminDAO();
	
	$id = Session::get('ID');
	$admin = $c_dao->getRow("admin", "id='$id'");

	
	if($_POST) {
		$c_var->id = Session::get('ID');
		
		$c_var->name = Form::data('name');
		$c_var->username = Form::data('username');
		$c_var->password = Form::data('password');
		$c_var->repeat_password = Form::data('repeat_password');
		$c_var->email_address = Form::data('email_address');
		
		$c_dao->updateLogin($c_var, 'admin');
		
	}
?>