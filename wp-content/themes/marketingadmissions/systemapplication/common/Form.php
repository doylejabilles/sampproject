<?php
class Form
{
	public static function get($field,$default='')
	{
		if( isset($_GET[$field]) )
			return trim( $_GET[$field] );
		return $default;
	}
	
	public static function post($field,$default='')
	{
		if( isset($_POST[$field]) )
			return trim( $_POST[$field] );
		return $default;
	}
	
	public static function getNumber($field,$default=0)
	{
		$d = self::data($field,$default);
		if(is_numeric($d))
		{
			if(is_int($d))
				return intval($d);
			else
				return doubleval($d);
		}			
		return $default;
	}
	
	public static function data($field,$default='')
	{
		if( isset($_REQUEST[$field]) )
			return trim( $_REQUEST[$field] );
		return $default;
	}
	
	public static function alert($msg)
	{
		echo '<script type="text/javascript">';
		echo '  alert("'. addslashes($msg). '");';
		echo '</script>';			
	}
	
	public static function redirect($url,$delay=0,$msg='')
	{
		if(!empty($msg)) {
			self::success($msg);
		}
		echo '<script type="text/javascript">';		
		echo "  setTimeout(function(){document.location.href = \"$url\";},($delay * 1000));</script>";
		echo '</script>';
	}
	
	public static function success($msg)
	{
		if(trim($msg) != '')
		{
			echo '<div style="padding:8px;" class="ui-state-highlight ui-corner-all msg-success">';
			echo '   <table border="0"><tr style="border:none;"><td valign="top" style="font:normal 12px Arial; padding-right:5px; width:90px;"><span style="float: left; margin-right: 0.3em" class="ui-icon ui-icon-check"></span><strong>Success : </strong></td><td valign="top" style="font:normal 12px Arial;color:green;">' . $msg . '</td></tr></table>';
			echo '</div>';
		}
	}
	
	public static function error($msg)
	{
		if(trim($msg) != '')
		{
			echo '<div style="padding:8px;" class="ui-state-error ui-corner-all msg-error">';
			echo '   <table><tr style="border:none;"><td valign="top" style="font:normal 12px Arial; padding-right:5px;"><span style="float:left; margin-right: 0.3em" class="ui-icon ui-icon-alert"></span><strong>Error : </strong></td><td valign="top" style="font:normal 12px Arial; text-align:left;">' . $msg . '</td></tr></table>';
			echo '</div>';
		}
	}
	
	public static function info($msg)
	{
		if(trim($msg) != '')
		{
			echo '<div style="padding:8px" class="ui-state-highlight ui-corner-all msg-info">';
			echo '   <table border="0"><tr style="border:none;"><td valign="top" style="font:normal 12px Arial; padding-right:5px; width:60px;"><span style="float:left; margin-right: 0.3em" class="ui-icon ui-icon-info"></span><strong>Note : </strong></td><td valign="top" style="font:normal 12px Arial;">' . $msg . '</td></tr></table>';
			echo '</div>';
		}
	}
	
	public static function warn($msg)
	{
		if(trim($msg) != '')
		{
			echo '<div style="padding:8px" class="ui-state-highlight ui-corner-all msg-warning">';
			echo '   <table border="0"><tr style="border:none;"><td valign="top" style="font:normal 12px Arial; padding-right:5px; width:60px;"><span style="float:left; margin-right: 0.3em" class="ui-icon ui-icon-alert"></span><strong>Note : </strong></td><td valign="top" style="font:normal 12px Arial;">' . $msg . '</td></tr></table>';
			echo '</div>';
		}
	}
	
	public static function genRandomString()
	{
		$length = 20;
		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
		$string = '';    
		for ($p = 0; $p < $length; $p++) {
			$string .= $characters[mt_rand(0, strlen($characters))];
		}
		return $string;
	}
	
	public static function curPageURL_full() {
	
		$pageURL = 'http';
		if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
			$pageURL .= "://";
		if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
		} else {
			$pageURL .= $_SERVER["SERVER_NAME"];
		}
		return $pageURL;
	}
	
	public static function curPageURL() {
		$pageURL = '';
		$str = '';
		$str = $_SERVER["REQUEST_URI"];
		$pageURL = explode("/", $str);

		$count = count($pageURL);
		return $pageURL[$count-1];
	}
}
?>