<?php
	/** 
		Sample Usage:		
		$result = DB::query('select * from table');
		while($row = DB::fetchArray($result)){
			...
		}		
	**/
	class DB
	{
		protected static $lastQuery = '';
		protected static $connection = false;
		
		public static function close()
		{
			if(self::$connection)
				mysql_close(self::$connection);
			else
				mysql_close();
		}
	
		
		public static function open()
		{
			self::$connection = mysql_connect(DB_HOST,DB_UID,DB_PWD);			
			if (!self::$connection)
				die('Could not connect to host: ' . DB_HOST .': '. mysql_error());
			$db = mysql_select_db(DB_NAME);
			if(!$db)
				die('Could not select database: '. DB_NAME .': '. mysql_error());
			return self::$connection;
		}
		
		public static function query($sql)
		{
			if(!self::$connection)
				self::open();
				
			$result = mysql_query($sql);
			if (!$result)
				die('Invalid query: ' . mysql_error());
			
			self::$lastQuery = $sql;
			return $result;
		}
		
		public static function fetchArray($result)
		{
			return mysql_fetch_array($result, MYSQL_BOTH);
		}
		
		public static function fetchRow($result)
		{
			return mysql_fetch_row($result);
		}
		
		public static function fetchObject($result)
		{
			return mysql_fetch_object($result);
		}
		
		public static function getFieldTypes($result)
		{
			$field_types = array();
			$field_count = mysql_num_fields($result);
			for($i=0;$i<$field_count;$i++)
				$field_types[] = mysql_field_type($result,$i);
			return $field_types;
		}
		
		public static function getFieldNames($result)
		{
			$field_names = array();
			$field_count = mysql_num_fields($result);
			for($i=0;$i<$field_count;$i++)
				$field_names[] = mysql_field_name($result,$i);
			return $field_names;
		}
		
		public static function LastId($table,$field='id')
		{
			$sql = "SELECT MAX($field) FROM $table";
			
			// echo $sql;  exit();
			$result = mysql_query($sql);
			$id = mysql_result($result,0,0);
			return $id;
		}
		
		public static function count($result)
		{
			$isSelect = strpos( strtolower(trim(self::$lastQuery)), 'select') === 0;			
			if($isSelect)
				return mysql_num_rows($result);
			return mysql_affected_rows();
		}
		
		public static function free($result)
		{
			return mysql_free_result($result);
		}
		
		
		public static function escape($str)
		{
			if(self::$connection)
				mysql_real_escape_string($str,self::$connection);
			return mysql_escape_string($str);
		}		
	}
?>