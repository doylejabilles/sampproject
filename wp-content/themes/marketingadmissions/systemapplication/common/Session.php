<?php
session_start();
class Session
{
	public static function get($key)
	{
		return $_SESSION[$key];
	}
	public static function set($key, $value)
	{
		$_SESSION[$key] = $value;
	}

	public static function user()
	{
		$dao = new UserDAO();
		return $dao->findById( intval(self::get('ID')) );
	}
	
	public static function active()
	{
		return intval(trim(self::get('ACTIVE')));
	}
	
	public static function start($user, $kind="")
	{
		self::set('ACTIVE', 1);
		if(empty($kind)) {
			self::set('ID', $user->company_id);
			self::set('TYPE', 'member');
		} else {
			self::set('ID', $user->id);
			self::set('TYPE', 'admin');
		}// self::set('TYPE', $user->type);
	}
	
	public static function stop()
	{
		// Unset all of the session variables.
		$_SESSION = array();
		
		// If it's desired to kill the session, also delete the session cookie.
		// Note: This will destroy the session, and not just the session data!
		if (ini_get("session.use_cookies")) {
			$params = session_get_cookie_params();
			setcookie(session_name(), '', time() - 42000,
				$params["path"], $params["domain"],
				$params["secure"], $params["httponly"]
			);
		}
		// Finally, destroy the session.
		session_destroy();
	}
}
?>