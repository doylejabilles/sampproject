<?php
	require_once('common/config.php');
	require_once('common/Form.php');
	require_once('common/DB.php');
	require_once('common/Session.php');
	require_once('model/Company.php');
	require_once('model/CompanyDAO.php');
	require_once('model/Member.php');
	require_once('model/MemberDAO.php');
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$search_val = $_POST['search_val'];
	
	$arraySearchVal = str_split($search_val);
	$apostrophe = "'";
	
	$newSearchVal = "";
	foreach($arraySearchVal as $asv):
		if($asv == $apostrophe){
			$newSearchVal .= "'".$asv;
		}else{
			$newSearchVal .= $asv;
		}
	endforeach;
	
	$search_val = $newSearchVal;
	
	// $company_search = $c_dao->getJoin('c.company_id, c.company_name, c.company_address, c.company_contactno, tc.company_id AS tc_id', "companies", "c.account_type='Member' AND c.company_name LIKE '%$search_val%'",'c.company_name','ASC');
	$company_search = $c_dao->get('companies', "account_type='Member' AND company_name LIKE '%$search_val%'", 'company_name','ASC');
	
	$results = array();
?>
<style>
.member-list-content a { color: #084D52 } 
</style>
<div class="admin_member_content">
	<table class="table text-left">
		<th>Company Name</th>
		<th>Company Address</th>
		<th>Company Contact</th>
		<th>Status</th>
	<?php if(count($company_search)>0) { ?>
	<?php foreach($company_search as $cs): ?>
	<?php $results[] = $cs->company_id; ?>
		<tr> 
			<td><?php echo $cs->company_name; ?></td> 
			<td><?php echo $cs->company_address; ?></td>
			<td><?php echo $cs->company_contactno; ?></td>
			<td colspan="2" ><?php if($cs->status == 1) {
					echo 'Paid';
				} else {
					echo 'Unpaid';
				}
				?>&nbsp;<a id="<?php echo $cs->company_id; ?>" class="table_status_form_2" href="javascript:;" data-toggle="modal" data-target="#UpdateStatus2"><img src="wp-content/themes/marketingadmissions/images/update_status_icon.png" title="Update Status"></a>
			</td>
			<!--<td><?php //echo ( !empty($cs->tc_id) ) ? '<a href="wp-content/themes/marketingadmissions/systemapplication/ajaxapprove.php" id="'.$cs->tc_id.'" class="approved"><img src="wp-content/themes/marketingadmissions/images/pending-icon.png" alt="img"/></a>' : ''; ?></td>-->
			<td><a target="_blank" href="charity-organization-admin-edit-user?id=<?php echo $cs->company_id; ?>" id="<?php echo $cs->company_id; ?>" class="editinfo" title="View/Edit"><img src="wp-content/themes/marketingadmissions/images/edit_icon.png" alt="img"/></a></td>
			<td><a id="<?php echo $cs->company_id; ?>" class="deletemember" href="wp-content/themes/marketingadmissions/systemapplication/ajaxadmin.php" title="Delete"><img src="wp-content/themes/marketingadmissions/images/delete_icon.png" alt="img"/></a></td>
		</tr>
	<?php endforeach; ?>
	<?php } ?>
	<?php if(count($company_search)==0){ ?>
		<p style="text-align: center;">No records found</p>
	<?php } ?>
	</table>
</div>
