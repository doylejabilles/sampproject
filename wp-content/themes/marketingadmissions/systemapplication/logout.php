<?php
	require_once('common/Form.php');
	require_once('common/Session.php');
	
	$url = Form::data('redirect_url');
	
	Session::stop();
	
	echo "<script>window.parent.location = 'http://localhost/project/charity-organization-login'</script>";
?>