<?php

	include 'wp-content/themes/marketingadmissions/forms/form_details.php';
	if($smtp)
	include 'wp-content/themes/marketingadmissions/forms/phpmailer/sendmail.php';

	get_common(array('config', 'Form', 'DB', 'Session', 'Paypal'));
	get_model(array('Company', 'CompanyDAO', 'Member', 'MemberDAO', 'Admin', 'Event', 'EventDAO'));
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$m_var = new Member();
	$m_dao = new MemberDAO();
	
	$e_var = new Event();
	$e_dao = new EventDAO();
	// print_r($e_var); exit();
	
	$a_var = new Admin();
	
	$errors = array();
	$error = '';
	$success = '';
	
	// $var_dao->variable = date('Y-m-d');

	$id = Session::get('ID');
	$admin = $c_dao->getRow("admin", "id='$id'");
	$events = $c_dao->get("event");
	// print_r($get_event);exit();
		
	
	$active = ( isset($_SESSION['ACTIVE']) && ($_SESSION['ACTIVE'] == 1) ) ? true : false;
	$type = ( isset($_SESSION['TYPE']) && ($_SESSION['TYPE'] == "member") ) ? 'member' : 'admin';
?>

<style>
.event_container {margin: 15px auto; padding:15px; color:#084D52; }
.event_container th{ color:#084D52; }
.event { color: #067C3E; margin-bottom: 10px;}
.event_customer_side { color: #067C3E; }
.payment-option { float:left; margin-top: 10px; }
.backarrow { position: absolute; }
.backarrow a{ background-image:url('<?php bloginfo('template_url') ?>/images/backarrow.png'); height: 20px; width:20px; display:block; }
.backarrow a:hover { background-image:url('<?php bloginfo('template_url') ?>/images/backarrow_hover.png'); }
.register-link { margin-left: 180px; margin-top: 13px; font-size: 15px; font-style: italic }
.register-link a:hover { color: #067C3E; }
</style>

<?php if($active == true && $type == 'admin' ): ?>

<?php
	
	//for setting an event
	if(isset($_POST['eventregistrationbtn'])){
		
		$a_var->event_name = Form::data('event_name');
		$a_var->sponsor = Form::data('sponsor');
		$a_var->price = Form::data('price');
		$a_var->event_date = Form::data ('event_date');
		$a_var->event_time = Form::data('event_time');
		$a_var->event_location = Form::data('event_location');
		$a_var->event_contact = Form::data('event_contact');
		//print_r($a_var);exit();
		if( empty($a_var->event_name) || empty($a_var->event_time) || empty($a_var->event_date) || empty($a_var->event_location) ){
		 $errors[] = "Please fill in your event";
		}
		if( count($errors) > 0 ) {
			$error = implode('<br/>', $errors);
		}
		elseif( $c_dao->addEvent($a_var, "event") ){
			//$success = "Event successfully added!";
			 echo '<script>alert("Profile has been successfully added!"); window.location = "charity-organization-event-registration";</script>';
		}
	}
	
?>
<!--<link rel="stylesheet" href="<?php //bloginfo('template_url'); ?>/css/datepicker.css">-->
<link  href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="<?php bloginfo('template_url'); ?>/js/datepicker.js"></script>


<div class="clearfix alert-info event_container">
	
	<div class="event table-responsive text-left"> 
		<table class="table table-condensed text-left " style="width:100%">	
			<tr>
				<th colspan="5" style="text-align:center; font-family: Georgia; font-size: 20px;">Upcoming Events</th>
			</tr>
			<tr>
				<th>Event Name</th>
				<th>Sponsor</th>
				<th>Date</th>
				<th>Time</th>
				<th colspan="2">Action</th>
			</tr>
			<?php if ( !empty($events) ) {
				foreach($events as $event):?>
			<tr>
				<td><?php echo $event->event_name; ?></td>	
				<td><?php echo $event->sponsor; ?></td>
				<td><?php echo date("m/d/y",strtotime($event->event_date)); ?></td>
				<td><?php echo $event->event_time; ?></td>
				<td><a href="charity-organization-event-details?id=<?php echo $event->id; ?>" title="View Full Detail"><img src="<?php bloginfo('template_url') ?>/images/view_detail_icon.png" alt="img"/></a></td>
				<td>
					<a id="<?php echo $event->id; ?>" class="deleteinfo" href="<?php bloginfo('template_url'); ?>/systemapplication/eventdelete.php" blog-info="<?php bloginfo('template_url'); ?>" title="Delete"><img src="<?php bloginfo('template_url') ?>/images/delete_icon.png" alt="img"/></a>
				</td>
				<!--<td><button type="submit" method="POST" action="eventdelete.php" name="delete" class="btn btn-default">Delete</td>-->
			</tr>
			<?php endforeach;
				} 
				else { ?>
			<tr><td colspan="6">No records found.</td></tr>
			<?php } ?>
		</table>	
	</div>
	<div id="notify"><?php echo (!empty($error)) ? Form::error($error) : ''; echo (!empty($success)) ? Form::success($success) : ''; ?></div>
	<form role="form" id="eventregistration" method="POST">
		<div class="row">
			<div class="col-md-6">	
				<div class="form-group">
					<label><span class="required">*</span>Event name</label>
					<input type="text" name="event_name" class="form-control validate[required]">				
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>Sponsor(s)</label>
					<input type="text" name="sponsor" class="form-control">				
				</div>
			</div>
		</div>
		<div class="form-group">
			<label><span class="required"></span>Location</label>
			<input type="text" name="event_location" class="form-control validate[required]">
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><span class="required">*</span>Date</label>
					<input type="text" name="event_date" class="form-control datepicker validate[required]">	
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label><span class="required">*</span>Time</label>
					<input type="text" name="event_time" class="form-control validate[required]">
				</div>		
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label>Price</label>
					<input type="text" name="price" class="form-control">				
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>Contact</label>
					<input type="text" name="event_contact" class="form-control">				
				</div>
			</div>
		</div>		
		<div style="text-align: center;">
			<button type="submit" class="btn btn-default" id="eventregistrationbtn" name="eventregistrationbtn">Post Event</button>
		</div>
	</form>
</div>

<?php else : ?>

<div class="clearfix alert-info event_container">
	<div class="backarrow">
		<a href="<?php echo site_url(); ?>"></a>
	</div>	
	<div class="event_customer_side"> 
		<table style="width:100%" class="text-left table-condensed">	
			<tr>
				<th colspan="5" style="text-align:center; font-family: Georgia; font-size: 20px;">Upcoming Events</th>
			</tr>
			<tr>
				<th>Event Name</th>
				<th>Sponsor(s)</th>
				<th>Date</th>
				<!--<th style="color:#43A6E9">Time</th>-->
				<th colspan="2">Action</th>
			</tr>
			<?php if( !empty($events) ) { 
			foreach($events as $event):?>
			<tr>
				<td><?php echo $event->event_name; ?></td>
				<td><?php echo $event->sponsor; ?></td>
				<td><?php echo date("m/d/y",strtotime($event->event_date)); ?></td>
				<!--<td><?php //echo $event->event_time; ?></td>-->
				<td style="text-align:center!important;"><a href="charity-organization-event-details?id=<?php echo $event->id; ?>" title="View Full Detail"><img src="<?php bloginfo('template_url') ?>/images/view_detail_icon.png" alt="img"/></a></td>
				<?php if($active == true && $type == 'member') : ?>	
				<td><button type="submit" class="btn btn-default btn-sm btnregister" name="register" data-eventid="<?php echo $event->id; ?>" data-eventname="<?php echo $event->event_name; ?>" data-toggle="modal" data-target="#myModal">Register</button></td>
				<?php endif; ?>
			</tr>
			<?php endforeach;
			} else { ?>
			<tr><td colspan="6">No records found.</td></tr>
			<?php } ?>
		</table>
	</div>
	<?php if($active == false): ?>
	<div class="register-link">
		<a href="charity-organization-become-a-member"  style="color: #084D52;">Be a member and join our events now!</a>
	</div>
	<?php endif; ?>
	<div class="payment-option">
		<a href="javascript:;"><img src="wp-content/themes/marketingadmissions/images/paypal.png" alt="paypal" /></a>
	</div>
</div>	
<?php endif; ?>
			
<script>
	$('.datepicker').datepicker({
		dateFormat: 'yy-mm-dd',
		showAnim: "fadeIn"
	});
	
	$(".btnregister").click(function() {
		var mydata = $(this).data('eventname');
		var mydataid = $(this).data('eventid');
		$('#eventname').val(mydata);
		$('#eventid').val(mydataid);
		//alert(mydata);
	});
</script>

<!-- Modal -->

<?php
/*PHP function for modal form*/

if(isset($_POST['payment_registration'])){
		
		
		$credicard = $_POST['creditcardnum'];
		$payment_type = $_POST['payment_type'];
		$expirationdate = $_POST['exp_date'];
		$CVV = $_POST['cvv'];
		$firstname = $_POST['fname'];
		$lastname = $_POST['lname'];
		$address = $_POST['address'];
		$zip = $_POST['zip'];
		$city = $_POST['city'];
		$state = $_POST['state'];
		$phone = $_POST['phone1'];
		$email = $_POST['email'];
		$country = $_POST['country'];
		$street = $_POST['street'];
		
		
		$requestParams = array(
		'IPADDRESS' => $_SERVER['REMOTE_ADDR'], // Get our IP Address
		'PAYMENTACTION' => 'Sale'
		);

		$creditCardDetails = array(
			'CREDITCARDTYPE' => $payment_type,
			'ACCT' => $credicard,
			'EXPDATE' => $expirationdate, // Make sure this is without slashes (NOT in the format 07/2017 or 07-2017)
			'CVV2' => $CVV
		);

		$payerDetails = array(
			'FIRSTNAME' => $firstname,
			'LASTNAME' => $lastname,
			'COUNTRYCODE' => $country,
			'STATE' => $state,
			'CITY' => $city,
			//'STREET' => $street,
			'ZIP' => $zip
		);

		$orderParams = array(
			'AMT' => '20', // This should be equal to ITEMAMT + SHIPPINGAMT
			'ITEMAMT' => '20',
			'SHIPPINGAMT' => '0',
			'CURRENCYCODE' => 'USD' // USD for US Dollars
		);

		$item = array(
			'L_NAME0' => 'Registration',
			'L_DESC0' => 'test',
			'L_AMT0' => '20',
			'L_QTY0' => '1'
		);
		
		// echo '<pre>'; print_r($c_var); echo '</pre>';
		// echo '<pre>'; print_r($m_var); echo '</pre>';
		// echo '<pre>'; print_r($requestParams); echo '</pre>';
		// echo '<pre>'; print_r($creditCardDetails); echo '</pre>';
		// echo '<pre>'; print_r($payerDetails); echo '</pre>';
		// echo '<pre>'; print_r($orderParams); echo '</pre>';
		// echo '<pre>'; print_r($item); echo '</pre>';
		// exit;
		
		
	   $paypal = new Paypal();
	   $response = $paypal->request('DoDirectPayment', $requestParams + $creditCardDetails + $payerDetails + $orderParams + $item);
	   // print_r($response); exit;
	   if( is_array($response) && $response['ACK'] == 'Success') { // Payment successful
		   // We'll fetch the transaction ID for internal bookkeeping
			// $transactionId = $response['TRANSACTIONID'];
			// print_r($transactionId); exit();
			// echo exit($transactionId);
			// $adver_var->transactionId = $transactionId;
			// $check_success = true;
			
			
			//sending email notification
			
			$event = $_POST['eventname'];
			$eventid = $_POST['eventid'];
			
			$body = 'This person has registered to ' . '<span style="color:#27366D; font-size: 15px;">' . $event . ' ' . '(event number:' . $eventid . ')' . '</span>' . ' please see paypal account for more details.<br/>' .  'Name: ' . $firstname  . ' ' . $lastname . '<br/>' . 'Address: ' . $address . ' ' . $state . '<br/>'. 'Email: ' . $email;
			
			$subject = $dcomp . "[ Account Updated ]";		
			
			/************** for smtp ***********/
			if($smtp) { 
				$mail = new SendMail($host, $username, $password);
				// $to_email = 'doyle@proweaver.net';
				// $to_email = 'Info@maps-sgv.org';
				$to_email = 'info@maps-sgv.org';
				$trysend = $mail->sendNow($to_email, $to_name, $cc, $bcc, $from_email, $from_name, $subject, $body);
				if ($trysend == 'ok')
					$sent = true;
				else
					$sent = false;
			}else {
			/************** for mail function ***********/
				$headers= 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= "From: ".$from_name." <".$from_email.">\n";
				$headers .= "Cc: ".$cc."\n";
				$headers .= "Bcc: ".$bcc;
				if(mail($to_email, $subject, $body, $headers)) {
					$sent = true;				
				}else {
					$sent = false;
				}
			}
			
			$e_var->event_id = $_POST['eventid'];
			$e_var->company_id = $id;
			$e_var->transaction_no = $response['TRANSACTIONID'];
			$e_dao->addEvent($e_var, "event_reports");
			
			echo '<script>alert("Thank you for registering, your transaction has been successful!");</script>';
		
	   }
	   else
	   {
		$paypal_error = $response['L_LONGMESSAGE0'];
		// $check_success = false;
		Echo "There was an error processing Request!";		
	   }
	   
		 
	}


/* Declaration starts here */


$states = array('Please select a state.','Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District Of Columbia','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Puerto Rico','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virgin Islands','Virginia','Washington','West Virginia','Wisconsin','Wyoming');

$country = array('Please your country.','ALAND ISLANDS','ALBANIA','ALGERIA','AMERICAN SAMOA','ANDORRA','ANGOLA','ANGUILLA','ANTARCTICA','ANTIGUA AND BARBUDA','ARGENTINA','ARMENIA','ARUBA','AUSTRALIA','AUSTRIA','AZERBAIJAN','BAHAMAS','BAHRAIN','BANGLADESH','BARBADOS','BELGIUM','BELIZE','BENIN','BERMUDA','BHUTAN','BOLIVIA','BOSNIA-HERZEGOVINA','BOTSWANA','BOUVET ISLAND','BRAZIL','BRITISH INDIAN OCEAN TERRITORY','BRUNEI DARUSSALAM','BULGARIA','BURKINA FASO','BURUNDI','CAMBODIA','CANADA','CAPE VERDE','CAYMAN ISLANDS','CENTRAL AFRICAN REPUBLIC','CHAD','CHILE','CHINA','CHRISTMAS ISLAND','COCOS (KEELING) ISLANDS','COLOMBIA','COMOROS','DEMOCRATIC REPUBLIC OF CONGO','CONGO','COOK ISLANDS','COSTA RICA','CROATIA','CYPRUS','CZECH REPUBLIC','DENMARK','DJIBOUTI','DOMINICA','DOMINICAN REPUBLIC','ECUADOR','EGYPT','EL SALVADOR','ERITERIA','ESTONIA','ETHIOPIA','FALKLAND ISLANDS (MALVINAS)','FAROE ISLANDS','FIJI','FINLAND','FRANCE','FRENCH GUIANA','FRENCH POLYNESIA','FRENCH SOUTHERN TERRITORIES','GABON','GAMBIA','GEORGIA','GERMANY','GHANA','GIBRALTAR','GREECE','GREENLAND','GRENADA','GUADELOUPE','GUAM','GUATEMALA','GUERNSEY','GUINEA','GUINEA BISSAU','GUYANA','HEARD ISLAND AND MCDONALD ISLANDS','HOLY SEE (VATICAN CITY STATE)','HONDURAS','HONG KONG','HUNGARY','ICELAND','INDIA','INDONESIA','IRELAND','ISLE OF MAN','ISRAEL','ITALY','JAMAICA','JAPAN','JERSEY','JORDAN','KAZAKHSTAN','KENYA','KIRIBATI','KOREA, REPUBLIC OF','KUWAIT','KYRGYZSTAN','LAOS','LATVIA','LESOTHO','LIECHTENSTEIN','LITHUANIA','LUXEMBOURG','MACAO','MACEDONIA','MADAGASCAR','MALAWI','MALAYSIA','MALDIVES','MALI','MALTA','MARSHALL ISLANDS','MARTINIQUE','MAURITANIA','MAURITIUS','MAYOTTE','MEXICO','MICRONESIA, FEDERATED STATES OF','MOLDOVA, REPUBLIC OF','MONACO','MONGOLIA','MONTENEGRO','MONTSERRAT','MOROCCO','MOZAMBIQUE','NAMIBIA','NAURU','NEPAL','NETHERLANDS','NETHERLANDS ANTILLES','NEW CALEDONIA','NEW ZEALAND','NICARAGUA','NICARAGUA','NIGER','NIUE','NORFOLK ISLAND','NORTHERN MARIANA ISLANDS','NORWAY','OMAN','PALAU','PALESTINE','PANAMA','PARAGUAY','PAPUA NEW GUINEA','PERU','PHILIPPINES','PITCAIRN','POLAND','PORTUGAL','QATAR','REUNION','ROMANIA','REPUBLIC OF SERBIA','RUSSIAN FEDERATION','RWANDA','SAINT HELENA','SAINT KITTS AND NEVIS','SAINT LUCIA','SAINT PIERRE AND MIQUELON','SAINT VINCENT AND THE GRENADINES','SAMOA','SAN MARINO','SAO TOME AND PRINCIPE','SAUDI ARABIA','SENEGAL','SEYCHELLES','SIERRA LEONE','SINGAPORE','SLOVAKIA','SLOVENIA','SOLOMON ISLANDS','SOMALIA','SOUTH AFRICA','SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS','SPAIN','SRI LANKA','SURINAME','SVALBARD AND JAN MAYEN','SWAZILAND','SWEDEN','SWITZERLAND','TAIWAN, PROVINCE OF CHINA','TAJIKISTAN','TANZANIA, UNITED REPUBLIC OF','THAILAND','TIMOR-LESTE','TOGO','TOKELAU','TONGA','TRINIDAD AND TOBAGO','TUNISIA','TURKEY','TURKMENISTAN','TURKS AND CAICOS ISLANDS','TUVALU','UGANDA','UKRAINE','UNITED ARAB EMIRATES','UNITED KINGDOM','UNITED STATES','UNITED STATES MINOR OUTLYING ISLANDS','URUGUAY','UZBEKISTAN','VANUATU');

?>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel" style="font-weight:bold">Registration</h4>
      </div>
      <div class="modal-body">
			<div class="row clearfix">
				<div class="col-md-12 alert alert-info">
					<form role="form" id="paypal" method="POST" action="">
						<input type="hidden" name="eventname" id="eventname">
						<input type="hidden" name="eventid" id="eventid">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label><span class="required">*</span>Country</label>
									<select class="form-control" name="country" value="<?php echo $country; ?>">
										<?php
										foreach($country as $c):
										if($c=="Please select your country."){
										echo '<option value="">'.$c.'</option>';
										continue;
										}	
										echo '<option value="'.$c.'">'.$c.'</option>';
										endforeach;
										?>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label><span class="required">*</span>Credit Card number</label>
									<input type="text"  name="creditcardnum" class="form-control validate[required]" placeholder="required">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="control-label">
								<input type="radio" name="payment_type" value="Visa" checked><img src="wp-content/themes/marketingadmissions/images/icon-visa.png" width="51" style="position:relative; top:10px;">
								<input type="radio" name="payment_type" value="Master Card"><img src="wp-content/themes/marketingadmissions/images/icon-mastercard.png" width="51" style="position:relative; top:10px;">
								<input type="radio" name="payment_type" value="Discover"><img src="wp-content/themes/marketingadmissions/images/icon-discover.png" width="51" style="position:relative; top:10px;">
								<input type="radio" name="payment_type" value="American Express"><img src="wp-content/themes/marketingadmissions/images/icon-american-express.png" width="51" style="position:relative; top:10px;">
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label><span class="required">*</span>Expiration Date</label>
									<input type="text"  name="exp_date" class="form-control validate[required]" placeholder="required, Don't put slashes(/) or dash (-)">
									<!--Don't put slashes(/) or dash (-)-->
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label><span class="required">*</span>CVV</label>
									<input type="text"  name="cvv" class="form-control validate[required]" placeholder="required">
								</div>
							</div>
						</div><br/>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label><span class="required">*</span>First Name</label>
									<input type="text"  name="fname" class="form-control validate[required]" placeholder="required">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label><span class="required">*</span>Last Name</label>
									<input type="text"  name="lname" class="form-control validate[required]" placeholder="required">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label><span class="required">*</span>Address</label>
									<input type="text"  name="address" class="form-control validate[required]" placeholder="required">	
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label><span class="required">*</span>Zip Code</label>
									<input type="text"  name="zip" class="form-control validate[required]" placeholder="required">
								</div>
							</div>
						</div>
						<div class="row">
						<div class="form-group">
							<div class="col-md-6">
								<label><span class="required">*</span> City</label>
								<input type="text" name="city" class="form-control validate[required]" placeholder="Enter city">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6">
								<label><span class="required">*</span> State</label>
								<select name="state" class="form-control validate[required]">
									<?php
										foreach($states as $s):
										if($s=="Please select a state."){
										echo '<option value="">'.$s.'</option>';
										continue;
										}	
										echo '<option value="'.$s.'">'.$s.'</option>';
										endforeach;
									?>
								</select>
							</div>
						</div>
						</div>  
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								<label><span class="required">*</span>Phone</label>
									<input type="text"  name="phone1" class="form-control validate[required]" placeholder="required">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label><span class="required">*</span>Email Address</label>
									<input type="text"  name="email" class="form-control validate[required,custom[email]]" placeholder="required">
								</div>
							</div>
						</div>
						<div style="text-align: center;">
							<button type="submit" name="payment_registration" class="btn btn-default">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
    </div>
  </div>
</div>

