<?php

	get_common(array('config', 'Form', 'DB', 'Session', 'Paypal'));
	get_model(array('Company', 'CompanyDAO', 'Member', 'MemberDAO', 'Admin', 'Event', 'EventDAO'));
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$m_var = new Member();
	$m_dao = new MemberDAO();
	
	$e_var = new Event();
	$e_dao = new EventDAO();
	
	$a_var = new Admin();
	
	//$company_id = Session::get('ID');
	
	$member_id = $_GET['id'];
	
	$company = $c_dao->get("companies", 'company_id='.$member_id);
	$members = $m_dao->get("members", 'company_id='.$member_id);
	// var_dump($members); exit();
	$bio = $c_dao->get("autobiography", 'companyid='.$member_id);
	$gallery = $c_dao->get("facility", 'companyid='.$member_id);
	$pic = $c_dao->get("pictures", 'companyid='.$member_id);
	

?>

<style>
.container .bs-example-tabs .nav-tabs { border-bottom: none; }
.profile-content { float: left; float: left; width: 250px; margin-top: 50px; margin-left: 10px; text-align: center; font-size: 13px; }
.profile-content h1{ text-align: center; color: #51A9FF; }
.profile-content a { text-decoration: none; }
.profile-content a:hover { color: #067B3D; }
.img-container { text-align: center; border: 1px solid #fff; padding-top: 10px; margin-bottom: 10px; }
.backarrow { float: left; margin-top: 10px; margin-left: 10px }
.backarrow a{ background-image:url('<?php bloginfo('template_url') ?>/images/backarrow.png'); height: 20px; width:20px; display:block; }
.backarrow a:hover { background-image:url('<?php bloginfo('template_url') ?>/images/backarrow_hover.png'); }
.profile-pic-container { margin-top: 50px; width: 250px; height: 215px; margin-bottom: 25px; margin-left: 14px; border: 2px solid #E8E8E8; background-color: #E8E8E8; float: left; text-align: center; }
.profile-pic-container a{ text-decoration: none; }
.profile-pic-container a:hover { color: #067B3D; }
.no-upload-pic { text-align: center; }
.no-upload-pic img { margin-left: 5px; margin-right: 5px; margin-top: 10px; margin-bottom: 10px}
.no-upload-pic a{ text-decoration: none; }
.no-upload-pic a:hover{ color: #067B3D; }
.member-list-content { color: #084D52 }
.member-list-content h1{ color: #084D52 }
.member-list-content a{ color: #084D52 }
.member-list-content a:hover{ color: #1AB7DF }
</style>

<div class="clearfix">
	<div class="backarrow">
		<a href="charity-organization-members-directory" title="Go Back to Member's Directory"></a>
	</div>
	<div>
		<div class="profile-pic-container member-list-content">
			<?php	if(!empty($pic)) {
			foreach($pic as $p): ?>
			<div class="col-md-12"><!-- Uploaded profile must display here -->
				<div class="text-left">	
					<img src="<?php bloginfo('template_url'); ?>/systemapplication/uploads/<?php echo $p->pic; ?>" style="margin-top:10px; margin-bottom: 10px;" class="img-responsive img-thumbnail" alt="Responsive image">
				</div>	
			</div>	
			<?php endforeach; }
				else { ?>
			<div class="no-upload-pic">
				<img src="<?php bloginfo('template_url'); ?>/images/profile-avatar.png" class="img-responsive img-thumbnail" alt="Responsive image"><br/>
			</div>
			<?php } ?>
			<?php 
			$ctr = 0;
			foreach($members as $m):
				if($m->cemail != ""){
					if($ctr != 1){
						echo '<a href="mailto:'.$m->cemail.'"><img src="wp-content/themes/marketingadmissions/images/connect-mail-icon.png" alt="img">Connect me to a member</a>';
					}
					$ctr ++;
				}
			endforeach;
			?>
		</div>
		<div class="profile-content member-list-content">	
			<?php foreach($company as $c): ?>
			<h1><?php echo $c->company_name; ?></h1>
			<p>Call us Today! <br/><strong><?php echo $c->company_contactno; ?></strong></p>
			<p><?php echo $c->company_address; ?></p>
			<!--<p><a href="charity-organization-contact-us">Get Pricing</a></p>-->
			<?php endforeach; ?>
			<?php foreach($members as $m): ?>
			<p style="text-align: center;"><?php echo $m->fname; ?>&nbsp;<?php echo $m->lname; ?><br/>Contact Person(s)</p>
			<?php endforeach; ?>
		</div>
	</div>
	<div class="col-md-12 member-list-content">
		<div class="alert-info text-center"><h1>About us</h1></div>
		<div>
			<?php if(!empty($bio)) {
			foreach($bio as $b): ?>
			<div>
				<p><?php echo $b->autobiography; ?></p>
			</div>
			<?php endforeach; 
			} else { ?>
			<div class="text-center">
				<tr><td colspan="6">No records found.</td></tr>
			</div>
			<?php } ?>
		</div>
		<div class="alert-info text-center"><h1>Services</h1></div>
		<div>
			<?php if(!empty($company->services)) { 
				foreach($company as $c): ?>
			<div>
				<p><?php echo $c->services; ?></p>
			</div>
			<?php endforeach;
			} else { ?>
			<div class="text-center">
				<tr><td colspan="6">No records found.</td></tr>
			</div>
			<?php } ?>
		</div>
		<div class="alert-info text-center"><h1>Gallery</h1></div>
		<div>
			<?php if(!empty($gallery)) {
			foreach($gallery as $g): ?>
			<div class="img-container">
				<img src="<?php bloginfo('template_url'); ?>/systemapplication/uploads/facility/<?php echo $g->facility; ?>" class="img-responsive img-thumbnail" alt="Responsive image" style="text-align:center">
			</div>
			<?php endforeach;
			} else { ?>
			<div class="text-center">
				<tr><td colspan="6">No records found.</td></tr>
			</div>
			<?php } ?> 
		</div>	
	</div>	
</div>
