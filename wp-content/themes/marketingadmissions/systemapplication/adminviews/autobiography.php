<?php

	
	get_common(array('config', 'Form', 'DB', 'Session'));
	get_model(array('Company', 'CompanyDAO', 'Member', 'MemberDAO', 'Admin'));
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$m_var = new Member();
	$m_dao = new MemberDAO();
	
	// $company_id = Session::get('ID');
	// $company = $c_dao->getRow("companies", "company_id=$company_id");
	// $member = $m_dao->get("members", 'company_id='.$company_id);
	
	$company = $c_dao->getRow("companies", 'company_id='.$_GET['id']);
	$company_id = $company->company_id;
	$member = $m_dao->get("members", 'company_id='.$company_id);
	
	$bio = $c_dao->getRow("autobiography", "companyid=".$company_id);
	
	// include 'wp-content/themes/marketingadmissions/systemapplication/myprofile.php';
	

?>

<div id="autobiography" class="tab-pane fade"> 
	<p>
		<div class="row alert alert-info clearfix autobiography">
			<form role="form" action="" method="post">
				<div class="form-group">
					<?php if(!empty($bio)) { ?>
					<label>Autobiography</label>
					<textarea name="autobiography" class="form-control" rows="10" placeholder="autobiography" readonly><?php echo $bio->autobiography;?></textarea>
					<?php } else { ?>
					<div class="text-center">
						<tr><td colspan="6">No records found.</td></tr>
					</div>
					<?php } ?>
				</div>
			</form>
		</div>
	</p>
</div>