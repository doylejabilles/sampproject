<?php
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$m_var = new Member();
	$m_dao = new MemberDAO();

	$company = $c_dao->getRow("companies", 'company_id='.$_GET['id']);
	$company_id = $company->company_id;
	$member = $m_dao->get("members", 'company_id='.$company_id);
	
	
	/** Declaration for state **/
	$states = array('Please select a state.','Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District Of Columbia','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Puerto Rico','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virgin Islands','Virginia','Washington','West Virginia','Wisconsin','Wyoming');
	

?>

<div id="profile" class="tab-pane fade active in"> 
	<div class="row">
		<div class="col-md-11  alert alert-info">
			<form role="form" method="POST">
				<div class="form-group">
					<span class="required">*</span><label>Account Type</label>
					<div class="row">
						<div class="col-xs-5">
						  <select name="account_type" class="form-control validate[required]">
							<option <?php if($company->account_type=="Member"){ echo "selected";}?> value="Member">Member</option>
							<option <?php if($company->account_type=="Resource"){ echo "selected";}?> value="Resource">Resource</option>
							<option <?php if($company->account_type=="Mail"){ echo "selected";}?> value="Mail">Mail</option>
						  </select>
						</div>
					</div>
				  </div>
				  <br>
				  <br>
				  <div class="form-group">
					<span class="required">*</span><label>Company Name</label>
					<input type="text" name="company_name" class="form-control validate[required]" placeholder="Enter company name" value="<?php echo $company->company_name;?>">
				  </div>
				  <div class="form-group">
					<span class="required">*</span><label>Address</label>
					<input type="text" name="company_address" class="form-control validate[required]" placeholder="Enter address" value="<?php echo $company->company_address;?>">
				  </div>
				  <div class="row">
					<div class="col-md-6">
						  <div class="form-group">
							<span class="required">*</span><label>City</label>
							<input type="text" name="city" class="form-control validate[required]" placeholder="Enter city" value="<?php echo $company->city;?>">
						  </div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Zip</label>
							<input type="text" name="zip" class="form-control" placeholder="Enter zip" value="<?php echo $company->zip;?>">
						</div>
					</div>
				</div>
				<div class="form-group">
					<span class="required">*</span><label>State</label>
					<select name="state" class="form-control validate[required]" value="<?php echo $company->state;?>">
						<?php
							foreach($states as $s):
								if($s==$company->state){
									echo '<option value="'.$s.'" selected>'.$s.'</option>';
								}else{
									echo '<option value="'.$s.'">'.$s.'</option>';
								}
							endforeach;
						?>
					</select>
				</div>
				<div class="form-group">
					<span class="required">*</span><label>Username</label>
					<input type="text" name="username" class="form-control" placeholder="Enter Username validate[required]" value="<?php echo $company->username;?>" readonly>
				</div>
				<div class="form-group">
					<span class="required">*</span><label>Company Contact Number</label>
					<input type="text" name="company_contactno" class="form-control validate[required]" placeholder="Company Contact Number" value="<?php echo $company->company_contactno;?>">
				</div>
		  <!--<div class="row">
		  <div class="col-md-6">
			  <div class="form-group">
				<label>Password</label>
				<input type="password" readonly name="password" class="form-control validate[required]" placeholder="Password" value="<?php //echo $company->password;?>" disabled>
			  </div>
		  </div>
		  <div class="col-md-6">
			  <div class="form-group">
				<label>Repeat Password</label>
				<input type="password" readonly name="repeat_password" class="form-control validate[required]" placeholder="Repeat Password" value="<?php //echo $company->repeat_password;?>" disabled>
			  </div>
		  </div>
		  </div>-->
			  <div class="form-group">
				<label>Category</label>
				<select name="category" class="form-control" value="<?php echo $company->category;?>">
				  <option value="">Please select a category</option>
				  <option <?php if($company->category=="Retirement Communities"){ echo "selected";}?> value="Retirement Communities">Retirement Communities</option>
				  <option <?php if($company->category=="In-home Services & Caregiver Agencies"){ echo "selected";}?> value="In-home Services & Caregiver Agencies">In-home Services & Caregiver Agencies</option>
				  <option <?php if($company->category=="Legal, Financial & Real Estate"){ echo "selected";}?> value="Legal, Financial & Real Estate">Legal, Financial & Real Estate</option>
				  <option <?php if($company->category=="Senior Care Management"){ echo "selected";}?> value="Senior Care Management">Senior Care Management</option>
				  <option <?php if($company->category=="Residential Placement"){ echo "selected";}?> value="Residential Placement">Residential Placement</option>
				  <option <?php if($company->category=="Other Senior Resources"){ echo "selected";}?> value="Other Senior Resources">Other Senior Resources</option>
				  <option <?php if($company->category=="Community Partners"){ echo "selected";}?> value="Community Partners">Community Partners</option>
				</select>
			  </div>
			  <div class="form-group">
			  <label>Capacity</label>
					<input type="text" name="capacity" class="form-control" placeholder="Capacity">
			  </div>
			  <label>Company Type</label><br><br>
			  <div class="row">
				<div class="col-md-3 col-md-offset-2 validate[required]">
						<?php 
							$company_types = explode(",", $company->company_type);
							$ct = array();
							
							foreach($company_types as $c):
								$ct[] = trim($c);
							endforeach;
							// var_dump($ct);
						?>
					  <input type="checkbox" name="company_type[]" value="CCRC" <?php echo ( in_array("CCRC", $ct) ) ? 'checked': ''; ?>> CCRC		<br>
					  <input type="checkbox" name="company_type[]" value="RCFE" <?php echo ( in_array("RCFE", $ct) ) ? 'checked': ''; ?>> RCFE		<br>
					  <input type="checkbox" name="company_type[]" value="Ind" <?php echo ( in_array("Ind", $ct) ) ? 'checked': ''; ?>> Ind		<br>
					  <input type="checkbox" name="company_type[]" value="Asst Lvg" <?php echo ( in_array("Asst Lvg", $ct) ) ? 'checked': ''; ?>> Asst Lvg	<br>
					  <input type="checkbox" name="company_type[]" value="Day Care" <?php echo ( in_array("Day Care", $ct) ) ? 'checked': ''; ?>> Day Care	<br>
					  <input type="checkbox" name="company_type[]" value="Secured" <?php echo ( in_array("Secured", $ct) ) ? 'checked': ''; ?>> Secured   <br>
					  <input type="checkbox" name="company_type[]" value="SNF" <?php echo ( in_array("SNF", $ct) ) ? 'checked': ''; ?>> SNF   <br>
				</div>
				<div class="col-md-7">
					  <input type="checkbox" name="company_type[]" value="Hospice" <?php  echo ( in_array("Hospice", $ct) ) ? 'checked': '';?>> Hospice          <br>
					  <input type="checkbox" name="company_type[]" value="Home Care" <?php  echo ( in_array("Home Care", $ct) ) ? 'checked': '';?>> Home Care        <br>
					  <input type="checkbox" name="company_type[]" value="Referral Agency" <?php echo ( in_array("Referral Agency", $ct) ) ? 'checked': ''; ?>> Referral Agency  <br>
					  <input type="checkbox" name="company_type[]" value="Board & Care" <?php echo ( in_array("Board & Care", $ct) ) ? 'checked': ''; ?>> Board & Care 	   <br>
					  <input type="checkbox" name="company_type[]" value="Home Health Care" <?php echo ( in_array("Home Health Care", $ct) ) ? 'checked': ''; ?>> Home Health Care <br>
					  <input type="checkbox" name="company_type[]" value="Miscellaneous Support" <?php echo ( in_array("Miscellaneous Support", $ct) ) ? 'checked': ''; ?>> Miscellaneous Support <br>
					  <input type="checkbox" class="check_this">Other<br>
					  <input type="text" class="form-control validate[required] other_shows" name="other_type" value="<?php echo $company->other_type; ?>">
				</div>
			  </div>
			  <div class="row">
					<div class="col-md-6">
					  <div class="form-group">
						<label>Phone number</label>
						<input type="text" name="phone_number" class="form-control" placeholder="Enter phone number" value="<?php echo $company->phone_number;?>">
					  </div>
					</div>
					<div class="col-md-6">
					  <div class="form-group">
						<label>Fax number</label>
						<input type="text" name="fax_number" class="form-control" placeholder="Enter fax number" value="<?php echo $company->fax_number;?>">
					  </div>
					</div>
			  </div>
			  <div class="form-group">
					<label>Website</label>
					<input type="text" name="website" class="form-control" placeholder="Enter Url" value="<?php echo $company->website;?>">
			  </div>
			  <div class="form-group">
					<span class="required">*</span><label>Company Email Address</label>
					<input type="email" name="email_address" class="form-control validate[required]" placeholder="Enter email address" value="<?php echo $company->email_address;?>">
			  </div>
			  <label>Contact Person 1</label>
			  <div class="row">
				<div class="col-md-4">
				  <div class="form-group">
					<label><span class="required">*</span>First Name</label>
					<input type="text" name="fname[]" class="form-control validate[required]" placeholder="First name" value="<?php echo $member[0]->fname;?>">
				  </div>
				</div>
				<div class="col-md-4">
				  <div class="form-group">
					<label>Middle Name</label>
					<input type="text" name="mname[]" class="form-control" placeholder="Middle name" value="<?php echo $member[0]->mname;?>">
				  </div>
				</div>
				<div class="col-md-4">
				  <div class="form-group">
					<label><span class="required">*</span>Last Name</label>
					<input type="text" name="lname[]" class="form-control validate[required]" placeholder="Last name" value="<?php echo $member[0]->lname;?>">
				  </div>
				</div>
			  </div>
			<div class="row">
				<div class="col-md-6">
				  <div class="form-group">
					<label>Title</label>
					<input type="text" name="title[]" class="form-control" placeholder="Title" value="<?php echo $member[0]->title;?>">
				  </div>
				</div>
				<div class="col-md-6">
				  <div class="form-group">
					<label><span class="required">*</span>Email</label>
					<input type="text" name="cemail[]" class="form-control validate[required]" placeholder="Email" value="<?php echo $member[0]->cemail;?>">
				  </div>
				</div>
			</div>
			<label>Contact Person 2</label>
			<div class="row">
				<div class="col-md-4">
				  <div class="form-group">
					<label>First Name</label>
					<input type="text" name="fname[]" class="form-control" placeholder="First name" value="<?php echo $member[1]->fname;?>">
				  </div>
				</div>
				<div class="col-md-4">
				  <div class="form-group">
					<label>Middle Name</label>
					<input type="text" name="mname[]" class="form-control" placeholder="Middle name" value="<?php echo $member[1]->mname;?>">
				  </div>
				</div>
				<div class="col-md-4">
				  <div class="form-group">
					<label>Email</label>
					<input type="text" name="lname[]" class="form-control" placeholder="Last name" value="<?php echo $member[1]->lname;?>">
				  </div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
				  <div class="form-group">
					<label>Title</label>
					<input type="text" name="title[]" class="form-control" placeholder="Title" value="<?php echo $member[1]->title;?>">
				  </div>
				</div>
				<div class="col-md-6">
				  <div class="form-group">
					<label>Email</label>
					<input type="text" name="cemail[]" class="form-control" placeholder="Email" value="<?php echo $member[1]->cemail;?>">
				  </div>
				</div>
			</div><br><br>
			<div class="form-group">
				<label>Alz</label>
				<div>
					<input type="text" name="alz" class="form-control" placeholder="Alz" value="<?php echo $company->alz;?>">
				</div>
			</div>
			<div class="form-group">
				<label>Other</label>
				<div>
					<textarea name="other" class="form-control" rows="3" placeholder="Other" value="<?php echo $company->other;?>"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label>Description</label>
				<div>
					<textarea name="description" class="form-control" rows="3" placeholder="Description" value="<?php echo $company->description;?>"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label>Professional License Number</label>
				<div>
					<textarea name="res_qual" class="form-control" rows="3" placeholder="Professional License Number" value="<?php echo $company->res_qual;?>"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label>Private</label>
				<div>
					<select name="private1" class="form-control">
						<option value="No" <?php echo $company->company_type=="No" ? 'selected': ''; ?>>No</option>
						<option value="Yes" <?php echo $company->company_type=="Yes" ? 'selected': ''; ?>>Yes</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label>SSI</label>
				<div>
					<select name="ssi" class="form-control">
						<option value="No" <?php echo $company->company_type=="No" ? 'selected': ''; ?>>No</option>
						<option value="Yes" <?php echo $company->company_type=="Yes" ? 'selected': ''; ?>>Yes</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label>Medi-Cal</label>
				<div>
					<input type="text" name="medi_cal" class="form-control" placeholder="Medi-Cal" value="<?php echo $company->medi_cal;?>">
				</div>
			</div>
			<div class="form-group">
				<label>Medicare</label>
				<div>
					<input type="text" name="medicare" class="form-control" placeholder="Medicare" value="<?php echo $company->medicare;?>">
				</div>
			</div>
			<div class="form-group">
				<label>Other 2</label>
				<div>
					<textarea name="other2" class="form-control" rows="3" placeholder="Other 2" value="<?php echo $company->other2;?>"></textarea>
				</div>
			</div>
			<div style="text-align: center;">
				<input type="submit" class="btn btn-default" name="btnedit" value="Update">
			</div>
			</form>
		</div>
	</div>
</div>