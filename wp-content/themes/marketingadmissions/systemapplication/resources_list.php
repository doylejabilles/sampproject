<?php	
	get_common(array('config', 'Form', 'DB', 'Session'));
	get_model(array('Company', 'CompanyDAO'));
	get_model(array('Member', 'MemberDAO'));
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$m_var = new Member();
	$m_dao = new MemberDAO();
	$companies = $c_dao->get('companies',"account_type='Resource'",'company_name','ASC');
	
	$active = ( isset($_SESSION['ACTIVE']) && ($_SESSION['ACTIVE'] == 1) ) ? true : false;
	$type = ( isset($_SESSION['TYPE']) && ($_SESSION['TYPE'] == "member") ) ? 'member' : 'admin';
	
?>
<div class="cleartext member-list-content">
	<div class="member-list">
		<h1>Resources List</h1>
		<p>
		<?php foreach($companies as $c): ?>
			<?php $members = $m_dao->get('members', 'company_id='.$c->company_id); ?>
			
			<?php if($active == true ): ?>
			<a target="_blank" href="charity-organization-admin-edit-user?id=<?php echo $c->company_id; ?>" id="<?php echo $c->company_id; ?>" class="editinfo" title="View/Edit"><img src="<?php bloginfo('template_url') ?>/images/edit_icon.png" alt="img"/></a>&nbsp;&nbsp;
			<a id="<?php echo $c->company_id; ?>" class="delete_resource" href="<?php bloginfo('template_url'); ?>/systemapplication/ajaxadmin.php" blog-info="<?php bloginfo('template_url'); ?>" title="Delete"><img src="<?php bloginfo('template_url') ?>/images/delete_icon.png" alt="img"/></a>
			
			<?php endif; ?>
			&nbsp;&nbsp;<a href="charity-organization-member-profile?id=<?php echo $c->company_id; ?>"><?php echo $c->company_name; ?></a><br>
			<?php 
				// if(!empty($c->company_address))
					// echo $c->company_address.'<br/>';
			?>
			<?php 
				// if(!empty($c->company_contactno))
					// echo $c->company_contactno.'<br/>';
			?>
			<?php 
				// if(!empty($c->zip))
					// echo $c->zip.'<br/>';
			?>
			<?php 
				// if(!empty($c->category))
					// echo $c->category.'<br/>';
			?>
			<?php //foreach($members as $m):
					// echo ucfirst($m->fname).' '.ucfirst($m->lname).', '.$m->title.' '.$m->contact_no.'<br>';
				  // endforeach; ?>
				  <br>
		<?php endforeach; ?>
		</p>
	</div>
</div>