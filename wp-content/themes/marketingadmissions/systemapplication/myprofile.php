<?php 
	// error_reporting(E_ALL);
	// ini_set('display_errors', 1);
	
	get_common(array('config', 'Form', 'DB', 'Session'));
	get_model(array('Company', 'CompanyDAO', 'Member', 'MemberDAO', 'Admin'));
	
	include 'wp-content/themes/marketingadmissions/forms/form_details.php';
	if($smtp)
	include 'wp-content/themes/marketingadmissions/forms/phpmailer/sendmail.php';
	
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$m_var = new Member();
	$m_dao = new MemberDAO();
	
	// echo $userid = $_SESSION['ID'];
	
	$company_id = Session::get('ID');
	$company = $c_dao->getRow("companies", "company_id=$company_id");
	$member = $m_dao->get("members", 'company_id='.$company_id);
	
	// var_dump($member); exit();
	$regexp = "/^[a-z0-9]+([_\\.-][a-z0-9]+)*@([a-z0-9]+([\.-][a-z0-9]+)*)+\\.[a-z]{2,}$/i";
	
	$filename = "";
	$filename1 = "";
	// $scr = "";
	$abc = "";
	$errors = array();
	$error = '';
	$success = '';
	
	
	/*function for uploading profile picture*/
	define ("MAX_SIZE","10485760"); 
	
	function getExtension($str) {
		$i = strrpos($str, ".");
		if(!$i) {
			return "";
		}
		$l = strlen($str) - $i;
		$ext = substr($str, $i + 1, $l);
		return $ext;
	}

	if($_SERVER["REQUEST_METHOD"] == "POST") {
		$image = $_FILES["profile_pic"]["name"];
		$uploadedfile = $_FILES['profile_pic']['tmp_name'];

		if ($image) {
			$filename = stripslashes($_FILES['profile_pic']['name']);

			$extension = getExtension($filename);
			$extension = strtolower($extension);


			if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) {
				$errors[] = 'Please upload a photo!';
				$error = implode('<br/>', $errors);
			}
			else {
				$size = filesize($_FILES['profile_pic']['tmp_name']);

				if ($size > MAX_SIZE*1024) {
					$errors[] = 'The photo you wish to upload is beyond the maximum 2MB limit!';
				}
				
				if( count($errors) == 0 ) {

					if($extension == "jpg" || $extension == "jpeg" ) {
						$uploadedfile = $_FILES['profile_pic']['tmp_name'];
						$src = imagecreatefromjpeg($uploadedfile);
					}
					else if($extension=="png") {
						$uploadedfile = $_FILES['profile_pic']['tmp_name'];
						$src = imagecreatefrompng($uploadedfile);

					} else  {
						$src = imagecreatefromgif($uploadedfile);
					}

					// echo $scr;

					list($width,$height) = getimagesize($uploadedfile);


					$newwidth = 300;
					$newheight = ($height/$width)*$newwidth;
					$tmp = imagecreatetruecolor($newwidth,$newheight);


					$newwidth1 = 150;
					$newheight1 = ($height/$width)*$newwidth1;
					$tmp1 = imagecreatetruecolor($newwidth1,$newheight1);

					imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

					imagecopyresampled($tmp1,$src,0,0,0,0,$newwidth1,$newheight1,$width,$height);

					// $filename = "wp-content/themes/marketingadmissions/systemapplication/uploads/". date("mdyhis") .$_FILES['file']['name'];
					$filename = "wp-content/themes/marketingadmissions/systemapplication/uploads/". date("mdyhis") . '.' . $extension;
					$img_newname = date("mdyhis") . '.' . $extension;
					
					// $filename1 = "wp-content/themes/marketingadmissions/systemapplication/uploads/thumbnails/". date("mdyhis") . $_FILES['file']['name'];
					$filename1 = "wp-content/themes/marketingadmissions/systemapplication/uploads/thumbnails/thumb_". date("mdyhis") . '.' . $extension;
					$img_newname1 = "thumb_". date("mdyhis") . '.' . $extension;

					imagejpeg($tmp,$filename,100);

					imagejpeg($tmp1,$filename1,100);

					imagedestroy($src);
					imagedestroy($tmp);
					imagedestroy($tmp1);
					
				} else {
					$error = implode('<br/>', $errors);
				}
			}
		}
	}
	
	//If no errors registred, print the success message
	if(isset($_POST['btnupdate']) && (count($errors) == 0) ) {
		$c_var->pic = $img_newname;
		$c_var->companyid = $company_id;
		
		// member picture
		$m_pic = $c_dao->getRow('pictures', 'companyid='.$c_var->companyid);
		
		@unlink('wp-content/themes/marketingadmissions/systemapplication/uploads/'.$m_pic->pic);
		@unlink('wp-content/themes/marketingadmissions/systemapplication/uploads/thumbnails/thumb_'.$m_pic->pic);
		
		/* checks for existing record then update
		 if no record, then insert */
		$c_dao->save('pictures', 'companyid', $c_var);
		$success = 'Image Uploaded Successfully!';
	}
		
	/*function for updating profile info*/
	if( isset($_POST['btnupdateprofile']) && ($_POST['btnupdateprofile'] == 'Update') ) {
		$c_var->company_id = Session::get('ID');
		
		$c_var->account_type = Form::data('account_type');
		$c_var->company_name = Form::data('company_name');
		$c_var->company_address = Form::data('company_address');
		$c_var->company_contactno = Form::data('company_contactno');
		$c_var->username = Form::data('username');
		$c_var->password = Form::data('password');
		$c_var->repeat_password = Form::data('repeat_password');
		$c_var->city = Form::data('city');
		$c_var->state = Form::data('state');
		$c_var->zip = Form::data('zip');
		$c_var->category = Form::data('category');
		$c_var->capacity = Form::data('capacity');
		$temp_companytype = $_POST['company_type'];
		$c_var->other_type = Form::data('other_type');
		$c_var->phone_number = Form::data('phone_number');
		$c_var->fax_number = Form::data('fax_number');
		$c_var->website = Form::data('website');
		$c_var->email_address = Form::data('email_address');
		$c_var->alz = Form::data('alz');
		$c_var->other = Form::data('other');
		$c_var->description = Form::data('description');
		$c_var->res_qual = Form::data('res_qual');
		$c_var->private1 = Form::data('private1');
		$c_var->ssi = Form::data('ssi');
		$c_var->medi_cal = Form::data('medi_cal');
		$c_var->medicare = Form::data('medicare');
		$c_var->other2 = Form::data('other2');	
		
		$m_var->fname = $_POST['fname'];
		$m_var->mname = $_POST['mname'];
		$m_var->lname = $_POST['lname'];
		$m_var->cemail = $_POST['cemail'];
		$m_var->title = $_POST['title'];
		// print_r($m_var); exit();
		
		$c_var->company_type = "";
		foreach($temp_companytype as $t):
			$c_var->company_type .= $t.', ';
		endforeach;
		
		//validation of fields
		if(empty($c_var->account_type) || empty($c_var->company_name) || empty($c_var->company_address) || empty($c_var->company_contactno) || empty($c_var->username) || empty($c_var->password) || empty($c_var->repeat_password) || empty($c_var->city) || empty($c_var->state) || empty($c_var->email_address) ) {
			$errors[] = 'Please fill up the required fields';
		} 
		// validates if no contact person is entered. should input at least 1 contact person
		elseif( empty($m_var->fname) || empty($m_var->lname) || empty($m_var->cemail) ) {
			$errors[] = 'Please enter at least one contact person.';
		}
		
		//checking if the email is address is valid
		if( !preg_match($regexp, $c_var->email_address) ) { 
			$errors[] = 'Invalid Email address.';
	    }
			
		//checking if the password matched
		if( $c_var->password != $c_var->repeat_password){
			$errors[] = 'Your password did not matched!';
		}
		
		//counting all the errors
		if( count($errors) > 0 ) {
			$error = implode('<br/>', $errors);	
		} else {
			$c_var->company_id = $company_id = Session::get('ID');
			$c_dao->updateProfile($c_var, "companies");
			//$c_dao->updateTemp($c_var, "temp_companies", 'company_id');
			
			
			if(!empty($member[1]->fname)){
				//If there are 2 contact persons
				$member_ids = array($member[0]->member_id, $member[1]->member_id);
				var_dump($member_ids); exit;
				for($i = 0; $i < 2; $i++) {
					// echo $_POST['fname'][$i].' '.$_POST['lname'][$i].' '.$_POST['title'][$i].' '.$_POST['cemail'][$i].'<br>'; 
					// exit;
					if( !empty($_POST['fname'][$i]) && !empty($_POST['lname'][$i]) && !empty($_POST['cemail'][$i]) ) {
						$m_var->member_id = $member_ids[$i];
						$m_var->company_id = $company_id;
						$m_var->fname = $_POST['fname'][$i];
						$m_var->mname = $_POST['mname'][$i];
						$m_var->lname = $_POST['lname'][$i];
						$m_var->title = $_POST['title'][$i];
						$m_var->cemail = $_POST['cemail'][$i];
						$m_dao->updateMember($m_var, "members");
					}else{
						$m_dao->delete('members', "WHERE member_id=".$member_ids[$i]);
					}
				} 
			} else {
				//If there is only one contact person
				$member_id = $member[0]->member_id;
				if(!empty( $_POST['fname'][0]) && !empty($_POST['lname'][0]) && !empty($_POST['cemail'][0]) ) {
					$m_var->member_id = $member_id;
					$m_var->company_id = $company_id;
					$m_var->fname = $_POST['fname'][0];
					$m_var->mname = $_POST['mname'][0];
					$m_var->lname = $_POST['lname'][0];
					$m_var->title = $_POST['title'][0];
					$m_var->cemail = $_POST['cemail'][0];
					$m_dao->updateMember($m_var, "members");
				}
				if(!empty($_POST['fname'][1]) && !empty($_POST['lname'][1]) && !empty($_POST['cemail'][1]) ) {
					$m_var->company_id = $company_id;
					$m_var->fname = $_POST['fname'][1];
					$m_var->mname = $_POST['mname'][1];
					$m_var->lname = $_POST['lname'][1];
					$m_var->title = $_POST['title'][1];
					$m_var->cemail = $_POST['cemail'][1];
					$m_dao->addMember($m_var, "members");
				}
			}
			echo '<script>alert("Profile has been successfully updated!"); window.location = "charity-organization-my-profile";</script>';
			// echo '<script>location.reload();</script>';				

			//sending email notification
			
			$company_id = Session::get('ID');
			$profile_update1 = $m_var->fname = $_POST['fname'][0];
			$profile_update2 = $m_var->mname = $_POST['mname'][0];
			$profile_update3 = $m_var->lname = $_POST['lname'][0];
			$profile_update4 = $c_var->company_name = $_POST['company_name'];
			$profile_update5 = $c_dao->get("companies", "company_id=$company_id");
			$profile_update6 = $c_dao->get("temp_companies", "company_id=$company_id");	
			// print_r($profile_update5); exit();
			
			
			$body = $profile_update1 . ' ' . ' ' . $profile_update2 . ' ' . $profile_update3 . ' of ' . $profile_update4 . ' has requested to update profile, please login to your acount to approve pending request! <br/><br/> <span style="font-weight: bold;">Current Profile</span> <br /> <br />';
					
				foreach ($profile_update5 as $p) {
				
					$body .='<table style="text-align:left; width:100%;">';
								$body .= '<tr>' . '<td>' . 'Account Type: ' . $p->account_type . '</td>' . '</tr>';
								$body .= '<tr>' .'<td>' . 'Company: ' . $p->company_name . '</td>' . '</tr>';
								$body .= '<tr>' .'<td>' . 'Company Address: ' . $p->company_address . '</td>' . '</tr>';
								$body .= '<tr>' .'<td>' . 'Company Contact Number: ' . $p->company_contactno . '</td>' . '</tr>';
								$body .= '<tr>' .'<td>' . 'City: ' . $p->city . '</td>' . '</tr>';
								$body .= '<tr>' .'<td>' . 'State: ' . $p->state . '</td>' . '</tr>';
								$body .= '<tr>' .'<td>' . 'Zip: ' . $p->zip . '</td>' . '</tr>';
								$body .= '<tr>' .'<td>' . 'Category: ' . $p->category . '</td>' . '</tr>';
								$body .= '<tr>' .'<td>' . 'Company Type: ' . $p->company_type . '</td>' . '</tr>';
								$body .= '<tr>' .'<td>' . 'Phone Number: ' . $p->phone_number . '</td>' . '</tr>';
								$body .= '<tr>' .'<td>' . 'Fax Number: ' . $p->fax_number . '</td>' . '</tr>';
								$body .= '<tr>' .'<td>' . 'Website: ' . $p->website . '</td>' . '</tr>';
								$body .= '<tr>' .'<td>' . 'Email Address: ' . $p->email_address . '</td>' . '</tr>';
								$body .= '<tr>' .'<td>' . 'Alz: ' . $p->alz . '</td>' . '</tr>';
								$body .= '<tr>' .'<td>' . 'Other: ' . $p->other . '</td>' . '</tr>';
								$body .= '<tr>' .'<td>' . 'Description: ' . $p->description . '</td>' . '</tr>';
								$body .= '<tr>' .'<td>' . 'Res qual: ' . $p->res_qual . '</td>' . '</tr>';
								$body .= '<tr>' .'<td>' . 'Private: ' . $p->private1 . '</td>' . '</tr>';
								$body .= '<tr>' .'<td>' . 'SSI: ' . $p->ssi . '</td>' . '</tr>';
								$body .= '<tr>' .'<td>' . 'Medi-cal: ' . $p->medi_cal . '</td>' . '</tr>';
								$body .= '<tr>' .'<td>' . 'Medicare: ' . $p->medicare . '</td>' . '</tr>';
								$body .= '<tr>' .'<td>' . 'Other: ' . $p->other2 . '</td>' . '</tr>';
					$body .='</table>';	
				}
			
			$body .= $profile_update1 . ' ' . $profile_update2 . ' ' . $profile_update3 . ' ' . $profile_update4 . ' ' . 'requested to <br /><br /><span style="font-weight: bold;">Pending for approval profile update</span><br /><br />' ;
			
				foreach($profile_update6 as $tp){
					
					$body .='<table style="text-align:left; width:100%;">';
								$body .= '<tr>' . '<td>' . 'Account Type: ' . $tp->account_type . '</td>' . '</tr>'; 
								$body .= '<tr>' . '<td>' . 'Company: ' . $tp->company_name . '</td>' . '</tr>';
								$body .= '<tr>' . '<td>' . 'Company Address: ' . $tp->company_address . '</td>' . '</tr>';	
								$body .= '<tr>' . '<td>' . 'Company Contact Number: ' . $tp->company_contactno . '</td>' . '</tr>';	
								$body .= '<tr>' . '<td>' . 'City: ' . $tp->city . '</td>' . '</tr>';
								$body .= '<tr>' . '<td>' . 'State: ' . $tp->state . '</td>' . '</tr>';
								$body .= '<tr>' . '<td>' . 'Zip: ' . $tp->zip . '</td>' . '</tr>';
								$body .= '<tr>' . '<td>' . 'Category: ' . $tp->category . '</td>' . '</tr>';
								$body .= '<tr>' . '<td>' . 'Company Type: ' . $tp->company_type . '</td>' . '</tr>';								
								$body .= '<tr>' . '<td>' . 'Phone Number: ' . $tp->phone_number . '</td>' . '</tr>';								
								$body .= '<tr>' . '<td>' . 'Fax Number: ' . $tp->fax_number . '</td>' . '</tr>';								
								$body .= '<tr>' . '<td>' . 'Website: ' . $tp->website . '</td>' . '</tr>';								
								$body .= '<tr>' . '<td>' . 'Email Address: ' . $tp->email_address . '</td>' . '</tr>';								
								$body .= '<tr>' . '<td>' . 'Alz: ' . $tp->alz . '</td>' . '</tr>';								
								$body .= '<tr>' . '<td>' . 'Other: ' . $tp->other . '</td>' . '</tr>';								
								$body .= '<tr>' . '<td>' . 'Description: ' . $tp->description . '</td>' . '</tr>';								
								$body .= '<tr>' . '<td>' . 'Account Type: ' . $tp->res_qual . '</td>' . '</tr>';								
								$body .= '<tr>' . '<td>' . 'Account Type: ' . $tp->private1 . '</td>' . '</tr>';								
								$body .= '<tr>' . '<td>' . 'Account Type: ' . $tp->ssi . '</td>' . '</tr>';								
								$body .= '<tr>' . '<td>' . 'Account Type: ' . $tp->medi_cal . '</td>' . '</tr>';								
								$body .= '<tr>' . '<td>' . 'Account Type: ' . $tp->medicare . '</td>' . '</tr>';								
								$body .= '<tr>' . '<td>' . 'Account Type: ' . $tp->other2 . '</td>' . '</tr>';								
					$body .='</table>';
				}
			
			$subject = $dcomp . "[ Account Updated ]";		
			
			/************** for smtp ***********/
			if($smtp) { 
				$mail = new SendMail($host, $username, $password);
				 // $to_email = 'doyle@proweaver.net';
				// $to_email = 'Info@maps-sgv.org';
				$to_email = 'info@maps-sgv.org';
				$trysend = $mail->sendNow($to_email, $to_name, $cc, $bcc, $from_email, $from_name, $subject, $body);
				if ($trysend == 'ok')
					$sent = true;
				else
					$sent = false;
			}else {
			/************** for mail function ***********/
				$headers= 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= "From: ".$from_name." <".$from_email.">\n";
				$headers .= "Cc: ".$cc."\n";
				$headers .= "Bcc: ".$bcc;
				if(mail($to_email, $subject, $body, $headers)) {
					$sent = true;				
				}else {
					$sent = false;
				}
			}
			
			if($sent) {
					// $prompt_message = '<div id="success">Status update has been successfully sent to your email.</div>';
					 // echo '<script>alert("Profile has been successfully updated!</script>';
					 echo '<script>alert("Status update has been successfully sent to the admin!"); window.location = "charity-organization-my-profile";</script>';
					 // echo '<script>location.reload(); </script>';
					unset($_POST);
			}else {
					$prompt_message = '<div id="error">Failed to send email.</div>';				
			}
			// Ending ni sa email notification
		}	
	}
	 
	/*function for adding autobiography*/
	if( isset($_POST['btnbiography'])) {
	
		// echo "sadas"; exit();
		$c_var->companyid = $company_id;		
		$c_var->autobiography = Form::data('autobiography');
		
		$c_dao->save('autobiography', 'companyid', $c_var);
		$success = 'Autobiography has been successfully updated!';
	}
	
	/*function for adding ads and banners*/
	if($_POST) {
		$btnyourad = Form::data('btnads');
		if(isset($btnyourad) && ($btnyourad == 'Submit Ad') ) {
			$image = $_FILES["ads"]["name"];
			$uploadedfile = $_FILES['ads']['tmp_name'];

			if ($image) {
				$filename = stripslashes($_FILES['ads']['name']);

				$extension = getExtension($filename);
				$extension = strtolower($extension);


				if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) {
					$errors[] = 'Please upload a photo!';
					$error = implode('<br/>', $errors);
				}
				else {
					$size = filesize($_FILES['ads']['tmp_name']);

					if ($size > MAX_SIZE*5242880) {
						$errors[] = 'The photo you wish to upload is beyond the maximum 5MB limit!';
					}
					
					if( count($errors) == 0 ) {

						if($extension == "jpg" || $extension == "jpeg" ) {
							$uploadedfile = $_FILES['ads']['tmp_name'];
							$src = imagecreatefromjpeg($uploadedfile);
						}
						else if($extension=="png") {
							$uploadedfile = $_FILES['ads']['tmp_name'];
							$src = imagecreatefrompng($uploadedfile);

						} else  {
							$src = imagecreatefromgif($uploadedfile);
						}

						// echo $scr;

						list($width,$height) = getimagesize($uploadedfile);


						$newwidth = 320;
						$newheight = ($height/$width)*$newwidth;
						$tmp = imagecreatetruecolor($newwidth,$newheight);


						$newwidth1 = 160	;
						$newheight1 = ($height/$width)*$newwidth1;
						$tmp1 = imagecreatetruecolor($newwidth1,$newheight1);

						imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

						imagecopyresampled($tmp1,$src,0,0,0,0,$newwidth1,$newheight1,$width,$height);

						// $filename = "wp-content/themes/marketingadmissions/systemapplication/uploads/". date("mdyhis") .$_FILES['file']['name'];
						$filename = "wp-content/themes/marketingadmissions/systemapplication/uploads/ads/". date("mdyhis") . '.' . $extension;
						$img_newname = date("mdyhis") . '.' . $extension;
						
						// $filename1 = "wp-content/themes/marketingadmissions/systemapplication/uploads/thumbnails/". date("mdyhis") . $_FILES['file']['name'];
						$filename1 = "wp-content/themes/marketingadmissions/systemapplication/uploads/ads/thumbnails/thumb_". date("mdyhis") . '.' . $extension;
						$img_newname1 = "thumb_". date("mdyhis") . '.' . $extension;

						imagejpeg($tmp,$filename,100);

						imagejpeg($tmp1,$filename1,100);

						imagedestroy($src);
						imagedestroy($tmp);
						imagedestroy($tmp1);
						
					} else {
						$error = implode('<br/>', $errors);
					}
				}
			}
			
			$c_var->ads = $img_newname;
			$c_var->companyid = $company_id;
			
			// member ads
			$m_ads = $c_dao->getRow('ads', 'companyid='.$c_var->companyid);
			
			@unlink('wp-content/themes/marketingadmissions/systemapplication/uploads/ads'.$m_ads->ads);
			@unlink('wp-content/themes/marketingadmissions/systemapplication/uploads/ads/thumbnails/thumb_'.$m_ads->ads);
			
			// checks for existing record then update
			// if no record, then insert
			$c_dao->save('ads', 'companyid', $c_var);
			
			$success = 'You have submitted your ad successfully!';
		}
		
		//function for uploading facility
		$c_var->companyid = $company_id;
		
		$btnfacility = Form::data('btn-facility');
		if(isset($btnfacility) && ($btnfacility == 'Upload') ) {
		
			$all_images = $c_dao->get('facility', 'companyid='.$c_var->companyid . '', '', 0, 3); 
		// print_r($c_dao); exit();
			if( count($all_images) < 3 ) {
				$image = $_FILES["facility"]["name"];
				$uploadedfile = $_FILES['facility']['tmp_name'];
				if ($image) {
					$filename = stripslashes($_FILES['facility']['name']);

					$extension = getExtension($filename);
					$extension = strtolower($extension);


					if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) {
						$errors[] = 'Please upload a photo!';
						$error = implode('<br/>', $errors);
					}
					else {
						$size = filesize($_FILES['facility']['tmp_name']);

						if ($size > MAX_SIZE*10485760) {
							$errors[] = 'The photo you wish to upload is beyond the maximum 10MB limit!';
						}
						
						if( count($errors) == 0 ) {

							if($extension == "jpg" || $extension == "jpeg" ) {
								$uploadedfile = $_FILES['facility']['tmp_name'];
								$src = imagecreatefromjpeg($uploadedfile);
							}
							else if($extension=="png") {
								$uploadedfile = $_FILES['facility']['tmp_name'];
								$src = imagecreatefrompng($uploadedfile);

							} else  {
								$src = imagecreatefromgif($uploadedfile);
							}

							// echo $scr;

							list($width,$height) = getimagesize($uploadedfile);


							$newwidth = 320;
							$newheight = ($height/$width)*$newwidth;
							$tmp = imagecreatetruecolor($newwidth,$newheight);


							$newwidth1 = 160	;
							$newheight1 = ($height/$width)*$newwidth1;
							$tmp1 = imagecreatetruecolor($newwidth1,$newheight1);

							imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

							imagecopyresampled($tmp1,$src,0,0,0,0,$newwidth1,$newheight1,$width,$height);

							// $filename = "wp-content/themes/marketingadmissions/systemapplication/uploads/". date("mdyhis") .$_FILES['file']['name'];
							$filename = "wp-content/themes/marketingadmissions/systemapplication/uploads/facility/". date("mdyhis") . '.' . $extension;
							$img_newname = date("mdyhis") . '.' . $extension;
							
							// $filename1 = "wp-content/themes/marketingadmissions/systemapplication/uploads/thumbnails/". date("mdyhis") . $_FILES['file']['name'];
							$filename1 = "wp-content/themes/marketingadmissions/systemapplication/uploads/facility/thumbnails/thumb_". date("mdyhis") . '.' . $extension;
							$img_newname1 = "thumb_". date("mdyhis") . '.' . $extension;

							imagejpeg($tmp,$filename,100);

							imagejpeg($tmp1,$filename1,100);

							imagedestroy($src);
							imagedestroy($tmp);
							imagedestroy($tmp1);
							
						} else {
							$error = implode('<br/>', $errors);
						}
					}
			
					//condition here
					//$c_dao->save('pictures', 'companyid', $c_var);
					$c_var->facility = $img_newname;
					$c_var->companyid = $company_id;
					$m_facility = $c_dao->getRow('facility', 'companyid='.$c_var->companyid);
					
					// @unlink('wp-content/themes/marketingadmissions/systemapplication/uploads/facility'.$m_facility->facility);
					// @unlink('wp-content/themes/marketingadmissions/systemapplication/uploads/facility/thumbnails/thumb_'.$m_facility->facility);
					
					//initiating upload
					$c_dao->addfacility("facility", $c_var);
					
					$success = 'You have submitted your facility successfully!';
				}
				else {
					// $error = 'Only 3 pictures are allowed.';
					echo '<script>alert("You can only upload a maximum of 3 images");</script>';
				}
			}
		
		}
	}
	
	
	
	$banner = $c_dao->getRow("ads", "companyid=".$company_id);
	$bio = $c_dao->getRow("autobiography", "companyid=".$company_id);
	$profile_pic = $c_dao->getRow("pictures", "companyid=".$company_id);
		
	/** Declaration for state **/
	$states = array('Please select a state.','Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District Of Columbia','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Puerto Rico','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virgin Islands','Virginia','Washington','West Virginia','Wisconsin','Wyoming');
?>


<!--<link rel="stylesheet" href="<?php //bloginfo('template_url'); ?>/css/media.css">-->
<!--<link rel="stylesheet" href="wp-content/themes/marketingadmissions/css/media.css">-->
<script type="text/javascript">

</script>
<style>

/*.profile-photo { width:150px; height:150px; border: 2px solid; float:left; }*/
.autobiography { height: auto; width: auto; margin: 15px auto; padding:15px; }
.container .bs-example-tabs .nav-tabs { border-bottom: none; }
.link-container { position: absolute; top: 20px; right: 200px; }

@media only screen 
and (max-width : 980px){
	.link-container { position: absolute; top: 20px; right: 470px; }
}

@media only screen 
and (max-width : 761px){
	.link-container { position: absolute; top: 20px; right: 270px; }
}

@media only screen 
and (max-width : 734px){
	.link-container { position: absolute; top: 20px; right: 230px; }	
}

@media only screen 
and (max-width : 678px){
	.link-container { position: absolute; top: 20px; right: 175px; }	
}

@media only screen 
and (max-width : 653px){
	.link-container { position: absolute; top: 20px; right: 145px; }	
}

@media only screen 
and (max-width : 530px){
	.link-container { position: absolute; top: 20px; right: 40px; }	
}

@media only screen 
and (max-width : 525px){
	.link-container { position: absolute; top: 20px; right: 0px; }	
}

@media only screen 
and (max-width : 520px){
	.link-container { position: absolute; top: 20px; right: 0px; }	
}

.member-list-content { color: #084D52 }
.member-list-content h1{ color: #084D52 }
.member-list-content a{ color: #084D52 }
.member-list-content label{ color: #084D52 }
.member-list-content a:hover{ color: #1AB7DF }

</style>

<div class="link-container">
	<!--<a href="charity-organization-event-registration" target="_blank">View Event</a>-->
</div>
<div class="container row clearfix member-list-content" style="width: 100%; display: block;">
	<div class="col-md-12" id="notify"><?php echo (!empty($error)) ? Form::error($error) : ''; echo (!empty($success)) ? Form::success($success) : ''; ?></div>
	<div class="col-md-6"><!-- Uploaded profile must display here -->
	<?php
	if(!empty($profile_pic)) {
	?>
		<img src="<?php bloginfo('template_url'); ?>/systemapplication/uploads/<?php echo $profile_pic->pic; ?>" class="img-responsive img-thumbnail" alt="Responsive image">
		<!--<div class="profile-photo"></div>-->
	<?php
	}
	?>
	</div>
	<div class="col-md-6">
		<form role="form" id="myprofile" method="POST" enctype="multipart/form-data" >
		<div class="form-group">
			<div class="col-md-12">
				<label for="file">Profile Picture</label>
				<input type="file" name="profile_pic" name="image" id="profile_pic"><br>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-12">
				<input type="submit" name="btnupdate" class="btn btn-primary" value="Submit">
			</div>
		</div>
		</form>
	</div>
	<div class="bs-example bs-example-tabs col-md-12">
		<ul role="tablist" class="nav nav-tabs" id="myTab">
			<li class="active"><a data-toggle="tab" role="tab" href="#profile">Profile</a></li>
			<li class=""><a data-toggle="tab" role="tab" href="#autobiography">About Us</a></li>
			<li class=""><a data-toggle="tab" role="tab" href="#services">Services</a></li>
			<!--<li class=""><a data-toggle="tab" role="tab" href="#bannerad">Your Ad</a></li>-->
			<!--<li class=""><a data-toggle="tab" role="tab" href="#event">Events</a></li>-->
			<li class=""><a data-toggle="tab" role="tab" href="#ourfacility">Gallery </a></li>
		</ul>
		<div class="tab-content" id="myTabContent">
			<!-- profile -->
			
			<?php echo get_subpage('views/profile'); ?> 
			
			<!-- About Us -->
			
			<?php echo get_subpage('views/autobiography'); ?>
			
			<!-- Services -->
			
			<?php echo get_subpage('views/services'); ?>
			
			<!-- banner ad -->
			
			<?php //echo get_subpage('views/ad'); ?>
			
			<!-- Event -->

			<?php //echo get_subpage('views/eventtab'); ?>
			
			<!-- our facility -->
			
			<?php echo get_subpage('views/facility'); ?>
			
			<!-- End of tabs -->
		</div>
	</div>
</div>
