<?php
	require_once('common/config.php');
	require_once('common/Form.php');
	require_once('common/DB.php');
	require_once('common/Session.php');
	require_once('model/Company.php');
	require_once('model/CompanyDAO.php');
	require_once('model/Member.php');
	require_once('model/MemberDAO.php');
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$m_var = new Member();
	$m_dao = new MemberDAO();
	
	$searchmail = $_POST['searchmail'];
	
	$arraySearchVal = str_split($searchmail);
	$apostrophe = "'";
	
	$newSearchVal = "";
	foreach($arraySearchVal as $asv):
		if($asv == $apostrophe){
			$newSearchVal .= "'".$asv;
		}else{
			$newSearchVal .= $asv;
		}
	endforeach;
	
	$searchmail = $newSearchVal;
	
	$mail_list = $c_dao->get('mailing_list', "company_name LIKE '%$searchmail%' OR company_contactno LIKE '%$searchmail%' OR email_address LIKE '%$searchmail%' OR contact_person LIKE '%$searchmail%'");
	//$directoryMembers = $m_dao->get('members', "fname LIKE '%$searchmail%' OR mname LIKE '%$searchmail%' OR lname LIKE '%$searchmail%' OR CONCAT(fname,' ',lname) LIKE '%$searchmail%'");
	
	$results = array();
?>
<style>
.member-list-content a { color: #084D52 }
</style>
	<div class="member-mail-content">
	<a href="charity-organization-mailing-list" style="margin-left: 100px">Back to mail list</a>
		<?php if(count($mail_list) > 0){ ?>
			<p>
				<?php foreach($mail_list as $m): ?>	
					<?php $results[] = $m->id;?>
					<p style="text-align:center;">
					<strong><?php echo $m->company_name; ?></strong><br/>
					<?php echo $m->contact_person; ?><br/>
					<?php echo $m->company_contactno; ?></br>
					<a href="mailto:<?php echo $m->email_address; ?>"><?php echo $m->email_address; ?></a><br/>
					</p>
					<br>
					<?php 
						// if(!empty($c->company_address))
							// echo $c->company_address.'<br>';
					?>
					<?php 
						// if(!empty($c->company_contactno))
							// echo $c->company_contactno.'<br>';
					?>
					<?php
						// if(!empty($c->zip))
							// echo $c->zip.'<br>';
					?>
					<?php
						// if(!empty($c->category))
							// echo $c->category.'<br>';
					?>
					<?php 
						// if(!empty($c->city))
							// echo $c->city.'<br>';
					?>
					<?php //foreach($members as $m):
							//echo ucfirst($m->fname).' '.ucfirst($m->lname).', '.$m->title.' '.$m->contact_no.'<br>';
						 // endforeach;?>
						  <br>
				<?php endforeach; ?>
			</p>
				<?php } ?>
		<?php if(count($mail_list)==0){?>
			<p style="text-align: center;">No records found</p>
		<?php } ?>
	</div>