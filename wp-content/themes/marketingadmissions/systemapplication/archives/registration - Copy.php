<?php
	get_common(array('config', 'Form', 'DB', 'Session'));
	get_model(array('Company', 'CompanyDAO', 'Member', 'MemberDAO', 'Admin'));
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$m_var = new Member();
	$m_dao = new MemberDAO();
	
	$errors = array();
	$error = '';
	$success = '';
	
	$regexp = "/^[a-z0-9]+([_\\.-][a-z0-9]+)*@([a-z0-9]+([\.-][a-z0-9]+)*)+\\.[a-z]{2,}$/i";
	
	if(isset($_POST['registrationSubmit'])){
		
		$c_var->account_type = Form::data('account_type');
		$c_var->company_name = Form::data('company_name');
		$c_var->company_address = Form::data('company_address');
		$c_var->company_contactno = Form::data('company_contactno');
		$c_var->username = Form::data('username');
		$c_var->password = Form::data('password');
		$c_var->repeat_password = Form::data('repeat_password');
		$c_var->city = Form::data('city');
		$c_var->state = Form::data('state');
		$c_var->zip = Form::data('zip');
		$c_var->category = Form::data('category');
		//$c_var->capacity = Form::data('capacity');
		$c_var->company_type = Form::data('company_type');
		$c_var->phone_number = Form::data('phone_number');
		$c_var->fax_number = Form::data('fax_number');
		$c_var->website = Form::data('website');
		$c_var->email_address = Form::data('email_address');
		$c_var->title = Form::data('title');
		$c_var->contact_email = Form::data('contact_email');
		$c_var->first_name2 = Form::data('first_name2');
		$c_var->middle_name2 = Form::data('middle_name2');
		$c_var->last_name2 = Form::data('last_name2');
		$c_var->title2 = Form::data('title2');
		$c_var->contact_email2 = Form::data('contact_email2');
		//$c_var->alz = Form::data('alz');
		//$c_var->other = Form::data('other');
		$c_var->description = Form::data('description');
		//$c_var->res_qual = Form::data('res_qual');
		//$c_var->private1 = Form::data('private1');
		//$c_var->ssi = Form::data('ssi');
		//$c_var->medi_cal = Form::data('medi_cal');
		//$c_var->medicare = Form::data('medicare');
		//$c_var->other2 = Form::data('other2');	
			
		//validating the fields through PHP
		
		//echo '<pre>'; print_r($c_var); echo '</pre>'; exit();
		
		if(empty($c_var->account_type) || empty($c_var->company_name) || empty($c_var->company_address) || empty($c_var->company_contactno) || empty($c_var->username) || empty($c_var->password) || empty($c_var->repeat_password) || empty($c_var->city) || empty($c_var->state) || empty($c_var->zip) || empty($c_var->category) || empty($c_var->company_type) || empty($c_var->phone_number) || empty($c_var->fax_number) || empty($c_var->website) || empty($c_var->email_address) || empty($c_var->title) || empty($c_var->contact_email) || empty($c_var->first_name2) || empty($c_var->middle_name2) || empty($c_var->last_name2) || empty($c_var->title2) || empty($c_var->contact_email2) || empty($c_var->description) ) {
			$errors[] = 'All fields are required!';
		}
		
		if( count($errors) > 0 ) {
			$error = implode('<br/>', $errors);
		} else {
			// check if email address format is correct
			if( !preg_match($regexp, $c_var->email_address) ) { 
				$errors[] = 'Invalid Email address.';
			}			
			//check if email address is existing on the database
			elseif($c_dao->checkExisting('companies', $c_var->email_address, 'email_address')) { 
				$errors[] = 'Email already existed.'; 
			}
			if( $c_var->password != $c_var->repeat_password ){
				$errors[] = "Your password did not match";
			}			
			if( count($errors) > 0 ) {
				$error = implode('<br/>', $errors);
			}
			//add record to db
			elseif( $c_dao->addCompany($c_var, "companies") ) {
				//add a member
				$company_id = DB::LastId("companies", 'company_id');	// get the last company id inserted
				$m_var->first_name = Form::data('first_name');
				$m_var->middle_name = Form::data('middle_name');
				$m_var->last_name = Form::data('last_name');
				$m_var->company_id = $company_id;
				$m_dao->addMember($m_var, "members");
				$m_dao->addMember($m_var, "members");
				
				$success = 'You have been registered!';	
			}
		}
	}
	
	
	/** Declaration for state **/
	$states = array('Please select a state.','Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District Of Columbia','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Puerto Rico','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virgin Islands','Virginia','Washington','West Virginia','Wisconsin','Wyoming');
?>

<style type="text/css">
#notify { background-color:#fff ; width: 613px; margin-bottom: 10px; font-size: 25px; font-family: arial; color: #1E90FF; font-style: italic; border-radius: 3px; border:solid 2px #fff; }
</style>
<div class="row clearfix">
	<div id="notify"><?php echo (!empty($error)) ? Form::error($error) : ''; echo (!empty($success)) ? Form::success($success) : ''; ?></div>
  <div class="col-md-11  alert alert-info">
	<form role="form" id="registration-form" method="POST">
	  <div class="form-group">
		<label><span class="required">*</span>Account Type</label>
		<div class="row">
			<div class="col-xs-5">
			  <select name="account_type" class="form-control validate[required]">
				<option value="Member">Member</option>
				<option value="Resource">Resource</option>
				<option value="Mail">Mail</option>
			  </select>
			</div>
		</div>
	  </div>
	  <br>
	  <br>
	  <div class="form-group">
		<label><span class="required">*</span> Company Name</label>
		<input type="text" name="company_name" class="form-control validate[required]" placeholder="Enter company name">
	  </div>
	  <div class="form-group">
		<label><span class="required">*</span> Address</label>
		<input type="text" name="company_address" class="form-control validate[required]" placeholder="Enter address">
	  </div>
	  <div class="form-group">
		<label><span class="required">*</span> Company Contact Number</label>
		<input type="text" name="company_contactno" class="form-control validate[required]" placeholder="Company Contact Number">
	  </div>
	  <div class="form-group">
		<label><span class="required">*</span> Username</label>
		<input type="text" name="username" class="form-control validate[required]" placeholder="Enter Username">
	  </div>
	  <div class="row">
	  <div class="col-md-6">
		  <div class="form-group">
			<label><span class="required">*</span> Password</label>
			<input type="password" name="password" class="form-control validate[required]" placeholder="Password">
		  </div>
	  </div>
	  <div class="col-md-6">
		  <div class="form-group">
			<label><span class="required">*</span> Repeat Password</label>
			<input type="password" name="repeat_password" class="form-control validate[required]" placeholder="Repeat Password">
		  </div>
	  </div>
	  </div>
	  <div class="row">
		<div class="col-md-6">
		  <div class="form-group">
			<label><span class="required">*</span> City</label>
			<input type="text" name="city" class="form-control validate[required]" placeholder="Enter city">
		  </div>
		</div>
		<div class="col-md-6">
		  <div class="form-group">
			<label><span class="required">*</span> State</label>
			<select name="state" class="form-control validate[required]">
				<?php
					foreach($states as $s):
						if($s=="Please select a state."){
							echo '<option value="">'.$s.'</option>';
							continue;
						}	
						echo '<option value="'.$s.'">'.$s.'</option>';
					endforeach;
				?>
			</select>
		  </div>
		</div>
	  </div>
	  <div class="form-group">
		<label><span class="required">*</span> Zip</label>
		<input type="text" name="zip" class="form-control validate[required]" placeholder="Enter zip">
	  </div>
	  <div class="form-group">
		<label><span class="required">*</span> Category</label>
		<select name="category" class="form-control validate[required]">
		  <option value="">Please select a category</option>
		  <option value="1">Retirement Communities</option>
		  <option value="2">In-home Services & Caregiver Agencies</option>
		  <option value="3">Legal, Financial & Real Estate</option>
		  <option value="4">Senior Care Management</option>
		  <option value="5">Residential Placement</option>
		  <option value="6">Other Senior Resources</option>
		  <option value="7">Community Partners</option>
		</select>
	  </div>
	  <div class="form-group">
	  <label>Capacity</label>
			<input type="text" name="capacity" class="form-control" placeholder="Capacity">
	  </div>
	  <label><span class="required">*</span> Company Type</label><br><br>
	  <div class="row">
		<div class="col-md-3 col-md-offset-2">
			  <input type="checkbox" name="company_type" class="validate[minCheckbox[1]]" value="1"> CCRC		<br>
			  <input type="checkbox" name="company_type" class="validate[minCheckbox[1]]" value="2"> RCFE		<br>
			  <input type="checkbox" name="company_type" class="validate[minCheckbox[1]]" value="3"> Ind		<br>
			  <input type="checkbox" name="company_type" class="validate[minCheckbox[1]]" value="4"> Asst Lvg	<br>
			  <input type="checkbox" name="company_type" class="validate[minCheckbox[1]]" value="5"> Day Care	<br>
			  <input type="checkbox" name="company_type" class="validate[minCheckbox[1]]" value="6"> Secured   <br>
			  <input type="checkbox" name="company_type" class="validate[minCheckbox[1]]" value="7"> SNF              <br>
		</div>
		<div class="col-md-7">
			  <input type="checkbox" name="company_type" class="validate[minCheckbox[1]]" value="8"> Hospice          <br>
			  <input type="checkbox" name="company_type" class="validate[minCheckbox[1]]" value="9"> Home Care        <br>
			  <input type="checkbox" name="company_type" class="validate[minCheckbox[1]]" value="10"> Referral Agency  <br>
			  <input type="checkbox" name="company_type" class="validate[minCheckbox[1]]" value="11"> Board & Care 	   <br>
			  <input type="checkbox" name="company_type" class="validate[minCheckbox[1]]" value="12"> Home Health Care <br>
			  <input type="checkbox" name="company_type" class="validate[minCheckbox[1]]" value="13"> Miscellaneous Support <br><br><br>
		</div>
	  </div>
	  <div class="row">
		<div class="col-md-6">
		  <div class="form-group">
			<span class="required">*</span><label>Phone number</label>
			<input type="text" name="phone_number" class="form-control" placeholder="Enter phone number">
		  </div>
		</div>
		<div class="col-md-6">
		  <div class="form-group">
			<span class="required">*</span><label>Fax number</label>
			<input type="text" name="fax_number" class="form-control" placeholder="Enter fax number">
		  </div>
		</div>
	  </div>
	  <div class="form-group">
		<span class="required">*</span><label>Website</label>
		<input type="text" name="website" class="form-control validate[required]" placeholder="Enter Url">
	  </div>
	  <div class="form-group">
	    <span class="required">*</span><label>Email Address</label>
		<input type="email" name="email_address" class="form-control validate[required,custom[email]]" placeholder="Enter email address">
	  </div>
	  <br>
	  <span class="required">*</span><label>Contact Person 1</label>
	  <div class="row">
		<div class="col-md-4">
		  <div class="form-group">
			<input type="text" name="fname[]" class="form-control" placeholder="First name">
		  </div>
		</div>
		<div class="col-md-4">
		  <div class="form-group">
			<input type="text" name="mname[]" class="form-control" placeholder="Middle name">
		  </div>
		</div>
		<div class="col-md-4">
		  <div class="form-group">
			<input type="text" name="lname[]" class="form-control" placeholder="Last name">
		  </div>
		</div>
	  </div>
	<div class="row">
		<div class="col-md-6">
		  <div class="form-group">
			<span class="required">*</span><label>Title</label>
			<input type="text" name="title[]" class="form-control" placeholder="Title">
		  </div>
		</div>
		<div class="col-md-6">
		  <div class="form-group">
			<span class="required">*</span><label>Email</label>
			<input type="text" name="cemail[]" class="form-control" placeholder="Email">
		  </div>
		</div>
	</div>
	<span class="required">*</span><label>Contact Person 2</label>
	<div class="row">
		<div class="col-md-4">
		  <div class="form-group">
			<input type="text" name="fname[]" class="form-control" placeholder="First name">
		  </div>
		</div>
		<div class="col-md-4">
		  <div class="form-group">
			<input type="text" name="mname[]" class="form-control" placeholder="Middle name">
		  </div>
		</div>
		<div class="col-md-4">
		  <div class="form-group">
			<input type="text" name="lname[]" class="form-control" placeholder="Last name">
		  </div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
		  <div class="form-group">
			<span class="required">*</span><label>Title</label>
			<input type="text" name="title[]" class="form-control" placeholder="Title">
		  </div>
		</div>
		<div class="col-md-6">
		  <div class="form-group">
			<span class="required">*</span><label>Email</label>
			<input type="text" name="cemail[]" class="form-control" placeholder="Email">
		  </div>
		</div>
	</div><br><br>
	<div class="form-group">
		<label>Alz</label>
		<div>
			<input type="text" name="alz" class="form-control" placeholder="Alz">
		</div>
	</div>
	<div class="form-group">
		<label>Other</label>
		<div>
			<textarea name="other" class="form-control" rows="3" placeholder="Other"></textarea>
		</div>
	</div>
	<div class="form-group">
		<span class="required">*</span><label>Description</label>
		<div>
			<textarea name="description" class="form-control" rows="3" placeholder="Description"></textarea>
		</div>
	</div>
	<div class="form-group">
		<label>Res Qual</label>
		<div>
			<textarea name="res_qual" class="form-control" rows="3" placeholder="Res Qual"></textarea>
		</div>
	</div>
	<div class="form-group">
		<label>Private</label>
		<div>
			<select name="private1" class="form-control">
				<option value="No">No</option>
				<option value="Yes">Yes</option>
			</select>
		</div>
	</div>	
	<div class="form-group">
		<label>SSI</label>
		<div>
			<select name="ssi" class="form-control validate[required]">
				<option value="No">No</option>
				<option value="Yes">Yes</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label>Medi-Cal</label>
		<div>
			<input type="text" name="medi_cal" class="form-control" placeholder="Medi-Cal">
		</div>
	</div>
	<div class="form-group">
		<label>Medicare</label>
		<div>
			<input type="text" name="medicare" class="form-control" placeholder="Medicare">
		</div>
	</div>
	<div class="form-group">
		<label>Other 2</label>
		<div>
			<textarea name="other2" class="form-control" rows="3" placeholder="Other 2"></textarea>
		</div>
	</div>
	<div class="checkbox" style="margin-bottom: 20px;">
		<label><input type="checkbox" id="checkme" name="checkme"> I have agreed on the <a href="javascript:;" data-toggle="modal" data-target="#myModal">Terms and Conditions</a>.</label>
	</div>
	<div style="text-align: center;">
		<button type="submit" class="btn btn-default" id="registrationSubmit" name="registrationSubmit" disabled>Register</button>
	</div>
	</form>
<!-- Modal -->
		<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Terms and Conditions</h4>
					</div>
					<div class="modal-body" style="text-align: justify;">
					<!--<p><span style="text-decoration: underline; font-weight: bold;">Renewal Conditions:</span> By joining a plan, you are authorizing Direct Dental Access to bill your credit card or checking account for the plan you have selected. This charge shall remain in force until you notify Direct Dental Access.com of request to cancel. By joining, you indicate you have read the terms and conditions of the plan. This plan will automatically renew at the end of your annual membership term, billing is on a  monthly or Annual basis, and your credit card or bank account will be automatically charged or drafted for the appropriate amount.</p>
					<p><span style="text-decoration: underline; font-weight: bold;">Termination Conditions:</span> Direct Dental Access.com and Careington International Corporation (Careington) reserves the right to terminate plan members from its plan for any reason, including non-payment. </p>
					<p><span style="text-decoration: underline; font-weight: bold;">Cancellation Conditions:</span> You have the right to cancel within the first 30 days after receipt of membership materials and receive a full refund, less the processing fee, if applicable.  FL Residents: You have the right to cancel within 30 days after the effective date. If for any reason during this time period you are dissatisfied with the plan and wish to cancel and obtain a refund, you must submit a written cancellation request. Direct Dental Access.com will accept and cancel plan memberships at any time during the membership period and will cease collecting membership fees in a reasonable amount of time, but no later than 30 days after receiving a cancellation notice. Please send a cancellation letter and a request for refund with your name and member number to Direct Dental Access.com, fax: 801-363-3932. You may also submit cancellation by email: cancellation@directdentalaccess.com. If Direct Dental Access.com is billing you annually, Direct Dental Access.com will, in the event of cancellation of the membership by either party, make a pro-rata reimbursement of the periodic charges to the member.</p>
					<p><span style="text-decoration: underline; font-weight: bold;">Description of Services:</span> Please see the enclosed materials for a specific description of the programs that you have purchased.</p>
					<p><span style="text-decoration: underline; font-weight: bold;">Limitations, Exclusions &amp; Exceptions:</span> This program is a network discount membership program offered by Careington. Careington is not a licensed insurer, health maintenance organization, or other underwriter of health care services. No portion of any providerís fees will be reimbursed or otherwise paid by Careington. Careington is not licensed to provide and does not provide medical services or items to individuals. You will receive discounts for medical services at certain health care providers who have contracted with the plan. You are obligated to pay for all health care services at the time of your appointment. Savings are based upon the providerís normal fees. Actual savings will vary depending upon location and specific services or products purchased. Please verify such services with each individual provider. The discounts contained herein may not be used in conjunction with any other discount plan or program. All listed or quoted prices are current prices by participating providers and subject to change without notice. Any procedures performed by a non-participating provider are not discounted. From time to time, certain providers may offer products or services to the general public at prices lower than the discounted prices available through this program. In such event, members will be charged the lowest price. Discounts on professional services are not available where prohibited by law. This plan does not discount all procedures. Providers are subject to change without notice and services may vary in some states. It is the memberís responsibility to verify that the provider is a participant in the plan. At any time Careington may substitute a provider network at its sole discretion. Careington cannot guarantee the continued participation of any provider. If the provider leaves the plan, you will need to select another provider. Providers contracted by Careington are solely responsible for the professional advice and treatment rendered to members and Careington disclaims any liability with respect to such matters.</p>
					<p><span style="text-decoration: underline; font-weight: bold;">Complaint Procedure:</span> If you would like to file a complaint or grievance regarding your plan membership, you must submit your grievance in writing to: Careington International Corporation, P.O. Box 2568, Frisco, TX 75034. You have the right to request an appeal if you are dissatisfied with the complaint resolution. After completing the complaint resolution process and you remain dissatisfied, you may contact your state insurance department.</p>
					<p>Form: F03F-<?php //echo date('Y'); //$start_year = '2014'; $current_year = date('Y'); $copyright = ($current_year == $start_year) ? $start_year : $start_year.' - '.$current_year; echo $copyright; ?>-TERMCON</p>-->
					<p><span class="comingsoon">Coming Soon...</span></p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
   </div>
</div>


