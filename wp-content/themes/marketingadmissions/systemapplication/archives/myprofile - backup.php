<?php 
	// error_reporting(E_ALL);
	// ini_set('display_errors', 1);
	
	get_common(array('config', 'Form', 'DB', 'Session'));
	get_model(array('Company', 'CompanyDAO', 'Member', 'MemberDAO', 'Admin'));
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$m_var = new Member();
	$m_dao = new MemberDAO();
	
	// echo $userid = $_SESSION['ID'];
	
	$company_id = Session::get('ID');
	$company = $c_dao->getRow("companies", "company_id='$company_id'");
	$member = $m_dao->get("members", 'company_id='.$company_id);
	// print_r($member); exit();
	
	$regexp = "/^[a-z0-9]+([_\\.-][a-z0-9]+)*@([a-z0-9]+([\.-][a-z0-9]+)*)+\\.[a-z]{2,}$/i";
	
	$filename = "";
	$filename1 = "";
	// $scr = "";
	$abc = "";
	$errors = array();
	$error = '';
	$success = '';
	
	
	/*function for uploading profile picture*/
	define ("MAX_SIZE","10485760");
	
	function getExtension($str) {
		$i = strrpos($str, ".");
		if(!$i) {
			return "";
		}
		$l = strlen($str) - $i;
		$ext = substr($str, $i + 1, $l);
		return $ext;
	}

	if($_SERVER["REQUEST_METHOD"] == "POST") {
		$image = $_FILES["profile_pic"]["name"];
		$uploadedfile = $_FILES['profile_pic']['tmp_name'];

		if ($image) {
			$filename = stripslashes($_FILES['profile_pic']['name']);

			$extension = getExtension($filename);
			$extension = strtolower($extension);


			if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) {
				$errors[] = 'Please upload a photo!';
				$error = implode('<br/>', $errors);
			}
			else {
				$size = filesize($_FILES['profile_pic']['tmp_name']);

				if ($size > MAX_SIZE*1024) {
					$errors[] = 'The photo you wish to upload is beyond the maximum 2MB limit!';
				}
				
				if( count($errors) == 0 ) {

					if($extension == "jpg" || $extension == "jpeg" ) {
						$uploadedfile = $_FILES['profile_pic']['tmp_name'];
						$src = imagecreatefromjpeg($uploadedfile);
					}
					else if($extension=="png") {
						$uploadedfile = $_FILES['profile_pic']['tmp_name'];
						$src = imagecreatefrompng($uploadedfile);

					} else  {
						$src = imagecreatefromgif($uploadedfile);
					}

					// echo $scr;

					list($width,$height) = getimagesize($uploadedfile);


					$newwidth = 300;
					$newheight = ($height/$width)*$newwidth;
					$tmp = imagecreatetruecolor($newwidth,$newheight);


					$newwidth1 = 150;
					$newheight1 = ($height/$width)*$newwidth1;
					$tmp1 = imagecreatetruecolor($newwidth1,$newheight1);

					imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

					imagecopyresampled($tmp1,$src,0,0,0,0,$newwidth1,$newheight1,$width,$height);

					// $filename = "wp-content/themes/marketingadmissions/systemapplication/uploads/". date("mdyhis") .$_FILES['file']['name'];
					$filename = "wp-content/themes/marketingadmissions/systemapplication/uploads/". date("mdyhis") . '.' . $extension;
					$img_newname = date("mdyhis") . '.' . $extension;
					
					// $filename1 = "wp-content/themes/marketingadmissions/systemapplication/uploads/thumbnails/". date("mdyhis") . $_FILES['file']['name'];
					$filename1 = "wp-content/themes/marketingadmissions/systemapplication/uploads/thumbnails/thumb_". date("mdyhis") . '.' . $extension;
					$img_newname1 = "thumb_". date("mdyhis") . '.' . $extension;

					imagejpeg($tmp,$filename,100);

					imagejpeg($tmp1,$filename1,100);

					imagedestroy($src);
					imagedestroy($tmp);
					imagedestroy($tmp1);
					
				} else {
					$error = implode('<br/>', $errors);
				}
			}
		}
	}
	
	//If no errors registred, print the success message
	if(isset($_POST['btnupdate']) && (count($errors) == 0) ) {
		$c_var->pic = $img_newname;
		$c_var->companyid = $company_id;
		
		// member picture
		$m_pic = $c_dao->getRow('pictures', 'companyid='.$c_var->companyid);
		
		@unlink('wp-content/themes/marketingadmissions/systemapplication/uploads/'.$m_pic->pic);
		@unlink('wp-content/themes/marketingadmissions/systemapplication/uploads/thumbnails/thumb_'.$m_pic->pic);
		
		// checks for existing record then update
		// if no record, then insert
		$c_dao->save('pictures', 'companyid', $c_var);
		$success = 'Image Uploaded Successfully!';
	}
		
	/*function for updating profile info*/
	if( isset($_POST['btnupdateprofile']) && ($_POST['btnupdateprofile'] == 'Update') ) {
		$c_var->company_id = Session::get('ID');
		
		$c_var->account_type = Form::data('account_type');
		$c_var->company_name = Form::data('company_name');
		$c_var->company_address = Form::data('company_address');
		$c_var->company_contactno = Form::data('company_contactno');
		$c_var->username = Form::data('username');
		$c_var->password = Form::data('password');
		$c_var->repeat_password = Form::data('repeat_password');
		$c_var->city = Form::data('city');
		$c_var->state = Form::data('state');
		$c_var->zip = Form::data('zip');
		$c_var->category = Form::data('category');
		$c_var->capacity = Form::data('capacity');
		$c_var->company_type = Form::data('company_type');
		$c_var->phone_number = Form::data('phone_number');
		$c_var->fax_number = Form::data('fax_number');
		$c_var->website = Form::data('website');
		$c_var->email_address = Form::data('email_address');
		$c_var->alz = Form::data('alz');
		$c_var->other = Form::data('other');
		$c_var->description = Form::data('description');
		$c_var->res_qual = Form::data('res_qual');
		$c_var->private1 = Form::data('private1');
		$c_var->ssi = Form::data('ssi');
		$c_var->medi_cal = Form::data('medi_cal');
		$c_var->medicare = Form::data('medicare');
		$c_var->other2 = Form::data('other2');	
		
		$m_var->fname = $_POST['fname'];
		$m_var->mname = $_POST['mname'];
		$m_var->lname = $_POST['lname'];
		$m_var->title = $_POST['title'];
		$m_var->cemail = $_POST['cemail'];
		
		//validation of fields
		if(empty($c_var->account_type) || empty($c_var->company_name) || empty($c_var->company_address) || empty($c_var->company_contactno) || empty($c_var->username) || empty($c_var->password) || empty($c_var->repeat_password) || empty($c_var->city) || empty($c_var->state) || empty($c_var->zip) || empty($c_var->category) || empty($c_var->company_type) || empty($c_var->phone_number) || empty($c_var->fax_number) || empty($c_var->website) || empty($c_var->email_address) ) {
			$errors[] = '';
		} 
		// validates if no contact person is entered. should input at least 1 contact person
		elseif( empty($m_var->fname) || empty($m_var->mname) || empty($m_var->lname) || empty($m_var->title) || empty($m_var->cemail) ) {
			$errors[] = 'Please enter at least one contact person.';
		}
		
		//checking if the email is address is valid
		if( !preg_match($regexp, $c_var->email_address) ) { 
			$errors[] = 'Invalid Email address.';
	    }
		
		//Checking if the email already exist
		 elseif($c_dao->checkExistingToOtherUsers('companies', $c_var->email_address, 'email_address', $company_id)) { 
			$errors[] = 'Email already existed.'; 
		} 
			
		//checking if the password matched
		if( $c_var->password != $c_var->repeat_password){
			$errors[] = 'Your password did not matched!';
		}
		// print_r($c_var);
		// print_r($m_var); exit();
		//counting all the errors
		if( count($errors) > 0 ) {
			$error = implode('<br/>', $errors);	
		}
		elseif( $c_dao->updateProfile($c_var, "companies") ){
		
			 if($m_dao->delete('members', "WHERE company_id=".$company_id)) {
			//adding a member
			 // $m_dao->updateMember($m_var, "members");
				print_r($m_var); exit();
				if( is_array($m_var->fname) ) {
					$ctr = count($m_var->fname);	// first name counter
				}
				
				for($i = 0; $i < $ctr; $i++) {
					if( !empty($_POST['fname'][$i]) && !empty($_POST['lname'][$i]) && !empty($_POST['title'][$i]) && !empty($_POST['cemail'][$i]) ) {
						$m_var->company_id = $company_id;
						$m_var->fname = $_POST['fname'][$i];
						$m_var->mname = $_POST['mname'][$i];
						$m_var->lname = $_POST['lname'][$i];
						$m_var->title = $_POST['title'][$i];
						$m_var->cemail = $_POST['cemail'][$i];
						$m_dao->addMember($m_var, "members");
					}
				}
			}
			
			$success = 'Profile has been successfully updated!';
		}
		
	}
	 
	/*function for adding autobiography*/
	if( isset($_POST['btnbiography']) && ($_POST['btnbiography'] == 'submit') ){
		$c_var->companyid = $company_id;		
		$c_var->autobiography = Form::data('autobiography');
		
		$c_dao->save('autobiography', 'companyid', $c_var);
		$success = 'Autobiography has been successfully updated!';
	}
	
	/*function for adding ads and banners*/
	if($_POST) {
		$btnyourad = Form::data('btnads');
		if(isset($btnyourad) && ($btnyourad == 'Submit Ad') ) {
			$image = $_FILES["ads"]["name"];
			$uploadedfile = $_FILES['ads']['tmp_name'];

			if ($image) {
				$filename = stripslashes($_FILES['ads']['name']);

				$extension = getExtension($filename);
				$extension = strtolower($extension);


				if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) {
					$errors[] = 'Please upload a photo!';
					$error = implode('<br/>', $errors);
				}
				else {
					$size = filesize($_FILES['ads']['tmp_name']);

					if ($size > MAX_SIZE*5242880) {
						$errors[] = 'The photo you wish to upload is beyond the maximum 5MB limit!';
					}
					
					if( count($errors) == 0 ) {

						if($extension == "jpg" || $extension == "jpeg" ) {
							$uploadedfile = $_FILES['ads']['tmp_name'];
							$src = imagecreatefromjpeg($uploadedfile);
						}
						else if($extension=="png") {
							$uploadedfile = $_FILES['ads']['tmp_name'];
							$src = imagecreatefrompng($uploadedfile);

						} else  {
							$src = imagecreatefromgif($uploadedfile);
						}

						// echo $scr;

						list($width,$height) = getimagesize($uploadedfile);


						$newwidth = 320;
						$newheight = ($height/$width)*$newwidth;
						$tmp = imagecreatetruecolor($newwidth,$newheight);


						$newwidth1 = 160	;
						$newheight1 = ($height/$width)*$newwidth1;
						$tmp1 = imagecreatetruecolor($newwidth1,$newheight1);

						imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

						imagecopyresampled($tmp1,$src,0,0,0,0,$newwidth1,$newheight1,$width,$height);

						// $filename = "wp-content/themes/marketingadmissions/systemapplication/uploads/". date("mdyhis") .$_FILES['file']['name'];
						$filename = "wp-content/themes/marketingadmissions/systemapplication/uploads/ads/". date("mdyhis") . '.' . $extension;
						$img_newname = date("mdyhis") . '.' . $extension;
						
						// $filename1 = "wp-content/themes/marketingadmissions/systemapplication/uploads/thumbnails/". date("mdyhis") . $_FILES['file']['name'];
						$filename1 = "wp-content/themes/marketingadmissions/systemapplication/uploads/ads/thumbnails/thumb_". date("mdyhis") . '.' . $extension;
						$img_newname1 = "thumb_". date("mdyhis") . '.' . $extension;

						imagejpeg($tmp,$filename,100);

						imagejpeg($tmp1,$filename1,100);

						imagedestroy($src);
						imagedestroy($tmp);
						imagedestroy($tmp1);
						
					} else {
						$error = implode('<br/>', $errors);
					}
				}
			}
			
			$c_var->ads = $img_newname;
			$c_var->companyid = $company_id;
			
			// member ads
			$m_ads = $c_dao->getRow('ads', 'companyid='.$c_var->companyid);
			
			@unlink('wp-content/themes/marketingadmissions/systemapplication/uploads/ads'.$m_ads->ads);
			@unlink('wp-content/themes/marketingadmissions/systemapplication/uploads/ads/thumbnails/thumb_'.$m_ads->ads);
			
			// checks for existing record then update
			// if no record, then insert
			$c_dao->save('ads', 'companyid', $c_var);
			$success = 'You have submitted your ad successfully!';
		}
	}
	
	$banner = $c_dao->getRow("ads", "companyid=".$company_id);
	$bio = $c_dao->getRow("autobiography", "companyid=".$company_id);
	$profile_pic = $c_dao->getRow("pictures", "companyid=".$company_id);
		
	/** Declaration for state **/
	$states = array('Please select a state.','Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District Of Columbia','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Puerto Rico','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virgin Islands','Virginia','Washington','West Virginia','Wisconsin','Wyoming');
?>

<script type="text/javascript">

</script>
<style>
/*.profile-photo { width:150px; height:150px; border: 2px solid; float:left; }*/
.autobiography { height: auto; width: auto; margin: 15px auto; padding:15px; }
.container .bs-example-tabs .nav-tabs { border-bottom: none; }
</style>

<div class="container row clearfix" style="width: 100%; display: block;">
	<div class="col-md-12" id="notify"><?php echo (!empty($error)) ? Form::error($error) : ''; echo (!empty($success)) ? Form::success($success) : ''; ?></div>
	<div class="col-md-6"><!-- Uploaded profile must display here -->
	<?php
	if(!empty($profile_pic)) {
	?>
		<img src="<?php bloginfo('template_url'); ?>/systemapplication/uploads/<?php echo $profile_pic->pic; ?>" class="img-responsive img-thumbnail" alt="Responsive image">
		<!--<div class="profile-photo"></div>-->
	<?php
	}
	?>
	</div>
	<div class="col-md-6">
		<form role="form" id="myprofile" method="POST" enctype="multipart/form-data" >
		<div class="form-group">
			<div class="col-md-12">
				<label for="file">Profile Picture</label>
				<input type="file" name="profile_pic" name="image" id="profile_pic"><br>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-12">
				<input type="submit" name="btnupdate" class="btn btn-primary" value="Submit">
			</div>
		</div>
		</form>
	</div>
	<div class="bs-example bs-example-tabs col-md-12">
		<ul role="tablist" class="nav nav-tabs" id="myTab">
			<li class="active"><a data-toggle="tab" role="tab" href="#autobiography">Autobiography</a></li>
			<li class=""><a data-toggle="tab" role="tab" href="#profile">Profile</a></li>
			<li class=""><a data-toggle="tab" role="tab" href="#bannerad">Your Ad</a></li>
		</ul>
		<div class="tab-content" id="myTabContent">
			<!-- autobiography -->
			<div id="autobiography" class="tab-pane fade active in"> 
				<p>
					<div class="row alert alert-info clearfix autobiography">
						<form role="form" action="" method="post">
							<div class="form-group">
								<label>Autobiography</label>
								<textarea name="autobiography" class="form-control" rows="10" placeholder="autobiography"><?php echo $bio->autobiography;?></textarea>
							</div>
							<input type="submit" class="btn btn-default" name="btnbiography" value="Submit">
						</form>
					</div>
				</p>
			</div>
			<!-- profile -->
			<div id="profile" class="tab-pane fade"> 
				<p>
					<div class="row">
						<div class="col-md-11  alert alert-info">
							<form role="form" id="registration-form" method="POST">
								<div class="form-group">
									<label>Account Type</label>
									<div class="row">
										<div class="col-xs-5">
										  <select name="account_type" class="form-control" readonly="readonly" disabled>
											<option <?php if($company->account_type=="Member"){ echo "selected";}?> value="Member">Member</option>
											<option <?php if($company->account_type=="Resource"){ echo "selected";}?> value="Resource">Resource</option>
											<option <?php if($company->account_type=="Mail"){ echo "selected";}?> value="Mail">Mail</option>
										  </select>
										</div>
									</div>
								  </div>
								  <br>
								  <br>
								  <div class="form-group">
									<label><span class="required">*</span>Company Name</label>
									<input type="text" name="company_name" class="form-control validate[required]" placeholder="Enter company name" value="<?php echo $company->company_name;?>">
								  </div>
								  <div class="form-group">
									<label><span class="required">*</span>Address</label>
									<input type="text" name="company_address" class="form-control validate[required]" placeholder="Enter address" value="<?php echo $company->company_address;?>">
								  </div>
								  <div class="form-group">
									<label><span class="required">*</span>Company Contact Number</label>
									<input type="text" name="company_contactno" class="form-control validate[required]" placeholder="Company Contact Number" value="<?php echo $company->company_contactno;?>">
								  </div>
									<div class="form-group">
							<label><span class="required">*</span>Username</label>
							<input type="text" name="username" class="form-control validate[required]" placeholder="Enter Username" value="<?php echo $company->username;?>">
						  </div>
						  <div class="row">
						  <div class="col-md-6">
							  <div class="form-group">
								<label><span class="required">*</span>Password</label>
								<input type="password" name="password" class="form-control validate[required]" placeholder="Password" value="<?php echo $company->password;?>">
							  </div>
						  </div>
						  <div class="col-md-6">
							  <div class="form-group">
								<label><span class="required">*</span>Repeat Password</label>
								<input type="password" name="repeat_password" class="form-control validate[required]" placeholder="Repeat Password" value="<?php echo $company->repeat_password;?>">
							  </div>
						  </div>
						  </div>
							<div class="row">
								<div class="col-md-6">
									  <div class="form-group">
										<label><span class="required">*</span>City</label>
										<input type="text" name="city" class="form-control validate[required]" placeholder="Enter city" value="<?php echo $company->city;?>">
									  </div>
								</div>
								<div class="col-md-6">
								<div class="form-group">
								<label><span class="required">*</span>State</label>	
									<select name="state" class="form-control validate[required]">
										<?php
											foreach($states as $s):
												if($s==$company->state){
													echo '<option value="'.$s.'" selected>'.$s.'</option>';
												}else{
													echo '<option value="'.$s.'">'.$s.'</option>';
												}
											endforeach;
										?>
									</select>
								</div>
								</div>
							</div>
							  <div class="form-group">
								<label><span class="required">*</span>Zip</label>
								<input type="text" name="zip" class="form-control validate[required]" placeholder="Enter zip" value="<?php echo $company->zip;?>">
							  </div>
							  <div class="form-group">
								<label><span class="required">*</span>Category</label>
								<select name="category" class="form-control validate[required]">
								  <option value="">Please select a category</option>
								  <option <?php if($company->category=="Retirement Communities"){ echo "selected";}?> value="Retirement Communities">Retirement Communities</option>
								  <option <?php if($company->category=="In-home Services & Caregiver Agencies"){ echo "selected";}?> value="In-home Services & Caregiver Agencies">In-home Services & Caregiver Agencies</option>
								  <option <?php if($company->category=="Legal, Financial & Real Estate"){ echo "selected";}?> value="Legal, Financial & Real Estate">Legal, Financial & Real Estate</option>
								  <option <?php if($company->category=="Senior Care Management"){ echo "selected";}?> value="Senior Care Management">Senior Care Management</option>
								  <option <?php if($company->category=="Residential Placement"){ echo "selected";}?> value="Residential Placement">Residential Placement</option>
								  <option <?php if($company->category=="Other Senior Resources"){ echo "selected";}?> value="Other Senior Resources">Other Senior Resources</option>
								  <option <?php if($company->category=="Community Partners"){ echo "selected";}?> value="Community Partners">Community Partners</option>
								</select>
							  </div>
							  <div class="form-group">
							  <label>Capacity</label>
									<input type="text" name="capacity" class="form-control" placeholder="Capacity">
							  </div>
							  <label><span class="required">*</span>Company Type</label><br><br>
							  <div class="row">
								<div class="col-md-3 col-md-offset-2">
										
									  <input type="checkbox" name="company_type" value="1" <?php echo $company->company_type=="1" ? 'checked': ''; ?>> CCRC		<br>
									  <input type="checkbox" name="company_type" value="2" <?php echo $company->company_type=="2" ? 'checked': ''; ?>> RCFE		<br>
									  <input type="checkbox" name="company_type" value="3" <?php echo $company->company_type=="3" ? 'checked': ''; ?>> Ind		<br>
									  <input type="checkbox" name="company_type" value="4" <?php echo $company->company_type=="4" ? 'checked': ''; ?>> Asst Lvg	<br>
									  <input type="checkbox" name="company_type" value="5" <?php echo $company->company_type=="5" ? 'checked': ''; ?>> Day Care	<br>
									  <input type="checkbox" name="company_type" value="6" <?php echo $company->company_type=="6" ? 'checked': ''; ?>> Secured   <br>
									  <input type="checkbox" name="company_type" value="7" <?php echo $company->company_type=="7" ? 'checked': ''; ?> selected> SNF   <br>
								</div>
								<div class="col-md-7">
									  <input type="checkbox" name="company_type" value="8" <?php  echo $company->company_type=="8" ? 'checked': '';?> > Hospice          <br>
									  <input type="checkbox" name="company_type" value="9" <?php  echo $company->company_type=="9" ? 'checked': '';?>> Home Care        <br>
									  <input type="checkbox" name="company_type" value="10" <?php echo $company->company_type=="10" ? 'checked': ''; ?>> Referral Agency  <br>
									  <input type="checkbox" name="company_type" value="11" <?php echo $company->company_type=="11" ? 'checked': ''; ?>> Board & Care 	   <br>
									  <input type="checkbox" name="company_type" value="12" <?php echo $company->company_type=="12" ? 'checked': ''; ?>> Home Health Care <br>
									  <input type="checkbox" name="company_type" value="13" <?php echo $company->company_type=="13" ? 'checked': ''; ?>> Miscellaneous Support <br><br><br>
								</div>
							  </div>
							  <div class="row">
									<div class="col-md-6">
									  <div class="form-group">
										<label><span class="required">*</span>Phone number</label>
										<input type="text" name="phone_number" class="form-control validate[required]" placeholder="Enter phone number" value="<?php echo $company->phone_number;?>">
									  </div>
									</div>
									<div class="col-md-6">
									  <div class="form-group">
										<label><span class="required">*</span>Fax number</label>
										<input type="text" name="fax_number" class="form-control validate[required]" placeholder="Enter fax number" value="<?php echo $company->fax_number;?>">
									  </div>
									</div>
							  </div>
							  <div class="form-group">
									<label><span class="required">*</span>Website</label>
									<input type="text" name="website" class="form-control validate[required]" placeholder="Enter Url" value="<?php echo $company->website;?>">
							  </div>
							  <div class="form-group">
									<label><span class="required">*</span>Email Address</label>
									<input type="email" name="email_address" class="form-control validate[required]" placeholder="Enter email address" value="<?php echo $company->email_address;?>">
							  </div>
							  <label><span class="required">*</span>Contact Person 1</label>
							  <div class="row">
								<div class="col-md-4">
								  <div class="form-group">
									<input type="text" name="fname[]" class="form-control validate[required]" placeholder="First name" value="<?php echo $member[0]->fname;?>">
								  </div>
								</div>
								<div class="col-md-4">
								  <div class="form-group">
									<input type="text" name="mname[]" class="form-control validate[required]" placeholder="Middle name" value="<?php echo $member[0]->mname;?>">
								  </div>
								</div>
								<div class="col-md-4">
								  <div class="form-group">
									<input type="text" name="lname[]" class="form-control validate[required]" placeholder="Last name" value="<?php echo $member[0]->lname;?>">
								  </div>
								</div>
							  </div>
							<div class="row">
								<div class="col-md-6">
								  <div class="form-group">
									<label>Title</label>
									<input type="text" name="title[]" class="form-control validate[required]" placeholder="Title" value="<?php echo $member[0]->title;?>">
								  </div>
								</div>
								<div class="col-md-6">
								  <div class="form-group">
									<label>Email</label>
									<input type="text" name="cemail[]" class="form-control validate[required]" placeholder="Email" value="<?php echo $member[0]->cemail;?>">
								  </div>
								</div>
							</div>
							<label>Contact Person 2</label>
							<div class="row">
								<div class="col-md-4">
								  <div class="form-group">
									<input type="text" name="fname[]" class="form-control" placeholder="First name" value="<?php echo $member[1]->fname;?>">
								  </div>
								</div>
								<div class="col-md-4">
								  <div class="form-group">
									<input type="text" name="mname[]" class="form-control" placeholder="Middle name" value="<?php echo $member[1]->mname;?>">
								  </div>
								</div>
								<div class="col-md-4">
								  <div class="form-group">
									<input type="text" name="lname[]" class="form-control" placeholder="Last name" value="<?php echo $member[1]->lname;?>">
								  </div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
								  <div class="form-group">
									<label>Title</label>
									<input type="text" name="title[]" class="form-control" placeholder="Title" value="<?php echo $member[1]->title;?>">
								  </div>
								</div>
								<div class="col-md-6">
								  <div class="form-group">
									<label>Email</label>
									<input type="text" name="cemail[]" class="form-control" placeholder="Email" value="<?php echo $member[1]->cemail;?>">
								  </div>
								</div>
							</div><br><br>
							<div class="form-group">
								<label>Alz</label>
								<div>
									<input type="text" name="alz" class="form-control" placeholder="Alz" value="<?php echo $company->alz;?>">
								</div>
							</div>
							<div class="form-group">
								<label>Other</label>
								<div>
									<textarea name="other" class="form-control" rows="3" placeholder="Other" value="<?php echo $company->other;?>"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label>Description</label>
								<div>
							<textarea type="text" name="description" class="form-control" rows="3" placeholder="Description" value="<?php echo $company->description;?>"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label>Res Qual</label>
								<div>
									<textarea name="res_qual" class="form-control" rows="3" placeholder="Res Qual" value="<?php echo $company->res_qual;?>"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label>Private</label>
								<div>
									<select name="private1" class="form-control">
										<option value="No" <?php echo $company->company_type=="No" ? 'selected': ''; ?>>No</option>
										<option value="Yes" <?php echo $company->company_type=="Yes" ? 'selected': ''; ?>>Yes</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label>SSI</label>
								<div>
									<select name="ssi" class="form-control">
										<option value="No" <?php echo $company->company_type=="No" ? 'selected': ''; ?>>No</option>
										<option value="Yes" <?php echo $company->company_type=="Yes" ? 'selected': ''; ?>>Yes</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label>Medi-Cal</label>
								<div>
									<input type="text" name="medi_cal" class="form-control" placeholder="Medi-Cal" value="<?php echo $company->medi_cal;?>">
								</div>
							</div>
							<div class="form-group">
								<label>Medicare</label>
								<div>
									<input type="text" name="medicare" class="form-control" placeholder="Medicare" value="<?php echo $company->medicare;?>">
								</div>
							</div>
							<div class="form-group">
								<label>Other 2</label>
								<div>
									<textarea name="other2" class="form-control" rows="3" placeholder="Other 2" value="<?php echo $company->other2;?>"></textarea>
								</div>
							</div>
							<div style="text-align: center;">
								<input type="submit" class="btn btn-default" name="btnupdateprofile" value="Update">
							</div>
							</form>
						</div>
					</div>
				</p>
			</div>			
			<!-- banner ad -->
			<div id="bannerad" class="tab-pane fade"> 
				<p>
					<div class="row alert alert-info clearfix autobiography">
						<div class="col-md-12 text-center" style="margin-bottom: 20px;">
						<?php
						if(!empty($banner)) { ?>
							<img src="<?php bloginfo('template_url'); ?>/systemapplication/uploads/ads/<?php echo $banner->ads; ?>" class="img-responsive img-thumbnail" alt="Responsive image">
							<!--<div class="profile-photo"></div>-->
						<?php
						} ?>
						</div>
						<form role="form" id="ads_banner" method="POST" enctype="multipart/form-data" >
							<div class="form-group">
								<div class="col-md-12">
									<label for="file">Upload your Ad</label>
									<input type="file" name="ads" name="image" id="ads"><br>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<input type="submit" name="btnads" class="btn btn-primary" value="Submit Ad">
								</div>
							</div>
						</form>
					</div>
				</p>
			</div>
		</div>
	</div>
</div>
