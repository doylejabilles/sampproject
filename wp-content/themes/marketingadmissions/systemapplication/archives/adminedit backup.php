<?php
	
	include 'wp-content/themes/marketingadmissions/forms/form_details.php';
	if($smtp)
	include 'wp-content/themes/marketingadmissions/forms/phpmailer/sendmail.php';

	get_common(array('config', 'Form', 'DB', 'Session'));
	get_model(array('Company', 'CompanyDAO', 'Member', 'MemberDAO', 'Admin'));
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$m_var = new Member();
	$m_dao = new MemberDAO();

	
	$errors = array();
	$error = '';
	$success = '';
	
	$regexp = "/^[a-z0-9]+([_\\.-][a-z0-9]+)*@([a-z0-9]+([\.-][a-z0-9]+)*)+\\.[a-z]{2,}$/i";
	
	$company = $c_dao->getRow("companies", 'company_id='.$_GET['id']);
	
	if( isset($_POST['btnedit']) ) {
		$c_var->company_id = $_GET['id'];
		
		$c_var->account_type = Form::data('account_type');
		$c_var->company_name = Form::data('company_name');
		$c_var->company_address = Form::data('company_address');
		$c_var->company_contactno = Form::data('company_contactno');
		$c_var->username = Form::data('username');
		$c_var->password = Form::data('password');
		$c_var->repeat_password = Form::data('repeat_password');
		$c_var->city = Form::data('city');
		$c_var->state = Form::data('state');
		$c_var->zip = Form::data('zip');
		$c_var->category = Form::data('category');
		$c_var->capacity = Form::data('capacity');
		$c_var->company_type = Form::data('company_type');
		$c_var->phone_number = Form::data('phone_number');
		$c_var->fax_number = Form::data('fax_number');
		$c_var->website = Form::data('website');
		$c_var->email_address = Form::data('email_address');
		$c_var->first_name = Form::data('first_name');
		$c_var->middle_name = Form::data('middle_name');
		$c_var->last_name = Form::data('last_name');
		$c_var->title = Form::data('title');
		$c_var->contact_email = Form::data('contact_email');
		$c_var->first_name2 = Form::data('first_name2');
		$c_var->middle_name2 = Form::data('middle_name2');
		$c_var->last_name2 = Form::data('last_name2');
		$c_var->title2 = Form::data('title2');
		$c_var->contact_email2 = Form::data('contact_email2');
		$c_var->alz = Form::data('alz');
		$c_var->other = Form::data('other');
		$c_var->description = Form::data('description');
		$c_var->res_qual = Form::data('res_qual');
		$c_var->private1 = Form::data('private1');
		$c_var->ssi = Form::data('ssi');
		$c_var->medi_cal = Form::data('medi_cal');
		$c_var->medicare = Form::data('medicare');
		$c_var->other2 = Form::data('other2');	
		
		
		//checking if the email is address is valid
		if( !preg_match($regexp, $c_var->email_address) ) { 
			$errors[] = 'Invalid Email address.';
	    }
		
		//Checking if the email already exist
		 elseif($c_dao->checkExistingToOtherUsers('companies', $c_var->email_address, 'email_address', $c_var->company_id)) { 
			$errors[] = 'Email already existed.'; 
		} 
			
		//checking if the password matched
		if( $c_var->password != $c_var->repeat_password){
			$errors[] = 'Your password did not matched!';
		}
		
		//counting all the errors
		if( count($errors) > 0 ) {
			$error = implode('<br/>', $errors);	
		}
		elseif( $c_dao->updateProfile($c_var, "companies") ){
			$success = 'Profile has been successfully updated!';
			
			//sending email notification
			$body = 'Changes in your settings has been updated!';
			
			$subject = $dcomp . "[ Account Updated ]";		
			
			/************** for smtp ***********/
			if($smtp) { 
				$mail = new SendMail($host, $username, $password);
				$trysend = $mail->sendNow($to_email, $to_name, $cc, $bcc, $from_email, $from_name, $subject, $body);
				if ($trysend == 'ok')
					$sent = true;
				else
					$sent = false;
			}else {
			/************** for mail function ***********/
				$headers= 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= "From: ".$from_name." <".$from_email.">\n";
				$headers .= "Cc: ".$cc."\n";
				$headers .= "Bcc: ".$bcc;
				if(mail($to_email, $subject, $body, $headers)) {
					$sent = true;				
				}else {
					$sent = false;
				}
			}
			
			if($sent) {
					$prompt_message = '<div id="success">Status update has been successfully sent to your email.</div>';
					unset($_POST);
			}else {
					$prompt_message = '<div id="error">Failed to send email.</div>';				
			}
		}
		
	}
	
?>

<style>
.edit-profile { height: auto; width: auto; margin: 15px auto; padding:15px; }
</style>
<div class="clearfix edit-profile"> 
	<?php echo $prompt_message; ?>
	<div class="col-md-12" id="notify"><?php echo (!empty($error)) ? Form::error($error) : ''; echo (!empty($success)) ? Form::success($success) : ''; ?></div>
		<div class="row">
			<div class="col-md-11  alert alert-info">
				<form role="form" method="POST">
					<div class="form-group">
						<label>Account Type</label>
						<div class="row">
							<div class="col-xs-5">
							  <select name="account_type" class="form-control">
								<option <?php if($company->account_type=="Member"){ echo "selected";}?> value="Member">Member</option>
								<option <?php if($company->account_type=="Resource"){ echo "selected";}?> value="Resource">Resource</option>
								<option <?php if($company->account_type=="Mail"){ echo "selected";}?> value="Mail">Mail</option>
							  </select>
							</div>
						</div>
					  </div>
					  <br>
					  <br>
					  <div class="form-group">
						<label>Company Name</label>
						<input type="text" name="company_name" class="form-control" placeholder="Enter company name" value="<?php echo $company->company_name;?>">
					  </div>
					  <div class="form-group">
						<label>Address</label>
						<input type="text" name="company_address" class="form-control" placeholder="Enter address" value="<?php echo $company->company_address;?>">
					  </div>
					  <div class="form-group">
						<label>Company Contact Number</label>
						<input type="text" name="company_contactno" class="form-control" placeholder="Company Contact Number" value="<?php echo $company->company_contactno;?>">
					  </div>
						<div class="form-group">
				<label>Username</label>
				<input type="text" name="username" class="form-control" placeholder="Enter Username" value="<?php echo $company->username;?>">
			  </div>
			  <div class="row">
			  <div class="col-md-6">
				  <div class="form-group">
					<label>Password</label>
					<input type="password" name="password" class="form-control" placeholder="Password" value="<?php echo $company->password;?>">
				  </div>
			  </div>
			  <div class="col-md-6">
				  <div class="form-group">
					<label>Repeat Password</label>
					<input type="password" name="repeat_password" class="form-control" placeholder="Repeat Password" value="<?php echo $company->repeat_password;?>">
				  </div>
			  </div>
			  </div>
				<div class="row">
					<div class="col-md-6">
						  <div class="form-group">
							<label>City</label>
							<input type="text" name="city" class="form-control" placeholder="Enter city" value="<?php echo $company->city;?>">
						  </div>
					</div>
					<div class="col-md-6">
					<div class="form-group">
					<label>State</label>
						<select name="state" class="form-control" value="<?php echo $company->state;?>">
							<?php
								foreach($states as $s):
									if($s=="Please select a state."){
										echo '<option value="">'.$s.'</option>';
										continue;
									}	
									echo '<option value="'.$s.'">'.$s.'</option>';
								endforeach;
							?>
						</select>
					</div>
					</div>
				</div>
				  <div class="form-group">
					<label>Zip</label>
					<input type="text" name="zip" class="form-control" placeholder="Enter zip" value="<?php echo $company->value;?>">
				  </div>
				  <div class="form-group">
					<label>Category</label>
					<select name="category" class="form-control" value="<?php echo $company->category;?>">
					  <option value="">Please select a category</option>
					  <option value="1">Retirement Communities</option>
					  <option value="2">In-home Services & Caregiver Agencies</option>
					  <option value="3">Legal, Financial & Real Estate</option>
					  <option value="4">Senior Care Management</option>
					  <option value="5">Residential Placement</option>
					  <option value="6">Other Senior Resources</option>
					  <option value="7">Community Partners</option>
					</select>
				  </div>
				  <div class="form-group">
				  <label>Capacity</label>
						<input type="text" name="capacity" class="form-control" placeholder="Capacity">
				  </div>
				  <label>Company Type</label><br><br>
				  <div class="row">
					<div class="col-md-3 col-md-offset-2">
							
						  <input type="checkbox" name="company_type" value="1" <?php echo $company->company_type=="1" ? 'checked': ''; ?>> CCRC		<br>
						  <input type="checkbox" name="company_type" value="2" <?php echo $company->company_type=="2" ? 'checked': ''; ?>> RCFE		<br>
						  <input type="checkbox" name="company_type" value="3" <?php echo $company->company_type=="3" ? 'checked': ''; ?>> Ind		<br>
						  <input type="checkbox" name="company_type" value="4" <?php echo $company->company_type=="4" ? 'checked': ''; ?>> Asst Lvg	<br>
						  <input type="checkbox" name="company_type" value="5" <?php echo $company->company_type=="5" ? 'checked': ''; ?>> Day Care	<br>
						  <input type="checkbox" name="company_type" value="6" <?php echo $company->company_type=="6" ? 'checked': ''; ?>> Secured   <br>
						  <input type="checkbox" name="company_type" value="7" <?php echo $company->company_type=="7" ? 'checked': ''; ?> selected> SNF   <br>
					</div>
					<div class="col-md-7">
						  <input type="checkbox" name="company_type" value="8" <?php  echo $company->company_type=="8" ? 'checked': '';?> > Hospice          <br>
						  <input type="checkbox" name="company_type" value="9" <?php  echo $company->company_type=="9" ? 'checked': '';?>> Home Care        <br>
						  <input type="checkbox" name="company_type" value="10" <?php echo $company->company_type=="10" ? 'checked': ''; ?>> Referral Agency  <br>
						  <input type="checkbox" name="company_type" value="11" <?php echo $company->company_type=="11" ? 'checked': ''; ?>> Board & Care 	   <br>
						  <input type="checkbox" name="company_type" value="12" <?php echo $company->company_type=="12" ? 'checked': ''; ?>> Home Health Care <br>
						  <input type="checkbox" name="company_type" value="13" <?php echo $company->company_type=="13" ? 'checked': ''; ?>> Miscellaneous Support <br><br><br>
					</div>
				  </div>
				  <div class="row">
						<div class="col-md-6">
						  <div class="form-group">
							<label>Phone number</label>
							<input type="text" name="phone_number" class="form-control" placeholder="Enter phone number" value="<?php echo $company->phone_number;?>">
						  </div>
						</div>
						<div class="col-md-6">
						  <div class="form-group">
							<label>Fax number</label>
							<input type="text" name="fax_number" class="form-control" placeholder="Enter fax number" value="<?php echo $company->fax_number;?>">
						  </div>
						</div>
				  </div>
				  <div class="form-group">
						<label>Website</label>
						<input type="text" name="website" class="form-control" placeholder="Enter Url" value="<?php echo $company->website;?>">
				  </div>
				  <div class="form-group">
						<label>Email Address</label>
						<input type="email" name="email_address" class="form-control" placeholder="Enter email address" value="<?php echo $company->email_address;?>">
				  </div>
				  <label>Contact Person 1</label>
				  <div class="row">
					<div class="col-md-4">
					  <div class="form-group">
						<input type="text" name="first_name" class="form-control" placeholder="First name" value="<?php echo $company->first_name;?>">
					  </div>
					</div>
					<div class="col-md-4">
					  <div class="form-group">
						<input type="text" name="middle_name" class="form-control" placeholder="Middle name" value="<?php echo $company->middle_name;?>">
					  </div>
					</div>
					<div class="col-md-4">
					  <div class="form-group">
						<input type="text" name="last_name" class="form-control" placeholder="Last name" value="<?php echo $company->last_name;?>">
					  </div>
					</div>
				  </div>
				<div class="row">
					<div class="col-md-6">
					  <div class="form-group">
						<label>Title</label>
						<input type="text" name="title" class="form-control" placeholder="Title" value="<?php echo $company->title;?>">
					  </div>
					</div>
					<div class="col-md-6">
					  <div class="form-group">
						<label>Email</label>
						<input type="text" name="contact_email" class="form-control" placeholder="Email" value="<?php echo $company->contact_email;?>">
					  </div>
					</div>
				</div>
				<label>Contact Person 2</label>
				<div class="row">
					<div class="col-md-4">
					  <div class="form-group">
						<input type="text" name="first_name2" class="form-control" placeholder="First name" value="<?php echo $company->first_name2;?>">
					  </div>
					</div>
					<div class="col-md-4">
					  <div class="form-group">
						<input type="text" name="middle_name2" class="form-control" placeholder="Middle name" value="<?php echo $company->middle_name2;?>">
					  </div>
					</div>
					<div class="col-md-4">
					  <div class="form-group">
						<input type="text" name="last_name2" class="form-control" placeholder="Last name" value="<?php echo $company->last_name2;?>">
					  </div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
					  <div class="form-group">
						<label>Title</label>
						<input type="text" name="title2" class="form-control" placeholder="Title" value="<?php echo $company->title2;?>">
					  </div>
					</div>
					<div class="col-md-6">
					  <div class="form-group">
						<label>Email</label>
						<input type="text" name="contact_email2" class="form-control" placeholder="Email" value="<?php echo $company->contact_emil;?>">
					  </div>
					</div>
				</div><br><br>
				<div class="form-group">
					<label>Alz</label>
					<div>
						<input type="text" name="alz" class="form-control" placeholder="Alz" value="<?php echo $company->alz;?>">
					</div>
				</div>
				<div class="form-group">
					<label>Other</label>
					<div>
						<textarea name="other" class="form-control" rows="3" placeholder="Other" value="<?php echo $company->other;?>"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label>Description</label>
					<div>
						<textarea name="description" class="form-control" rows="3" placeholder="Description" value="<?php echo $company->description;?>"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label>Res Qual</label>
					<div>
						<textarea name="res_qual" class="form-control" rows="3" placeholder="Res Qual" value="<?php echo $company->res_qual;?>"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label>Private</label>
					<div>
						<select name="private1" class="form-control">
							<option value="No" <?php echo $company->company_type=="No" ? 'selected': ''; ?>>No</option>
							<option value="Yes" <?php echo $company->company_type=="Yes" ? 'selected': ''; ?>>Yes</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label>SSI</label>
					<div>
						<select name="ssi" class="form-control">
							<option value="No" <?php echo $company->company_type=="No" ? 'selected': ''; ?>>No</option>
							<option value="Yes" <?php echo $company->company_type=="Yes" ? 'selected': ''; ?>>Yes</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label>Medi-Cal</label>
					<div>
						<input type="text" name="medi_cal" class="form-control" placeholder="Medi-Cal" value="<?php echo $company->medi_cal;?>">
					</div>
				</div>
				<div class="form-group">
					<label>Medicare</label>
					<div>
						<input type="text" name="medicare" class="form-control" placeholder="Medicare" value="<?php echo $company->medicare;?>">
					</div>
				</div>
				<div class="form-group">
					<label>Other 2</label>
					<div>
						<textarea name="other2" class="form-control" rows="3" placeholder="Other 2" value="<?php echo $company->other2;?>"></textarea>
					</div>
				</div>
				<div style="text-align: center;">
					<input type="submit" class="btn btn-default" name="btnedit" value="Update">
				</div>
				</form>
			</div>
		</div>
</div>	