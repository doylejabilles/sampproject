<?php

	// error_reporting(E_ALL);
	// ini_set('display_errors', 1);
	
	get_common(array('config', 'Form', 'DB', 'Session'));
	get_model(array('Company', 'CompanyDAO', 'Admin', 'AdminDAO'));
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$a_var = new Admin();
	$a_var = new AdminDAO();
	
	//echo $userid = $_SESSION['ID'];
	
	$id = Session::get('ID');
	$admin = $c_dao->getRow("admin", "id='$id'");
	// $company = $c_dao->getJoin('c.company_id, c.company_name, c.company_address, c.company_contactno, tc.company_id AS tc_id', "companies", "c.account_type='Member' AND c.company_name LIKE '%$search_val%'",'c.company_name','ASC');
	$company = $c_dao->get('companies', "account_type='Member'", 'company_name','ASC');
	$company_search = $c_dao->get('companies', "account_type='Member' AND company_name LIKE '%$search_val%'", 'company_name','ASC');
	// $company_search = $c_dao->getJoin('c.company_id, c.company_name, c.company_address, c.company_contactno, tc.company_id AS tc_id', "companies", "c.account_type='Member' AND c.company_name LIKE '%$search_val%'",'c.company_name','ASC');
	// echo '<pre>'; print_r($company); '<pre>'; exit();
?>

<div style="text-align:center">
	<form method="POST" id="search-members-form-admin" url="<?php bloginfo('template_url'); ?>/systemapplication/ajaxadminmembersearch.php" blog-info="<?php bloginfo('template_url'); ?>">
		<label class="form-label">Search </label>
		<input class="validate[required] text-input form-input" type="text" id="search_val" name="search_val" placeholder="Type the Company">
		<input class="form-btn" type="submit" name="Submit" value="Search">
	</form>
</div>
<div id="notify4"></div>
<div class="admin_member_list" style="display:none;">
	<?php foreach($company_search as $cs): ?>
		<div>
		
		</div>
	<?php endforeach; ?>
</div>

<div class="clearfix">
	<div class="pop-up" style="position:absolute; right:15px; top:55px;">
		<a href="javascript:;" data-toggle="modal" data-target="#edit-admin-info-modal" title="Edit Info"><img src="<?php bloginfo('template_url'); ?>/images/edit_adminpanel_login_icon.png" alt="img"/></a>
		&nbsp;&nbsp;&nbsp;
		<a href="charity-organization-mailing-list" target="_blank" title="View/Add Mailing List"><img src="<?php bloginfo('template_url') ?>/images/mail_list_icon.png" alt="img"/></a>&nbsp;&nbsp;&nbsp;
		<a href="charity-organization-event-registration" target="_blank" title="Add an Event"><img src="<?php bloginfo('template_url') ?>/images/add_event_icon.png" alt="img"/></a>&nbsp;&nbsp;&nbsp;
		<a href="charity-organization-registration" target="_blank" title="Add a Member"><img src="<?php bloginfo('template_url') ?>/images/add_member_icon.png" alt="img"></a>
		&nbsp;&nbsp;&nbsp;
		<!--<a href="<?php //bloginfo('template_url') ?>/systemapplication/downloademails.php" target="_blank" title="Download Member Emails"><img src="<?php //bloginfo('template_url'); ?>/images/download_email_icon.png" alt="img"/></a>-->
	</div>
	<div id="notify"></div>
	<div class="table-responsive" id="admin-search">
		<span style="display:none" id="bloginfo" bloginfo="<?php echo bloginfo('template_url');?>"></span>
		<form method="POST">
			<table class="table text-left table-condensed">
				<th>Company Name</th>
				<th>Company Address</th>
				<th>Company Contact</th> 
				<th>Status</th>
				<th colspan="2"></th>
				<?php
				if( !empty($company) ) {
				foreach($company as $c): ?>
				<tr>
					<td><?php echo $c->company_name; ?></td>
					<td><?php echo $c->company_address; ?></td>
					<td><?php echo $c->company_contactno; ?></td>
					<td colspan="2"><?php echo $c->status; ?>&nbsp;<a id="<?php echo $c->company_id; ?>" class="table_status_form" href="javascript:;" data-toggle="modal" data-target="#UpdateStatus"><img src="wp-content/themes/marketingadmissions/images/update_status_icon.png" title="Update Status"></a>
					</td>
					<td><a target="_blank" href="charity-organization-admin-edit-user?id=<?php echo $c->company_id; ?>" id="<?php echo $c->company_id; ?>" class="editinfo" title="View/Edit"><img src="<?php bloginfo('template_url') ?>/images/edit_icon.png" alt="img"/></a></td>
					<td><a id="<?php echo $c->company_id; ?>" class="deleteinfo" href="<?php bloginfo('template_url'); ?>/systemapplication/ajaxadmin.php" blog-info="<?php bloginfo('template_url'); ?>" title="Delete"><img src="<?php bloginfo('template_url') ?>/images/delete_icon.png" alt="img"/></a></td>
				</tr>
				<?php
				endforeach;
				} 
				else { ?>
				<tr><td colspan="6">No records found.</td></tr>
				<?php				
				} ?> 
			</table>
		</form>
	</div>
</div>
 
<!-- Modal -->
	<?php echo get_subpage('views/editAdminInfo'); ?>
	
	<?php echo get_subpage('views/adminStatusUpdate'); ?>
	
	<?php echo get_subpage('views/adminStatusUpdate2'); ?>
	

<!-- Tooltip -->
<!--<div class="tooltip top" role="tooltip">
  <div class="tooltip-arrow"></div>
  <div class="tooltip-inner">
   View/Add Mailing List
  </div>
</div>-->
