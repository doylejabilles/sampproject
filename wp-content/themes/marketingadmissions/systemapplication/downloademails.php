<?php
	/*
	require_once('common/config.php');
	require_once('common/DB.php');
	require_once('model/Company.php');
	require_once('model/CompanyDAO.php');
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$emails = $c_dao->get('companies', $where='', 'company_name', 'ASC', '', 0, 0, 'company_name, email_address');*/
	
	// Connect to database server and select database
	$con = mysql_connect('localhost','link2new_prouser','dbWeAVerp14');
	mysql_select_db('link2new_dbname37', $con);

	// retrive data which you want to export
	$query = "SELECT company_name, email_address FROM companies ORDER BY company_name ASC ";
	$export = mysql_query ($query ) or die ( "Sql error : " . mysql_error( ) );

	if (PHP_SAPI == 'cli')
		die('This example should only be run from a Web Browser');

	/** Include PHPExcel */
	require_once 'common/Classes/PHPExcel.php';

	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();

	// Set document properties
	$objPHPExcel->getProperties()->setCreator("MAPS")
								 ->setLastModifiedBy("MAPS")
								 ->setTitle("Office 2007 XLSX MAPS Document")
								 ->setSubject("Office 2007 XLSX MAPS Document")
								 ->setDescription("MAPS Document")
								 ->setKeywords("office 2007 openxml php")
								 ->setCategory("MAPS result file");


	// Add some data
	$ctr = 2;
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	// header
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'Company Name')
				->setCellValue('B1', 'Email Address');
	// cell values
	while( $row = mysql_fetch_row( $export ) ) {
		$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$ctr, $row[0])
				->setCellValue('B'.$ctr, $row[1]);
		$ctr++;
	}

	// Rename worksheet
	$objPHPExcel->getActiveSheet()->setTitle('company_emails');


	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);


	// Redirect output to a client’s web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="company_emails.xls"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
	exit;
?>