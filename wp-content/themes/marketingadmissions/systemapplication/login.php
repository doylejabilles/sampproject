<?php
	get_common(array('config', 'Form', 'DB', 'Session', 'class.upload'));
	get_model(array('Company', 'CompanyDAO', 'Member', 'MemberDAO', 'Admin'));
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$m_var = new Member();
	$m_dao = new MemberDAO();
	
	$a_var = new Admin();
	
	$errors = array();
	$error = '';
	$success = '';
	
	$company_id = Session::get('ID');
	$company = $c_dao->getRow("companies", "company_id='$c_var->username'");
	$admin = $c_dao->getRow("admin", "id='$a_var->username'");
		
	if(isset($_POST['submit']) && ($_POST['submit'] == 'submit') ) {
		$c_var->username = Form::data('username');
		$c_var->password = Form::data('password');
		$a_var->username = Form::data('username');
		$a_var->password = Form::data('password');
			
		if( empty($c_var->username) || empty($c_var->password) || empty($a_var->username) || empty($a_var->password) ){
			$errors[]="All fields are required!";
		}
	
		if(count($errors) > 0 ) {
			$error = implode('<br/>', $errors);
		} else { 
			$company = $c_dao->getRow("companies", "username='". $c_var->username ."' AND password='". $c_var->password ."'");
			$admin = $c_dao->getRow("admin", "username='". $a_var->username ."' AND password='". $a_var->password ."'");
			// print_r($company);
			// print_r($admin); exit();
			if($company) {
				// login for member
				Session::start($company);
				echo "<script>window.parent.location = 'charity-organization-my-profile'</script>"; exit();

			} elseif($admin) {
				//login for admin
				Session::start($admin, 'admin');
				echo "<script>window.parent.location = 'charity-organization-admin'</script>"; exit();
			} else {
				// $errors[] = 'Username and password doesn\'t exist.';
				echo '<script>alert("Username and password doesn\'t exist");</script>'; 
			}
		}
	}

?>
<style>
#notify { color: #BA0001; font-size: 15px; }
.member-list-content { color: #084D52 }
.member-list-content h1{ color: #084D52 }
.member-list-content a{ color: #084D52 }
.member-list-content label{ color: #084D52 }
.member-list-content a:hover{ color: #1AB7DF }
</style>
<div class="row clearfix member-list-content"><br>
<div id="notify"><?php echo (!empty($error)) ? Form::error($error) : ''; echo (!empty($success)) ? Form::success($success) : ''; ?></div>
  <div class="col-md-9 col-md-offset-1 alert alert-info">
	<form role="form" method="POST" id="login-form">
	  <div class="form-group">
		<label><span class="required">*</span>Username</label>
		<input  name="username" type="text" class="form-control validate[required]" placeholder="Enter username">
	  </div>
	  <div class="form-group">
		<label><span class="required">*</span>Password</label>
		<input name="password" type="password" class="form-control validate[required]" placeholder="Enter password">
	  </div>
	  <button type="submit" class="btn btn-default" value="submit" name="submit" >Submit</button>
	</form>
	<div class="text-center" style="color: #000;">
		Forgot your password? <a href="charity-organization-forgot-password"  title="Forgot Password" style="color: #084D52;">Click here!</a></br> 
		For instruction <a href="javascript:;" data-toggle="modal" data-target="#instruction" style="color: #084D52;">Click here!</a><br/>
		Not a member yet? <a href="charity-organization-registration" title="Register here!" style="color: #084D52;">Register here!</a>
	</div>
   </div>
</div>

<!--Modal-->
<div class="modal fade" id="instruction" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content alert-info">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel" style="color: #084D52;"><strong>Instruction</strong></h4>
      </div>
      <div class="modal-body member-list-content">
		<ul>
			<li>Sign in using the username and password you have used when you registered.</li>
			<li>If you have forgot your password, plase click the link in the "Forgot password?"</li>
			<li>If not yet a member, please click the link in the "Not a member yet?"</li>
		</ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
