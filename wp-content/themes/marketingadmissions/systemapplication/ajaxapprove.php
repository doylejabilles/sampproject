<?php
	require_once('common/config.php');
	require_once('common/Form.php');
	require_once('common/DB.php');
	require_once('common/Session.php');
	require_once('model/Company.php');
	require_once('model/CompanyDAO.php');
	require_once('model/Member.php');
	require_once('model/MemberDAO.php');
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$m_var = new Member();
	$m_dao = new MemberDAO();
	
	$success = '';
	
	if($_POST) {
		$id = Form::data('id');
		$temp_company = $c_dao->getRow("temp_companies", "company_id=".$id);
		$c_dao->updateProfile($temp_company, "companies");
		$c_dao->delete("temp_companies", "WHERE company_id=".$id);
	}
?>