<?php

	get_common(array('config', 'Form', 'DB', 'Session'));
	get_model(array('Company', 'CompanyDAO', 'Admin'));
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$id = Session::get('ID');
	$admin = $c_dao->getRow("admin", "id='$id'");
	$mail = $c_dao->get("mailing_list","","company_name","ASC");
	
	$active = ( isset($_SESSION['ACTIVE']) && ($_SESSION['ACTIVE'] == 1) ) ? true : false;
	$type = ( isset($_SESSION['TYPE']) && ($_SESSION['TYPE'] == "member") ) ? 'member' : 'admin';
	
	$error = '';
	
	if(is_page('charity-organization-mailing-list') && !$active ) {
		echo "<script>window.location = 'charity-organization-login';</script>";
	} elseif( is_page('charity-organization-mailing-list') && $type == 'member' && $active ){
		echo '<script>alert("Only the admin can access this page!");window.location = "charity-organization-login";</script>';
	} else {

		if( isset($_POST['mailing_btn']) ){
		
			$c_var->company_name = Form::data('company_name');
			$c_var->company_contactno = Form::data('company_contactno');
			$c_var->email_address = Form::data('email_address');
			$c_var->contact_person = Form::data('contact_person');
			
			if( empty($c_var->company_name) || empty($c_var->company_contactno) || empty($c_var->contact_person) ){
				echo '<script>Pls fill in all the fields</script>';
			} 
			if( count($errors) > 0 ) {
				$error = implode('<br/>', $errors);
			} elseif( $c_dao->addMail($c_var, 'mailing_list') ){
				// print_r($c_dao); exit();
				echo '<script>alert("It has been succesfully added!");window.location = "charity-organization-mailing-list";</script>';
			}
		
		}
	}
	
?>
<style>

.mailing_list_content td{

		
		
	}

</style>
<div class="clearfix" style="color: #084D52! important; width: 100% !important;">
	<!--<div class="pop-up" style="position:absolute; right:15px; top:55px;">
		<a href="<?php //bloginfo('template_url') ?>/systemapplication/downloadmailinglist.php" target="_blank" title="Download Member Emails"><img src="<?php //bloginfo('template_url'); ?>/images/download_email_icon.png" alt="img"/></a>
	</div>-->
	<div class="col-md-12">
		<form role="form" method="POST" id="mailing">
			<div class="form-group">
				<span class="required">*</span><label>Company Name</label>
				<input type="text" name="company_name" class="form-control validate[required]">				
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<span class="required">*</span><label>Contact Number</label>
						<input type="text" name="company_contactno" class="form-control validate[required]">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Email</label>
						<input type="text" name="email_address" class="form-control">
					</div>
				</div>	
				<div class="col-md-4">
					<div class="form-group">
						<span class="required">*</span><label>Contact Person</label>
						<input type="text" name="contact_person" class="form-control validate[required]">
					</div>
				</div>
			</div>
			<div style="text-align:center;">
				<diV class="form-group">		
					<button type="submit" class="btn btn-default" id="mailing_btn" name="mailing_btn">Add</button>
				</div>
			</div>
		</form>
	</div>
	<div style="text-align:center">
		<form method="POST" id="mailing_search_form" url="<?php bloginfo('template_url'); ?>/systemapplication/ajaxAdminMailingList.php" blog-info="<?php bloginfo('template_url'); ?>">
			<label class="form-label">Search </label>
			<input class="validate[required] text-input form-input" type="text" id="searchmail" name="searchmail" placeholder="Type Company or Contact Person">
			<input class="form-btn" type="submit" name="Submit" value="Search">
		</form>
	</div>
	<div class="mailing-list" style="display:none;">
	<p>
	<?php foreach($mail as $m): ?>	
		<?php //$members = $m_dao->get('members', 'company_id='.$c->company_id); ?>
		<a href="charity-organization-mailing-list?id=<?php echo $m->id; ?>"><?php echo $m->company_name; ?>&nbsp;<?php echo $m->contact_person; ?></a><br>
		<?php 
			// if(!empty($c->company_address))
				// echo $c->company_address.'<br/>';
		?>
		<?php 
			// if(!empty($c->company_contactno))
				// echo $c->company_contactno.'<br/>';
		?>
		<?php 
			// if(!empty($c->zip))
				// echo $c->zip.'<br/>';
		?>
		<?php 
			// if(!empty($c->category))
				// echo $c->category.'<br/>';
		?>
	    <?php //foreach($members as $m):
				// echo ucfirst($m->fname).' '.ucfirst($m->lname).', '.$m->title.' '.$m->contact_no.'<br>';
			  // endforeach; ?>
			  <br>
	<?php endforeach; ?>
	</p>
	</div>
	<div class="table-responsive">
		<table class="table text-left table-condensed mailing_list_content" style="width: 150%; font-size: 12px;">
			<th>Company Name</th>
			<!--<th>Contact Number</th>-->
			<th>Email</th>
			<th>Contact Person</th>
			<th></th>
			<?php if( !empty($mail) ) {
				foreach($mail as $m): ?>
				<tr>
					<td> <?php echo $m->company_name; ?> </td>
					<!--<td> <?php //echo $m->company_contactno; ?> </td>-->
					<td> <a href="mailto:<?php echo $m->email_address; ?>"><?php echo $m->email_address; ?></a> </td>
					<td> <?php echo $m->contact_person; ?> </td>
					<td style="text-align: center"> <a id="<?php echo $m->id; ?>" class="deletemail" href="<?php bloginfo('template_url'); ?>/systemapplication/mailingajax.php" blog-info="<?php bloginfo('template_url'); ?>" title="Delete"><img src="<?php bloginfo('template_url') ?>/images/delete_icon.png" alt="img"/></a> </td>
				</tr>
			<?php endforeach;
				} 
				else { ?>
				<tr><td colspan="4">No records found.</td></tr>
			<?php } ?>
		</table>
	</div>
</div>

	