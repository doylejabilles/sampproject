<?php
	get_common(array('config', 'Form', 'DB', 'Session'));
	get_model(array('Company', 'CompanyDAO'));
	get_model(array('Member', 'MemberDAO'));
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$m_var = new Member();
	$m_dao = new MemberDAO();
	$companies = $c_dao->get('companies',"account_type='Member'",'company_name','ASC');
	
?>
<style>
.member-list-content a { color: #084D52 }
</style>
<div class="member-list-content">
<p><strong>Listed below are all members of MAPS.</strong></p>
<div>
	<form method="POST" id="search-members-form" url="<?php bloginfo('template_url'); ?>/systemapplication/ajaxsearchmember.php" blog-info="<?php bloginfo('template_url'); ?>">
		<label class="form-label">Search </label>
		<input class="validate[required] text-input form-input" type="text" id="searchval" class="autocomplete" name="searchval" placeholder="Type Company or Contact Person">
		<input class="form-btn" type="submit" name="Submit" value="Search">
	</form>
</div>
<br>
<div class="member-list">
	<p>
	<?php foreach($companies as $c): ?>	
		<?php $members = $m_dao->get('members', 'company_id='.$c->company_id); ?>
		<a href="charity-organization-member-profile?id=<?php echo $c->company_id; ?>"><?php echo $c->company_name; ?></a><br>
		<?php 
			// if(!empty($c->company_address))
				// echo $c->company_address.'<br/>';
		?>
		<?php 
			// if(!empty($c->company_contactno))
				// echo $c->company_contactno.'<br/>';
		?>
		<?php 
			// if(!empty($c->zip))
				// echo $c->zip.'<br/>';
		?>
		<?php 
			// if(!empty($c->category))
				// echo $c->category.'<br/>';
		?>
	    <?php //foreach($members as $m):
				// echo ucfirst($m->fname).' '.ucfirst($m->lname).', '.$m->title.' '.$m->contact_no.'<br>';
			  // endforeach; ?>
			  <br>
	<?php endforeach; ?>
	</p>
</div>
<?php if(count($companies)==0){?>
	<p>No records found</p>
<?php } ?>
</div>