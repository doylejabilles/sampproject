<?php 
	require_once('common/config.php');
	require_once('common/Form.php');
	require_once('common/DB.php');
	require_once('common/Session.php');
	require_once('model/Company.php');
	require_once('model/CompanyDAO.php');
	require_once('model/Admin.php');
	require_once('model/AdminDAO.php');
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$a_var = new Admin();
	$a_var = new AdminDAO();
	
	//echo $userid = $_SESSION['ID'];
	
	
	$return = array("edited" => false);
	
	if($_POST) {
		$c_var->company_id = Form::data('company_id');
		$c_var->status = Form::data('status');
		$c_dao->updateStatus($c_var, "companies");
		
		$return['edited'] = true;
		$return['companies'] = $c_dao->getRow("companies", 'company_id='.$c_var->company_id);
		echo json_encode($return);
		flush();
	}
?> 