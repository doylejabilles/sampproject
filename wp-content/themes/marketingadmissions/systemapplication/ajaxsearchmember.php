<?php
	require_once('common/config.php');
	require_once('common/Form.php');
	require_once('common/DB.php');
	require_once('common/Session.php');
	require_once('model/Company.php');
	require_once('model/CompanyDAO.php');
	require_once('model/Member.php');
	require_once('model/MemberDAO.php');
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$m_var = new Member();
	$m_dao = new MemberDAO();
	
	$searchval = $_POST['searchval'];
	
	$arraySearchVal = str_split($searchval);
	$apostrophe = "'";
	
	$newSearchVal = "";
	foreach($arraySearchVal as $asv):
		if($asv == $apostrophe){
			$newSearchVal .= "'".$asv;
		}else{
			$newSearchVal .= $asv;
		}
	endforeach;
	
	$searchval = $newSearchVal;
	
	$companies = $c_dao->get('companies', "company_name LIKE '%$searchval%' OR zip LIKE '%$searchval%' OR category LIKE '%$searchval%' OR city LIKE '%$searchval%' OR company_address LIKE '%$searchval%' OR company_contactno LIKE '%$searchval%'","company_name","ASC");
	$directoryMembers = $m_dao->get('members', "fname LIKE '%$searchval%' OR mname LIKE '%$searchval%' OR lname LIKE '%$searchval%' OR CONCAT(fname,' ',lname) LIKE '%$searchval%'","fname","ASC");
	
	$results = array();
?>
<style>
.member-list-content a { color: #084D52 }
</style>
<div class="member-list-content">
<a href="charity-organization-members-directory">Back to member's list</a>
<?php if(count($companies) > 0){ ?>
	<p>
		<?php foreach($companies as $c): ?>	
			<?php $results[] = $c->company_id;?>
			<?php $members = $m_dao->get('members', 'company_id='.$c->company_id); ?>
			<a href="charity-organization-member-profile?id=<?php echo $c->company_id; ?>"><?php echo $c->company_name; ?></a><br>
			<?php 
				// if(!empty($c->company_address))
					// echo $c->company_address.'<br>';
			?>
			<?php 
				// if(!empty($c->company_contactno))
					// echo $c->company_contactno.'<br>';
			?>
			<?php
				// if(!empty($c->zip))
					// echo $c->zip.'<br>';
			?>
			<?php
				// if(!empty($c->category))
					// echo $c->category.'<br>';
			?>
			<?php 
				// if(!empty($c->city))
					// echo $c->city.'<br>';
			?>
			<?php //foreach($members as $m):
					//echo ucfirst($m->fname).' '.ucfirst($m->lname).', '.$m->title.' '.$m->contact_no.'<br>';
				 // endforeach;?>
				  <br>
		<?php endforeach; ?>
	</p>
<?php } ?>
<?php if(count($directoryMembers)>0){?>
	<p>
		<?php foreach($directoryMembers as $m): ?>	
			<?php if (in_array($m->company_id, $results)) { continue; }  //if member already displayed, dont display again  ?>
			<?php $company = $c_dao->getRow('companies', 'company_id='.$m->company_id); ?>
			<a href="charity-organization-member-profile?id=<?php echo $m->company_id; ?>"><?php echo $company->company_name; ?></a><br>
			<?php //echo $company->company_address; ?><br>
			<?php //echo $company->company_contactno; ?><br>
			<?php //echo $company->zip; ?><br>
			<?php //echo $company->category; ?><br>
			<?php //echo $compnay->city; ?><br>
			<?php 
				// if(!empty($company->company_contactno))
					// echo $company->company_contactno.'<br>';
			?>
			<?php echo ucfirst($m->fname).' '.ucfirst($m->lname).', '.$m->title.' '.$m->contact_no.'<br><br>'; ?>
		<?php endforeach; ?>
	</p>
<?php } ?>
<?php if(count($companies)==0){?>
	<p>No records found</p>
<?php } ?>
</div>