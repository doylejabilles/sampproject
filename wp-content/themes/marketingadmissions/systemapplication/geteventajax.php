<?php
	
	require_once('common/config.php');
	require_once('common/Form.php');
	require_once('common/DB.php');
	require_once('common/Session.php');
	require_once('model/Company.php');
	require_once('model/CompanyDAO.php');
	require_once('model/Admin.php');
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$id = $_GET['id'];
	$get_event = $c_dao->getRow('event', 'id='.$id);
	
	echo json_encode($get_event);
	flush();
	
?>