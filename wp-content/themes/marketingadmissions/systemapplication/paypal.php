<?php
	
	get_common(array('config', 'Form', 'DB', 'Session', 'Paypal'));
	get_model(array('Company', 'CompanyDAO', 'Member', 'MemberDAO', 'Admin'));
	
	$temp = unserialize(serialize(Session::get("paypal_cvar")));
	$temp2 = unserialize(serialize(Session::get("paypal_mvar")));
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$m_var = new Member();
	$m_dao = new MemberDAO();
	
	
	if(isset($_POST['payment_button'])){
	
		$c_var->account_type = $temp->account_type;
		$c_var->company_name = $temp->company_name;
		$c_var->company_address = $temp->company_address;
		$c_var->company_contactno = $temp->company_contactno;
		$c_var->username = $temp->username;
		$c_var->password = $temp->password;
		$c_var->repeat_password = $temp->repeat_password;
		$c_var->city = $temp->city;
		$c_var->state = $temp->state;
		$c_var->zip = $temp->zip;
		$c_var->category = $temp->category;
		$c_var->company_type = $temp->company_type;
		$c_var->phone_number = $temp->phone_number;
		$c_var->fax_number = $temp->fax_number;
		$c_var->website = $temp->website;
		$c_var->email_address = $temp->email_address;
		$c_var->description = $temp->description;
		$c_var->capacity = $temp->capacity;
		$c_var->alz = $temp->alz;
		$c_var->other = $temp->other;
		$c_var->private1 = $temp->private1;
		$c_var->ssi = $temp->ssi;
		$c_var->medi_cal = $temp->medi_cal;
		$c_var->medicare = $temp->medicare;
		$c_var->other2 = $temp->other2;
		
		$m_var->fname = $temp2->fname;
		$m_var->mname = $temp2->mname;
		$m_var->lname = $temp2->lname;
		$m_var->title = $temp2->title;
		$m_var->cemail = $temp2->cemail;
		
		
		$credicard = $_POST['creditcardnum'];
		$payment_type = $_POST['payment_type'];
		$expirationdate = $_POST['exp_date'];
		$CVV = $_POST['cvv'];
		$firstname = $_POST['fname'];
		$lastname = $_POST['lname'];
		$address = $_POST['address'];
		$zip = $_POST['zip'];
		$city = $_POST['city'];
		$state = $_POST['state'];
		$phone = $_POST['phone1'];
		$email = $_POST['email'];
		$country = $_POST['country'];
		$street = $_POST['street'];
		
		
		$requestParams = array(
		'IPADDRESS' => $_SERVER['REMOTE_ADDR'], // Get our IP Address
		'PAYMENTACTION' => 'Sale'
		);

		$creditCardDetails = array(
			'CREDITCARDTYPE' => $payment_type,
			'ACCT' => $credicard,
			'EXPDATE' => $expirationdate, // Make sure this is without slashes (NOT in the format 07/2017 or 07-2017)
			'CVV2' => $CVV
		);

		$payerDetails = array(
			'FIRSTNAME' => $firstname,
			'LASTNAME' => $lastname,
			'COUNTRYCODE' => $country,
			'STATE' => $state,
			'CITY' => $city,
			'STREET' => $street,
			'ZIP' => $zip
		);

		$orderParams = array(
			'AMT' => '20', // This should be equal to ITEMAMT + SHIPPINGAMT
			'ITEMAMT' => '20',
			'SHIPPINGAMT' => '0',
			'CURRENCYCODE' => 'USD' // USD for US Dollars
		);

		$item = array(
			'L_NAME0' => 'Registration',
			'L_DESC0' => 'test',
			'L_AMT0' => '20',
			'L_QTY0' => '1'
		);
		
		// echo '<pre>'; print_r($c_var); echo '</pre>';
		// echo '<pre>'; print_r($m_var); echo '</pre>';
		// echo '<pre>'; print_r($requestParams); echo '</pre>';
		// echo '<pre>'; print_r($creditCardDetails); echo '</pre>';
		// echo '<pre>'; print_r($payerDetails); echo '</pre>';
		// echo '<pre>'; print_r($orderParams); echo '</pre>';
		// echo '<pre>'; print_r($item); echo '</pre>';
		// exit;
		
		
	   $paypal = new Paypal();
	   $response = $paypal->request('DoDirectPayment', $requestParams + $creditCardDetails + $payerDetails + $orderParams + $item);
	   // print_r($response); exit;
	   if( is_array($response) && $response['ACK'] == 'Success') { // Payment successful
	   
		if( $c_dao->addCompany($c_var, "companies") ) {
				//add a member
				$company_id = DB::LastId("companies", 'company_id');	// get the last company id inserted
				
				if( is_array($m_var->fname) ) {
					$ctr = count($m_var->fname);	// first name counter
				}
				
				for($i = 0; $i < $ctr; $i++) {
					if( !empty($_POST['fname'][$i]) && !empty($_POST['lname'][$i]) && !empty($_POST['title'][$i]) && !empty($_POST['cemail'][$i]) ) {
						$m_var->company_id = $company_id;
						$m_var->fname = $_POST['fname'][$i];
						$m_var->mname = $_POST['mname'][$i];
						$m_var->lname = $_POST['lname'][$i];
						$m_var->title = $_POST['title'][$i];
						$m_var->cemail = $_POST['cemail'][$i];
						$m_dao->addMember($m_var, "members");
					}
				}
	   
			}	   
	   
	   
	   // We'll fetch the transaction ID for internal bookkeeping
			echo '<script>alert("Thank you for registering, your transaction has been successful!");window.location = "charity-organization-login";</script>';
		//$transactionId = $response['TRANSACTIONID'];
		// echo exit($transactionId);
		// $adver_var->transactionId = $transactionId;
		// $check_success = true;
	   }
	   else
	   {
		$paypal_error = $response['L_LONGMESSAGE0'];
		// $check_success = false;
		Echo "There was an error processing Request!";
	   }
		 
	}
	
	
	/*Declaration*/
	
	$states = array('Please select a state.','Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District Of Columbia','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Puerto Rico','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virgin Islands','Virginia','Washington','West Virginia','Wisconsin','Wyoming');
	
	$country = array('Please your country.','ALAND ISLANDS','ALBANIA','ALGERIA','AMERICAN SAMOA','ANDORRA','ANGOLA','ANGUILLA','ANTARCTICA','ANTIGUA AND BARBUDA','ARGENTINA','ARMENIA','ARUBA','AUSTRALIA','AUSTRIA','AZERBAIJAN','BAHAMAS','BAHRAIN','BANGLADESH','BARBADOS','BELGIUM','BELIZE','BENIN','BERMUDA','BHUTAN','BOLIVIA','BOSNIA-HERZEGOVINA','BOTSWANA','BOUVET ISLAND','BRAZIL','BRITISH INDIAN OCEAN TERRITORY','BRUNEI DARUSSALAM','BULGARIA','BURKINA FASO','BURUNDI','CAMBODIA','CANADA','CAPE VERDE','CAYMAN ISLANDS','CENTRAL AFRICAN REPUBLIC','CHAD','CHILE','CHINA','CHRISTMAS ISLAND','COCOS (KEELING) ISLANDS','COLOMBIA','COMOROS','DEMOCRATIC REPUBLIC OF CONGO','CONGO','COOK ISLANDS','COSTA RICA','CROATIA','CYPRUS','CZECH REPUBLIC','DENMARK','DJIBOUTI','DOMINICA','DOMINICAN REPUBLIC','ECUADOR','EGYPT','EL SALVADOR','ERITERIA','ESTONIA','ETHIOPIA','FALKLAND ISLANDS (MALVINAS)','FAROE ISLANDS','FIJI','FINLAND','FRANCE','FRENCH GUIANA','FRENCH POLYNESIA','FRENCH SOUTHERN TERRITORIES','GABON','GAMBIA','GEORGIA','GERMANY','GHANA','GIBRALTAR','GREECE','GREENLAND','GRENADA','GUADELOUPE','GUAM','GUATEMALA','GUERNSEY','GUINEA','GUINEA BISSAU','GUYANA','HEARD ISLAND AND MCDONALD ISLANDS','HOLY SEE (VATICAN CITY STATE)','HONDURAS','HONG KONG','HUNGARY','ICELAND','INDIA','INDONESIA','IRELAND','ISLE OF MAN','ISRAEL','ITALY','JAMAICA','JAPAN','JERSEY','JORDAN','KAZAKHSTAN','KENYA','KIRIBATI','KOREA, REPUBLIC OF','KUWAIT','KYRGYZSTAN','LAOS','LATVIA','LESOTHO','LIECHTENSTEIN','LITHUANIA','LUXEMBOURG','MACAO','MACEDONIA','MADAGASCAR','MALAWI','MALAYSIA','MALDIVES','MALI','MALTA','MARSHALL ISLANDS','MARTINIQUE','MAURITANIA','MAURITIUS','MAYOTTE','MEXICO','MICRONESIA, FEDERATED STATES OF','MOLDOVA, REPUBLIC OF','MONACO','MONGOLIA','MONTENEGRO','MONTSERRAT','MOROCCO','MOZAMBIQUE','NAMIBIA','NAURU','NEPAL','NETHERLANDS','NETHERLANDS ANTILLES','NEW CALEDONIA','NEW ZEALAND','NICARAGUA','NICARAGUA','NIGER','NIUE','NORFOLK ISLAND','NORTHERN MARIANA ISLANDS','NORWAY','OMAN','PALAU','PALESTINE','PANAMA','PARAGUAY','PAPUA NEW GUINEA','PERU','PHILIPPINES','PITCAIRN','POLAND','PORTUGAL','QATAR','REUNION','ROMANIA','REPUBLIC OF SERBIA','RUSSIAN FEDERATION','RWANDA','SAINT HELENA','SAINT KITTS AND NEVIS','SAINT LUCIA','SAINT PIERRE AND MIQUELON','SAINT VINCENT AND THE GRENADINES','SAMOA','SAN MARINO','SAO TOME AND PRINCIPE','SAUDI ARABIA','SENEGAL','SEYCHELLES','SIERRA LEONE','SINGAPORE','SLOVAKIA','SLOVENIA','SOLOMON ISLANDS','SOMALIA','SOUTH AFRICA','SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS','SPAIN','SRI LANKA','SURINAME','SVALBARD AND JAN MAYEN','SWAZILAND','SWEDEN','SWITZERLAND','TAIWAN, PROVINCE OF CHINA','TAJIKISTAN','TANZANIA, UNITED REPUBLIC OF','THAILAND','TIMOR-LESTE','TOGO','TOKELAU','TONGA','TRINIDAD AND TOBAGO','TUNISIA','TURKEY','TURKMENISTAN','TURKS AND CAICOS ISLANDS','TUVALU','UGANDA','UKRAINE','UNITED ARAB EMIRATES','UNITED KINGDOM','UNITED STATES','UNITED STATES MINOR OUTLYING ISLANDS','URUGUAY','UZBEKISTAN','VANUATU');
	
?>


<div class="row clearfix">
	<div class="col-md-11 alert alert-info">
		<form role="form" id="paypal" method="POST">
			<div class="form-group">
				<label><span class="required">*</span>Country</label>
				<select class="form-control" name="country" value="<?php echo $country; ?>">
					<?php
					foreach($country as $c):
					if($c=="Please select your country."){
					echo '<option value="">'.$c.'</option>';
					continue;
					}	
					echo '<option value="'.$c.'">'.$c.'</option>';
					endforeach;
					?>
				</select>
			</div>
			<div class="form-group">
				<label><span class="required">*</span>Credit Card number</label>
				<input type="text"  name="creditcardnum" class="form-control validate[required]" placeholder="required">
			</div>
			<div class="form-group">
				<div class="control-label">
					<input type="radio" name="payment_type" value="Visa" checked><img src="wp-content/themes/marketingadmissions/images/icon-visa.png" width="51" style="position:relative; top:10px;">
					<input type="radio" name="payment_type" value="Master Card"><img src="wp-content/themes/marketingadmissions/images/icon-mastercard.png" width="51" style="position:relative; top:10px;">
					<input type="radio" name="payment_type" value="Discover"><img src="wp-content/themes/marketingadmissions/images/icon-discover.png" width="51" style="position:relative; top:10px;">
					<input type="radio" name="payment_type" value="American Express"><img src="wp-content/themes/marketingadmissions/images/icon-american-express.png" width="51" style="position:relative; top:10px;">
				</div>
			</div>
			<div class="form-group">
				<label><span class="required">*</span>Expiration Date</label>
				<input type="text"  name="exp_date" class="form-control validate[required]" placeholder="required, Make sure this is without slashes (you type it in this format 72017 NOT in the format 07/2017 or 07-2017)">
			</div>
			<div class="form-group">
				<label><span class="required">*</span>CVV</label>
				<input type="text"  name="cvv" class="form-control validate[required]" placeholder="required">
			</div>
			<div class="form-group">
				<label><span class="required">*</span>First Name</label>
				<input type="text"  name="fname" class="form-control validate[required]" placeholder="required">
			</div>
			<div class="form-group">
				<label><span class="required">*</span>Last Name</label>
				<input type="text"  name="lname" class="form-control validate[required]" placeholder="required">
			</div>
			<div class="form-group">
				<label><span class="required">*</span>Address</label>
				<input type="text"  name="address" class="form-control validate[required]" placeholder="required">	
			</div>
			<div class="form-group">
				<label><span class="required">*</span>Zip Code</label>
				<input type="text"  name="zip" class="form-control validate[required]" placeholder="required">
			</div><div class="form-group">
				<label><span class="required">*</span>Street</label>
				<input type="text"  name="street" class="form-control validate[required]" placeholder="required">
			</div>
			<div class="row">
			<div class="form-group">
				<div class="col-md-6">
					<label><span class="required">*</span> City</label>
					<input type="text" name="city" class="form-control validate[required]" placeholder="Enter city">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6">
					<label><span class="required">*</span> State</label>
					<select name="state" class="form-control validate[required]">
						<?php
							foreach($states as $s):
							if($s=="Please select a state."){
							echo '<option value="">'.$s.'</option>';
							continue;
							}	
							echo '<option value="'.$s.'">'.$s.'</option>';
							endforeach;
						?>
					</select>
				</div>
			</div>
			</div>  
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label><span class="required">*</span>Phone</label>
						<input type="text"  name="phone1" class="form-control validate[required]" placeholder="required">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label><span class="required">*</span>Email Address</label>
						<input type="text"  name="email" class="form-control validate[required,custom[email]]" placeholder="required">
					</div>
				</div>
			</div>
			<div class="form-group" align="center">
				<button type="submit" name="payment_button" class="btn btn-default">Submit</button>
			</div>
		</form>
	</div>
</div>