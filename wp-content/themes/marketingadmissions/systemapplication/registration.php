<?php

	include_once ('wp-content/themes/marketingadmissions/forms/form_details.php');
	if($smtp)
	include_once ('wp-content/themes/marketingadmissions/forms/phpmailer/sendmail.php');

	get_common(array('config', 'Form', 'DB', 'Session'));
	get_model(array('Company', 'CompanyDAO', 'Member', 'MemberDAO', 'Admin'));
	
	$c_var = new Company();
	$c_dao = new CompanyDAO();
	
	$m_var = new Member();
	$m_dao = new MemberDAO();
	
	$errors = array();
	$error = '';
	$success = '';
	
	$active = ( isset($_SESSION['ACTIVE']) && ($_SESSION['ACTIVE'] == 1) ) ? true : false;
	$type = ( isset($_SESSION['TYPE']) && ($_SESSION['TYPE'] == "member") ) ? 'member' : 'admin';
	
	$regexp = "/^[a-z0-9]+([_\\.-][a-z0-9]+)*@([a-z0-9]+([\.-][a-z0-9]+)*)+\\.[a-z]{2,}$/i";
	
	if(isset($_POST['continueSubmit'])){
		$c_var->account_type = Form::data('account_type');
		$c_var->company_name = Form::data('company_name');
		$c_var->company_address = Form::data('company_address');
		$c_var->company_contactno = Form::data('company_contactno');
		$c_var->username = Form::data('username');
		$c_var->password = Form::data('password');
		$c_var->repeat_password = Form::data('repeat_password');
		$c_var->city = Form::data('city');
		$c_var->state = Form::data('state');
		$c_var->zip = Form::data('zip');
		$c_var->category = Form::data('category');
		$temp_companytype = $_POST['company_type'];
		$c_var->other_type = Form::data('other_type');
		$c_var->phone_number = Form::data('phone_number');
		$c_var->fax_number = Form::data('fax_number');
		$c_var->website = Form::data('website');
		$c_var->email_address = Form::data('email_address');
		$c_var->description = Form::data('description');
		$c_var->capacity = Form::data('capacity');
		$c_var->alz = Form::data('alz');
		$c_var->other = Form::data('other');
		$c_var->private1 = Form::data('private1');
		$c_var->ssi = Form::data('ssi');
		$c_var->medi_cal = Form::data('medi_cal');
		$c_var->medicare = Form::data('medicare');
		$c_var->other2 = Form::data('other2');
		
		$m_var->fname = $_POST['fname'];
		$m_var->mname = $_POST['mname'];
		$m_var->lname = $_POST['lname'];
		$m_var->title = $_POST['title'];
		$m_var->cemail = $_POST['cemail'];
		
		Session::set("paypal_cvar", $c_var);
		Session::set("paypal_mvar", $m_var);
		
		echo '<script>window.location = "charity-organization-payment-option";</script>';
		
		
	}
	
	if(isset($_POST['registrationSubmit'])){
		
		$c_var->account_type = Form::data('account_type');
		$c_var->company_name = Form::data('company_name');
		$c_var->company_address = Form::data('company_address');
		$c_var->company_contactno = Form::data('company_contactno');
		$c_var->username = Form::data('username');
		$c_var->password = Form::data('password');
		$c_var->repeat_password = Form::data('repeat_password');
		$c_var->city = Form::data('city');
		$c_var->state = Form::data('state');
		$c_var->zip = Form::data('zip');
		$c_var->category = Form::data('category');
		$temp_companytype = $_POST['company_type'];
		$c_var->other_type = Form::data('other_type');
		$c_var->phone_number = Form::data('phone_number');
		$c_var->fax_number = Form::data('fax_number');
		$c_var->website = Form::data('website');
		$c_var->email_address = Form::data('email_address');
		$c_var->description = Form::data('description');
		$c_var->capacity = Form::data('capacity');
		$c_var->alz = Form::data('alz');
		$c_var->other = Form::data('other');
		$c_var->private1 = Form::data('private1');
		$c_var->ssi = Form::data('ssi');
		$c_var->medi_cal = Form::data('medi_cal');
		$c_var->medicare = Form::data('medicare');
		$c_var->other2 = Form::data('other2');
		$c_var->services = Form::data('services');
		$c_var->status = Form::data('status');
		
		// if( $_POST['optionsRadios'] == 'Cheque') {
			// $c_var->status = 0;
		// }
		
		$m_var->fname = $_POST['fname'];
		$m_var->mname = $_POST['mname'];
		$m_var->lname = $_POST['lname'];
		$m_var->title = $_POST['title'];
		$m_var->cemail = $_POST['cemail'];
			
			
		// $c_var->company_type = "";
		// foreach($temp_companytype as $t):
			// $c_var->company_type .= $t.', ';
		// endforeach;
		
		//validating the fields through PHP
		
		// echo '<pre>'; print_r($c_var); echo '</pre>';
		// echo '<pre>'; print_r($m_var); echo '</pre>'; exit();
		
		if(empty($c_var->company_name) || empty($c_var->company_address) || empty($c_var->company_contactno) || empty($c_var->username) || empty($c_var->password) || empty($c_var->repeat_password) || empty($c_var->city) || empty($c_var->state) || empty($c_var->zip) || empty($c_var->phone_number) || empty($c_var->email_address) || empty($c_var->description) ) {
			$errors[] = 'Please fill-in required fields that is marked with an asterisk.';
		} 
		// validates if no contact person is entered. should input at least 1 contact person
		elseif( empty($m_var->fname) || empty($m_var->lname) || empty($m_var->cemail) ) {
			$errors[] = 'Please enter at least one contact person.';
		}
		
		if( count($errors) > 0 ) {
			$error = implode('<br/>', $errors);
		} else {
			// check if email address format is correct
			if( !preg_match($regexp, $c_var->email_address) ) { 
				$errors[] = 'Invalid Email address.';
			}			
			
			if( $c_var->password != $c_var->repeat_password ){
				$errors[] = "Your password did not match";
			}			
			if( count($errors) > 0 ) {
				$error = implode('<br/>', $errors);
			}
			//add record to db
			elseif( $c_dao->addCompany($c_var, "companies") ) {
				//add a member
				$company_id = DB::LastId("companies", 'company_id');	// get the last company id inserted
				
				
				if( is_array($m_var->fname) ) {
					$ctr = count($m_var->fname);	// first name counter
					// echo '<pre>'; print_r($ctr); echo '</pre>'; exit;
				}
				
				for($i = 0; $i < $ctr; $i++) {
					// echo $_POST['fname'][$i].' '.$_POST['lname'][$i].' '.$_POST['title'][$i].' '.$_POST['cemail'][$i].'<br>'; 
					if( !empty($_POST['fname'][$i]) && !empty($_POST['lname'][$i]) && !empty($_POST['cemail'][$i]) ) {
						$m_var->company_id = $company_id;
						$m_var->fname = $_POST['fname'][$i];
						$m_var->mname = $_POST['mname'][$i];
						$m_var->lname = $_POST['lname'][$i];
						$m_var->title = $_POST['title'][$i];
						$m_var->cemail = $_POST['cemail'][$i];
					
						
						$m_dao->addMember($m_var, "members");
					}
				}
				
				//echo '<script>alert("Registration Complete!");window.location = "charity-organization-login";</script>';
				// $success = 'You have been registered!';
				echo '<pre>'; print_r($c_var->status); echo '<pre>'; exit();
				$register_update = $c_var->company_name = Form::data('company_name');
				$register_update2 = $c_var->company_address = Form::data('company_address');
				$register_update3 = $c_var->company_contactno = Form::data('company_contactno');
				$register_update4 = $c_var->username = Form::data('username');
				//$register_update5 = $c_var->password = Form::data('password');
				//$register_update6 = $c_var->repeat_password = Form::data('repeat_password');
				$register_update7 = $c_var->city = Form::data('city');
				$register_update8 = $c_var->state = Form::data('state');
				$register_update9 = $c_var->zip = Form::data('zip');
				$register_update10 = $c_var->category = Form::data('category');
				$register_update11 = $c_var->company_type = Form::data('company_type');
				$register_update12 = $c_var->phone_number = Form::data('phone_number');
				$register_update13 = $c_var->fax_number = Form::data('fax_number');
				$register_update14 = $c_var->website = Form::data('website');
				$register_update15 = $c_var->email_address = Form::data('email_address');
				$register_update16 = $c_var->description = Form::data('description');
				$register_update17 = $c_var->capacity = Form::data('capacity');
				$register_update18 = $c_var->alz = Form::data('alz');
				$register_update19 = $c_var->other = Form::data('other');
				$register_update20 = $c_var->private1 = Form::data('private1');
				$register_update21 = $c_var->ssi = Form::data('ssi');
				$register_update22 = $c_var->medi_cal = Form::data('medi_cal');
				$register_update23 = $c_var->medicare = Form::data('medicare');
				$register_update24 = $c_var->other2 = Form::data('other2');
				$register_update25 = $_POST['payment_status'];
				$register_update26 = $_POST['transaction_number'];
				
				$register_member = $m_var->fname = $_POST['fname'][$i];
				$register_member2 = $m_var->mname = $_POST['mname'][$i];
				$register_member3 = $m_var->lname = $_POST['lname'][$i];
				$register_member4 = $m_var->title = $_POST['title'];
				$register_member5 = $m_var->cemail = $_POST['cemail'];
				//var_dump($register_member); exit();
				
				//sending email notification
				if ($active == false) {
					//$body = 'A new member has registered!';
					$body = '<span style="font-weight: bold;">A new member has registered, see the info below:</span>' . '<br/>' .
					'<div class="form-group">'
					. '<label><span style="font-weight: bold;">Company Name:</span></label>' . ' ' . $register_update .
					'</div>' .
					'<div class="form-group">' . 
						'<label><span style="font-weight: bold;">Address:</span></label>' . ' ' . $register_update2 .
					'</div>' .
					'<div class="form-group">' .
						'<label><span style="font-weight: bold;">Company Contact Number:</span></label>' . ' ' . $register_update3 .
					'</div>' .
					'<div class="form-group">' .
						'<label><span style="font-weight: bold;">Username:</span></label>' . ' ' . $register_update4 .
					'</div>' .
					'<div class="row">' .
						'<div class="col-md-6">' .
							'<div class="form-group">' .
								'<label><span style="font-weight: bold;">City:</span></label>' . ' ' . $register_update7 .
							'</div>' .
						'</div>' .
						'<div class="col-md-6">' .
							'<div class="form group">' .
								'<label><span style="font-weight: bold">State:</span></label>' . ' ' . $register_update8 .
							'</div>' .
						'</div>' .
					'</div>' . 
					'<div class="form-group">' .
						'<label><span style="font-weight: bold">Zip:</span></label>' . ' ' . $register_update9 . 
					'</div>' .
					'<div class="form-group">' .
						'<label><span style="font-weight: bold">Category:</span></label>' . ' ' . $register_update10 .
					'</div>' .
					'<div class="form-group">' .
						'<label><span style="font-weight:bold;">Company type:</span></label>' . '' . $register_update11 .
					'</div>' .
					'<div class="row">' .	
						'<div class="col-md-6">' .
							'<div class="form-group">' .
								'<label><span style="font-weight: bold;">Phone Number:</span></label>' . ' ' . $register_update12 . 
							'</div>' .
						'</div>' .
						'<div class="col-md-6">' .
							'<div class="form-group">' .
								'<label><span style="font-weight: bold;">Fax Number:</span></label>' . ' ' . $register_update13 .
							'</div>' .
						'</div>' .
					'</div>' .
					'<div class="form-group">' .
						'<label><span style="font-weight:bold;">Website:</span></label>' . ' ' . $register_update14 .
					'</div>' .
					'<div class="form-group">' .
						'<label><span style="font-weight: bold;">Email:</span></label>' . ' ' . $register_update15 .
					'</div>' .
					'<div class="form-group">' .
						'<label><span style="font-weight: bold;">Description:</span></label>' . ' ' . $register_update16 .
					'</div>' .
					'<div class="form-group">' .
						'<label><span style="font-weight: bold;">Capacity:</span></label>' . ' ' . $register_update17 .
					'</div>' .
					'<div class="form-group">' .
						'<label><span style="font-weight: bold;">Alz:</span></label>' . ' ' . register_update18 .
					'</div>' . 
					'<div class="form-group">' .
						'<label><span style="font-weight: bold;">Other:</span></label>' . ' ' . $register_update19 .
					'</div>' .
					'<div class="form-group">' .
						'<label><span style="font-weight: bold;">Private:</span></label>' . ' ' . $register_update20 .
					'</div>' .
					'<div class="form-group">' .
						'<label><span style="font-weight: bold;">SSI:</span></label>' . ' ' . $register_update21 .
					'</div>' .
					'<div class="form-group">' .
						'<label><span style="font-weight: bold;">Medi-Cal:</span></label>' . ' ' . $register_update22 .
					'</div>' .
					'<div class="form-group">' .
						'<label><span style="font-weight: bold;">Medicare:</span></label>' . ' ' . $register_update23 .
					'</div>' .
					'<div class="form-group">' .
						'<label><span style="font-weight: bold;">Other:</span></label>' . ' ' . $register_update24 .
					'</div>' . '<br/>' . 'Please see all the details about this new member in site.';
					
					$subject = $dcomp . "[ Account Updated ]";		
					
					/************** for smtp ***********/
					if($smtp) { 
						$mail = new SendMail($host, $username, $password);
						$to_email = 'doylace@gmail.com';
						$cc = 'doyle.jabilles@hotmail.com';
						// $to_email = 'info@maps-sgv.org';
						$trysend = $mail->sendNow($to_email, $to_name, $cc, $bcc, $from_email, $from_name, $subject, $body);
						if ($trysend == 'ok')
							$sent = true;
						else
							$sent = false;
					}else {
					/************** for mail function ***********/
						$headers= 'MIME-Version: 1.0' . "\r\n";
						$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
						$headers .= "From: ".$from_name." <".$from_email.">\n";
						$headers .= "Cc: ".$cc."\n";
						$headers .= "Bcc: ".$bcc;
						if(mail($to_email, $subject, $body, $headers)) {
							$sent = true;				
						}else {
							$sent = false;
						}
					}
					
					if($sent) {
							// $prompt_message = '<div id="success">Status update has been successfully sent to your email.</div>';
							 // echo '<script>alert("Profile has been successfully updated!</script>';
							 echo '<script>alert("Registration Complete!");window.parent.location = "charity-organization-login";</script>';
							 // echo '<script>location.reload(); </script>';
							unset($_POST);
					}else {
							$prompt_message = '<div id="error">Failed to send email.</div>';				
					} //ending ni sa email notification		
				}
			}	
		}
	}
	
	
	/** Declaration for state **/
	$states = array('Please select a state.','Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District Of Columbia','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Puerto Rico','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virgin Islands','Virginia','Washington','West Virginia','Wisconsin','Wyoming');
	
	$country = array('Pleae your country.','ALAND ISLANDS','ALBANIA','ALGERIA','AMERICAN SAMOA','ANDORRA','ANGOLA','ANGUILLA','ANTARCTICA','ANTIGUA AND BARBUDA','ARGENTINA','ARMENIA','ARUBA','AUSTRALIA','AUSTRIA','AZERBAIJAN','BAHAMAS','BAHRAIN','BANGLADESH','BARBADOS','BELGIUM','BELIZE','BENIN','BERMUDA','BHUTAN','BOLIVIA','BOSNIA-HERZEGOVINA','BOTSWANA','BOUVET ISLAND','BRAZIL','BRITISH INDIAN OCEAN TERRITORY','BRUNEI DARUSSALAM','BULGARIA','BURKINA FASO','BURUNDI','CAMBODIA','CANADA','CAPE VERDE','CAYMAN ISLANDS','CENTRAL AFRICAN REPUBLIC','CHAD','CHILE','CHINA','CHRISTMAS ISLAND','COCOS (KEELING) ISLANDS','COLOMBIA','COMOROS','DEMOCRATIC REPUBLIC OF CONGO','CONGO','COOK ISLANDS','COSTA RICA','CROATIA','CYPRUS','CZECH REPUBLIC','DENMARK','DJIBOUTI','DOMINICA','DOMINICAN REPUBLIC','ECUADOR','EGYPT','EL SALVADOR','ERITERIA','ESTONIA','ETHIOPIA','FALKLAND ISLANDS (MALVINAS)','FAROE ISLANDS','FIJI','FINLAND','FRANCE','FRENCH GUIANA','FRENCH POLYNESIA','FRENCH SOUTHERN TERRITORIES','GABON','GAMBIA','GEORGIA','GERMANY','GHANA','GIBRALTAR','GREECE','GREENLAND','GRENADA','GUADELOUPE','GUAM','GUATEMALA','GUERNSEY','GUINEA','GUINEA BISSAU','GUYANA','HEARD ISLAND AND MCDONALD ISLANDS','HOLY SEE (VATICAN CITY STATE)','HONDURAS','HONG KONG','HUNGARY','ICELAND','INDIA','INDONESIA','IRELAND','ISLE OF MAN','ISRAEL','ITALY','JAMAICA','JAPAN','JERSEY','JORDAN','KAZAKHSTAN','KENYA','KIRIBATI','KOREA, REPUBLIC OF','KUWAIT','KYRGYZSTAN','LAOS','LATVIA','LESOTHO','LIECHTENSTEIN','LITHUANIA','LUXEMBOURG','MACAO','MACEDONIA','MADAGASCAR','MALAWI','MALAYSIA','MALDIVES','MALI','MALTA','MARSHALL ISLANDS','MARTINIQUE','MAURITANIA','MAURITIUS','MAYOTTE','MEXICO','MICRONESIA, FEDERATED STATES OF','MOLDOVA, REPUBLIC OF','MONACO','MONGOLIA','MONTENEGRO','MONTSERRAT','MOROCCO','MOZAMBIQUE','NAMIBIA','NAURU','NEPAL','NETHERLANDS','NETHERLANDS ANTILLES','NEW CALEDONIA','NEW ZEALAND','NICARAGUA','NICARAGUA','NIGER','NIUE','NORFOLK ISLAND','NORTHERN MARIANA ISLANDS','NORWAY','OMAN','PALAU','PALESTINE','PANAMA','PARAGUAY','PAPUA NEW GUINEA','PERU','PHILIPPINES','PITCAIRN','POLAND','PORTUGAL','QATAR','REUNION','ROMANIA','REPUBLIC OF SERBIA','RUSSIAN FEDERATION','RWANDA','SAINT HELENA','SAINT KITTS AND NEVIS','SAINT LUCIA','SAINT PIERRE AND MIQUELON','SAINT VINCENT AND THE GRENADINES','SAMOA','SAN MARINO','SAO TOME AND PRINCIPE','SAUDI ARABIA','SENEGAL','SEYCHELLES','SIERRA LEONE','SINGAPORE','SLOVAKIA','SLOVENIA','SOLOMON ISLANDS','SOMALIA','SOUTH AFRICA','SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS','SPAIN','SRI LANKA','SURINAME','SVALBARD AND JAN MAYEN','SWAZILAND','SWEDEN','SWITZERLAND','TAIWAN, PROVINCE OF CHINA','TAJIKISTAN','TANZANIA, UNITED REPUBLIC OF','THAILAND','TIMOR-LESTE','TOGO','TOKELAU','TONGA','TRINIDAD AND TOBAGO','TUNISIA','TURKEY','TURKMENISTAN','TURKS AND CAICOS ISLANDS','TUVALU','UGANDA','UKRAINE','UNITED ARAB EMIRATES','UNITED KINGDOM','UNITED STATES','UNITED STATES MINOR OUTLYING ISLANDS','URUGUAY','UZBEKISTAN','VANUATU','VENEZUELA','VIETNAM','VIRGIN ISLANDS, BRITISH','VIRGIN ISLANDS, U.S.','WALLIS AND FUTUNA','WESTERN SAHARA','YEMEN','ZAMBIA');
	
?>

<style type="text/css">
#notify { background-color:#fff ; width: 613px; margin-bottom: 10px; font-size: 25px; font-family: arial; color: #1E90FF; font-style: italic; border-radius: 3px; border:solid 2px #fff; }
.member-list-content { color: #084D52 }
.member-list-content h1{ color: #084D52 }
.member-list-content a{ color: #084D52 }
.member-list-content label{ color: #084D52 }
.member-list-content a:hover{ color: #1AB7DF }
</style>
<div class="row clearfix member-list-content">
	<div id="notify"><?php echo (!empty($error)) ? Form::error($error) : ''; echo (!empty($success)) ? Form::success($success) : ''; ?></div>
  <div class="col-md-11  alert alert-info">
	<form role="form" id="registration-form" method="POST">
	<div><h1 align="center">M.A.P.S.-SanGabrielValley <br/> MembershipApplication2014</h1></div><br/>
	<?php $active = ( isset($_SESSION['ACTIVE']) && ($_SESSION['ACTIVE'] == 1) ) ? true : false;
		  $type = ( isset($_SESSION['TYPE']) && ($_SESSION['TYPE'] == "member") ) ? 'member' : 'admin';	?>					
	<?php if ($active == true && $type == 'admin'): ?>
	  <div class="form-group">
		<label><span class="required">*</span>Account Type</label>
		<div class="row">
			<div class="col-xs-5">
			  <select name="account_type" class="form-control validate[required]">
				<option value="Member">Member</option>
				<option value="Resource">Resource</option>
			  </select>
			</div>
		</div>
	  </div>
	<?php endif; ?>
	  <br>
	  <br>
	<?php if($active == false): ?>
	  <div class="form-group" style="display:none;">
		<label>Account Type</label>
		<div class="row">
			<div class="col-xs-5">
			  <select name="account_type" class="form-control" readonly>
				<option value="Member">Member</option>
			  </select>
			</div>
		</div>
	  </div>
	 <?php endif; ?>
	  <div class="form-group">
		<label><span class="required">*</span> Company Name</label>
		<input type="text" name="company_name" class="form-control validate[required]" placeholder="Enter company name">
	  </div>
	  <div class="form-group">
		<label><span class="required">*</span> Address</label>
		<input type="text" name="company_address" class="form-control validate[required]" placeholder="Enter address">
	  </div>
	  <div class="row">
		<div class="col-md-6">
		  <div class="form-group">
			<label><span class="required">*</span> City</label>
			<input type="text" name="city" class="form-control validate[required]" placeholder="Enter city">
		  </div>
		</div>
		<div class="col-md-6">
		  <div class="form-group">
			<label><span class="required">*</span> Zip</label>
			<input type="text" name="zip" class="form-control validate[required]" placeholder="Enter zip">
		  </div> 
		</div>
	  </div>
	  <div class="form-group">
		<label><span class="required">*</span> State</label> 
		<select name="state" class="form-control validate[required]">
			<?php
				foreach($states as $s):
					if($s=="Please select a state."){
						echo '<option value="">'.$s.'</option>';
						continue;
					}	
					echo '<option value="'.$s.'">'.$s.'</option>';
				endforeach;
			?>
		</select>
	  </div>
	  <div class="form-group">
		<label><span class="required">*</span> Username</label>
		<input type="text" name="username" class="form-control validate[required]" placeholder="Enter Username">
	  </div>
	  <div class="row">
		  <div class="col-md-6">
			  <div class="form-group">
				<label><span class="required">*</span> Password</label>
				<input type="password" name="password" class="form-control validate[required]" placeholder="Password">
			  </div>
		  </div>
		  <div class="col-md-6">
			  <div class="form-group">
				<label><span class="required">*</span> Repeat Password</label>
				<input type="password" name="repeat_password" class="form-control validate[required]" placeholder="Repeat Password">
			  </div>
		  </div>
	  </div>
	  <div class="form-group">
		<label><span class="required">*</span> Company Contact Number</label>
		<input type="text" name="company_contactno" class="form-control validate[required]" placeholder="Company Contact Number">
	  </div>
	  <div class="form-group">
		<label> Category</label>
		<select name="category" class="form-control">
		  <option value="">Please select a category</option>
		  <option value="Retirement Communities">Retirement Communities</option>
		  <option value="In-home Services & Caregiver Agencies">In-home Services & Caregiver Agencies</option>
		  <option value="Legal, Financial & Real Estate">Legal, Financial & Real Estate</option>
		  <option value="Senior Care Management">Senior Care Management</option>
		  <option value="Referral and Placement Services">Referral and Placement Services</option>
		  <option value="Other Senior Resources">Other Senior Resources</option>
		  <option value="Community Partners">Community Partners</option>
		</select>
	  </div>
	  <div class="form-group">
	  <label>Capacity</label>
			<input type="text" name="capacity" class="form-control" placeholder="Capacity">
	  </div>
	  <label> Company Type</label><br><br>
	  <div class="row">
		<div class="col-md-3 col-md-offset-2" style="color: #084D52;!important" >
			  <input type="checkbox" name="company_type[]" class="" value="CCRC"> CCRC		<br>
			  <input type="checkbox" name="company_type[]" class="" value="RCFE"> RCFE		<br>
			  <input type="checkbox" name="company_type[]" class="" value="Ind"> Ind		<br>
			  <input type="checkbox" name="company_type[]" class="" value="Asst Lvg"> Asst Lvg	<br>
			  <input type="checkbox" name="company_type[]" class="" value="Day Care"> Day Care	<br>
			  <input type="checkbox" name="company_type[]" class="" value="Secured"> Secured   <br>
			  <input type="checkbox" name="company_type[]" class="" value="SNF"> SNF              <br>
		</div>
		<div class="col-md-7" style="color: #084D52;!important" >
			  <input type="checkbox" name="company_type[]" class="" value="Hospice"> Hospice          <br>
			  <input type="checkbox" name="company_type[]" class="" value="Home Care"> Home Care        <br>
			  <input type="checkbox" name="company_type[]" class="" value="Referral Agency"> Referral Agency  <br>
			  <input type="checkbox" name="company_type[]" class="" value="Board & Care"> Board & Care 	   <br>
			  <input type="checkbox" name="company_type[]" class="" value="Home Health Care"> Home Health Care <br>
			  <input type="checkbox" name="company_type[]" class="" value="Miscellaneous Support"> Miscellaneous Support <br>
			  <input type="checkbox" class="check_this"> Others (Please Specify)<br>
			  <input type="text" class="form-control other_shows" name="other_type" style="display:none;">
		</div>
	  </div>
	  <div class="row">
		<div class="col-md-6">
		  <div class="form-group">
			<label><span class="required">*</span>Phone number</label>
			<input type="text" name="phone_number" class="form-control validate[required]" placeholder="Enter phone number">
		  </div>
		</div>
		<div class="col-md-6">
		  <div class="form-group">
			<label>Fax number</label>
			<input type="text" name="fax_number" class="form-control" placeholder="Enter fax number">
		  </div>
		</div>
	  </div>
	  <div class="form-group">
		<label>Website</label>
		<input type="text" name="website" class="form-control" placeholder="Enter Url">
	  </div>
	  <div class="form-group">
	    <label><span class="required">*</span>Email Address</label>
		<input type="email" name="email_address" class="form-control validate[required,custom[email]]" placeholder="Enter email address">
	  </div>
	  <br>
	  <label>Contact Person 1</label>
	  <div class="row">
		<div class="col-md-4">
		  <div class="form-group">
			<label><span class="required">*</span>First Name</label>
			<input type="text" name="fname[]" class="form-control validate[required]" placeholder="First name">
		  </div>
		</div>
		<div class="col-md-4">
		  <div class="form-group">
			<label>Middle Name</label>
			<input type="text" name="mname[]" class="form-control" placeholder="Middle name - optional">
		  </div>
		</div>
		<div class="col-md-4">
		  <div class="form-group">
			<label><span class="required">*</span>Last Name</label>
			<input type="text" name="lname[]" class="form-control validate[required]" placeholder="Last name">
		  </div>
		</div>
	  </div>
	<div class="row">
		<div class="col-md-6">
		  <div class="form-group">
			<label>Title</label>
			<input type="text" name="title[]" class="form-control" placeholder="Title">
		  </div>
		</div>
		<div class="col-md-6">
		  <div class="form-group">
			<span class="required">*</span><label>Email</label>
			<input type="text" name="cemail[]" class="form-control validate[required]" placeholder="Email">
		  </div>
		</div>
	</div>
	<label>Contact Person 2</label>
	<div class="row">
		<div class="col-md-4">
		  <div class="form-group">
			<label>First Name</label>
			<input type="text" name="fname[]" class="form-control" placeholder="First name">
		  </div>
		</div>
		<div class="col-md-4">
		  <div class="form-group">
			<label>Middle Name</label>
			<input type="text" name="mname[]" class="form-control" placeholder="Middle name">
		  </div>
		</div>
		<div class="col-md-4">
		  <div class="form-group">
			<label>Last Name</label>
			<input type="text" name="lname[]" class="form-control" placeholder="Last name">
		  </div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
		  <div class="form-group">
			<label>Title</label>
			<input type="text" name="title[]" class="form-control" placeholder="Title">
		  </div>
		</div>
		<div class="col-md-6">
		  <div class="form-group">
			<label>Email</label>
			<input type="text" name="cemail[]" class="form-control" placeholder="Email">
		  </div>
		</div>
	</div><br><br>
	<div class="form-group">
		<label>Alz</label>
		<div>
			<input type="text" name="alz" class="form-control" placeholder="Alz">
		</div>
	</div>
	<div class="form-group">
		<label>Other</label>
		<div>
			<textarea name="other" class="form-control" rows="3" placeholder="Other"></textarea>
		</div>
	</div>
	<div class="form-group">
		<label><span class="required">*</span>Description</label>
		<div>
			<textarea name="description" class="form-control validate[required]" rows="3" placeholder="Description"></textarea>
		</div>
	</div>
	<div class="form-group">
		<label>Professional License Number</label>
		<div>
			<textarea name="res_qual" class="form-control" rows="3" placeholder="Professional License Number"></textarea>
		</div>
	</div>
	<div class="form-group">
		<label>Private</label>
		<div>
			<select name="private1" class="form-control">
				<option value="No">No</option>
				<option value="Yes">Yes</option>
			</select>
		</div>
	</div>	
	<div class="form-group">
		<label>SSI</label>
		<div>
			<select name="ssi" class="form-control validate[required]">
				<option value="No">No</option>
				<option value="Yes">Yes</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label>Medi-Cal</label>
		<div>
			<input type="text" name="medi_cal" class="form-control" placeholder="Medi-Cal">
		</div>
	</div>
	<div class="form-group">
		<label>Medicare</label>
		<div>
			<input type="text" name="medicare" class="form-control" placeholder="Medicare">
		</div>
	</div>
	<div class="form-group">
		<label>Other 2</label>
		<div>
			<textarea name="other2" class="form-control" rows="3" placeholder="Other 2"></textarea>
		</div>
	</div>
	<div class="form-group">
		<label>Services</label>
		<div>
			<textarea name="services" class="form-control" row="3" placeholder="Services"></textarea>
		</div>
	</div>		
	<div class="form-group">
		<label><span class="required">*</span>Select Payment Option:</label><br/>
		<select class="form-control validate[required]" name="payment_status" id="payment_status_value">
			<option value="">Please select payment option</option>
			<option value="Unpaid">Mail the Cheque</option>
			<option value="Paid">Pay online</option>
		</select>
	</div>	
	<div class="form-group" id="paypalImage" style="display:none;">
	<label style="color:#084D52; font-size: 10px;">Please click here to pay through paypal.</label><br/>
		<a href="http://www.paypal.com" class="" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/paypal.png" alt="paypal image"/></a>
	</div>
	<div class="form-group" id="trans_nu" style="display:none;">
		<input type="text" class="form-control validate[required]" name="transaction_number" placeholder="Please enter the paypal transaction number here">
	</div>
	<input type="hidden" name="status" value="For approval">
	<div class="form-group">
		<div class="checkbox" style="margin-bottom: 20px;">
			<label><input type="checkbox" id="checkme" name="checkme"> I have agreed on the <a href="javascript:;" data-toggle="modal" data-target="#myModal2">Terms and Conditions</a>.</label>
		</div>
	</div>
	<div style="text-align: center;">
		<button type="submit" class="btn btn-default" id="registration-submit" name="registrationSubmit" disabled>Register</button>
		<button type="submit" class="btn btn-default" id="continue-submit" name="continueSubmit" style="display:none;" disabled>Continue</button>
	</div>
	</form>
<!-- Modal -->
		<div class="modal fade bs-example-modal-lg" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel" style="color: #084D52;!important">Terms and Conditions</h4>
					</div>
					<div class="modal-body" style="text-align: justify;">
					<!--<p><span style="text-decoration: underline; font-weight: bold;">Renewal Conditions:</span> By joining a plan, you are authorizing Direct Dental Access to bill your credit card or checking account for the plan you have selected. This charge shall remain in force until you notify Direct Dental Access.com of request to cancel. By joining, you indicate you have read the terms and conditions of the plan. This plan will automatically renew at the end of your annual membership term, billing is on a  monthly or Annual basis, and your credit card or bank account will be automatically charged or drafted for the appropriate amount.</p>
					<p><span style="text-decoration: underline; font-weight: bold;">Termination Conditions:</span> Direct Dental Access.com and Careington International Corporation (Careington) reserves the right to terminate plan members from its plan for any reason, including non-payment. </p>
					<p><span style="text-decoration: underline; font-weight: bold;">Cancellation Conditions:</span> You have the right to cancel within the first 30 days after receipt of membership materials and receive a full refund, less the processing fee, if applicable.  FL Residents: You have the right to cancel within 30 days after the effective date. If for any reason during this time period you are dissatisfied with the plan and wish to cancel and obtain a refund, you must submit a written cancellation request. Direct Dental Access.com will accept and cancel plan memberships at any time during the membership period and will cease collecting membership fees in a reasonable amount of time, but no later than 30 days after receiving a cancellation notice. Please send a cancellation letter and a request for refund with your name and member number to Direct Dental Access.com, fax: 801-363-3932. You may also submit cancellation by email: cancellation@directdentalaccess.com. If Direct Dental Access.com is billing you annually, Direct Dental Access.com will, in the event of cancellation of the membership by either party, make a pro-rata reimbursement of the periodic charges to the member.</p>
					<p><span style="text-decoration: underline; font-weight: bold;">Description of Services:</span> Please see the enclosed materials for a specific description of the programs that you have purchased.</p>
					<p><span style="text-decoration: underline; font-weight: bold;">Limitations, Exclusions &amp; Exceptions:</span> This program is a network discount membership program offered by Careington. Careington is not a licensed insurer, health maintenance organization, or other underwriter of health care services. No portion of any providerís fees will be reimbursed or otherwise paid by Careington. Careington is not licensed to provide and does not provide medical services or items to individuals. You will receive discounts for medical services at certain health care providers who have contracted with the plan. You are obligated to pay for all health care services at the time of your appointment. Savings are based upon the providerís normal fees. Actual savings will vary depending upon location and specific services or products purchased. Please verify such services with each individual provider. The discounts contained herein may not be used in conjunction with any other discount plan or program. All listed or quoted prices are current prices by participating providers and subject to change without notice. Any procedures performed by a non-participating provider are not discounted. From time to time, certain providers may offer products or services to the general public at prices lower than the discounted prices available through this program. In such event, members will be charged the lowest price. Discounts on professional services are not available where prohibited by law. This plan does not discount all procedures. Providers are subject to change without notice and services may vary in some states. It is the memberís responsibility to verify that the provider is a participant in the plan. At any time Careington may substitute a provider network at its sole discretion. Careington cannot guarantee the continued participation of any provider. If the provider leaves the plan, you will need to select another provider. Providers contracted by Careington are solely responsible for the professional advice and treatment rendered to members and Careington disclaims any liability with respect to such matters.</p>
					<p><span style="text-decoration: underline; font-weight: bold;">Complaint Procedure:</span> If you would like to file a complaint or grievance regarding your plan membership, you must submit your grievance in writing to: Careington International Corporation, P.O. Box 2568, Frisco, TX 75034. You have the right to request an appeal if you are dissatisfied with the complaint resolution. After completing the complaint resolution process and you remain dissatisfied, you may contact your state insurance department.</p>
					<p>Form: F03F-<?php //echo date('Y'); //$start_year = '2014'; $current_year = date('Y'); $copyright = ($current_year == $start_year) ? $start_year : $start_year.' - '.$current_year; echo $copyright; ?>-TERMCON</p>-->
					<p><span class="comingsoon">Coming Soon...</span></p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
   </div>
</div>


