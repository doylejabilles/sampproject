<?php
	Class EventDAO
	{
		
		/** Creating data in table **/
		private function createForm($row){
			$a = new Event();
			$a->id = ( !empty($row['id']) ) ? $row['id'] : '';
			$a->event_id = ( !empty($row['event_id']) ) ? $row['event_id'] : '';
			$a->company_id = ( !empty($row['company_id']) ) ? $row['company_id'] : '';
			$a->transaction_no = ( !empty($row['transaction_no']) ) ? $row['transaction_no'] : '';
			$a->date_added = ( !empty($row['date_added']) ) ? $row['date_added'] : '';
			return $a;
		}
		
		//adding to Event table	
		public function addEvent($event, $table){
		
			$event_id = DB::escape($event->event_id);
			$company_id = DB::escape($event->company_id);
			$transaction_no = DB::escape($event->transaction_no);
			
			$sql = " INSERT INTO $table(event_id, company_id, transaction_no) " . 
				   " VALUES('$event_id', '$company_id', '$transaction_no') ";

				//echo $sql; exit;
			
			$result = DB::query($sql);
			return DB::count($result) > 0;
		}
		
		public function get($table, $where='', $orderby='', $order='DESC', $groupby='', $startpoint=0, $limit=0){
			$sql = "SELECT * FROM $table ";
			if(!empty($where))
				$sql = $sql . "WHERE $where ";
			
			if(!empty($orderby))
				$sql = $sql . "ORDER BY $orderby $order ";
			
			if(!empty($groupby))
				$sql = $sql . "GROUP BY $groupby ";
				
			if($startpoint >= 0 && $limit > 0)
				$sql = $sql . "LIMIT $startpoint, $limit";
			
			// echo $sql; exit();
			
			$result = DB::query($sql);
			$list = Array();
			if(DB::count($result)>0){
				while($row = DB::fetchArray($result)){
					$list[] = $this->createForm($row);
				}
			}
			return $list;
		}
		
	}
?>