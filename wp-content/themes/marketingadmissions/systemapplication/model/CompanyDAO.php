<?php
	class CompanyDAO
	{
		//creating the data in the table
		private function createForm($row) {
			$a = new Company();
			$a->id = ( !empty($row['id']) ) ? $row['id'] : '';
			$a->company_id = ( !empty($row['company_id']) ) ? $row['company_id'] : '';
			$a->company_name = ( !empty($row['company_name']) ) ? $row['company_name'] : '';
			$a->company_address = ( !empty($row['company_address']) ) ? $row['company_address'] : '';
			$a->account_type = ( !empty($row['account_type']) ) ? $row['account_type'] : '';
			$a->company_contactno = ( !empty($row['company_contactno']) ) ? $row['company_contactno'] : '';
			$a->pic = ( !empty($row['pic']) ) ? $row['pic'] : '';
			$a->companyid = ( !empty($row['companyid']) ) ? $row['companyid'] : '';
			$a->name = ( !empty($row['name']) ) ? $row['name'] : '';
			$a->username = ( !empty($row['username']) ) ? $row['username'] : '';
			$a->password = ( !empty($row['password']) ) ? $row['password'] : '';
			$a->repeat_password = ( !empty($row['repeat_password']) ) ? $row['repeat_password'] : '';
			$a->city = ( !empty($row['city']) ) ? $row['city'] : '';
			$a->state = ( !empty($row['state']) ) ? $row['state'] : '';
			$a->zip = ( !empty($row['zip']) ) ? $row['zip'] : '';
			$a->category = ( !empty($row['category']) ) ? $row['category'] : '';
			$a->capacity = ( !empty($row['capacity']) ) ? $row['capacity'] : '';
			$a->company_type = ( !empty($row['company_type']) ) ? $row['company_type'] : '';
			$a->other_type = ( !empty($row['other_type']) ) ? $row['other_type'] : '';
			$a->phone_number = ( !empty($row['phone_number']) ) ? $row['phone_number'] : '';
			$a->fax_number = ( !empty($row['fax_number']) ) ? $row['fax_number'] : '';
			$a->website = ( !empty($row['website']) ) ? $row['website'] : '';
			$a->email_address = ( !empty($row['email_address']) ) ? $row['email_address'] : '';
			$a->alz = ( !empty($row['alz']) ) ? $row['alz'] : '';
			$a->other = ( !empty($row['other']) ) ? $row['other'] : '';
			$a->description = ( !empty($row['description']) ) ? $row['description'] : '';
			$a->private1 = ( !empty($row['private1']) ) ? $row['private1'] : '';
			$a->ssi = ( !empty($row['ssi']) ) ? $row['ssi'] : '';
			$a->medi_cal = ( !empty($row['medi_cal']) ) ? $row['medi_cal'] : '';
			$a->medicare = ( !empty($row['medicare']) ) ? $row['medicare'] : '';
			$a->other2 = ( !empty($row['other2']) ) ? $row['other2'] : '';
			$a->autobiography = ( !empty($row['autobiography']) ) ? $row['autobiography'] : '';
			$a->ads = ( !empty($row['ads']) ) ? $row['ads'] : '';
			$a->event_name = ( !empty($row['event_name']) ) ? $row['event_name'] : '';
			$a->price = ( !empty($row['price']) ) ? $row['price'] : '';
			$a->event_date = ( !empty($row['event_date']) ) ? $row['event_date'] : '';
			$a->event_time = ( !empty($row['event_time']) ) ? $row['event_time'] : '';
			$a->event_location = ( !empty($row['event_location']) ) ? $row['event_location'] : '';
			$a->event_contact = ( !empty($row['event_contact']) ) ? $row['event_contact'] : '';
			$a->event_contact = ( !empty($row['event_contact']) ) ? $row['event_contact'] : '';
			$a->sponsor = ( !empty($row['sponsor']) ) ? $row['sponsor'] : '';
			$a->tc_id = ( !empty($row['tc_id']) ) ? $row['tc_id'] : '';
			$a->contact_person = ( !empty($row['contact_person']) ) ? $row['contact_person'] : '';
			$a->facility = ( !empty($row['facility']) ) ? $row['facility'] : '';
			$a->name = ( !empty($row['name']) ) ? $row['name'] : '';
			$a->services = ( !empty($row['services']) ) ? $row['services'] : '';
			$a->status = ( !empty($row['status']) ) ? $row['status'] : '';
			$a->payment_status = ( !empty($row['payment_status']) ) ? $row['payment_status'] : '';
			return $a;
		}
		//add company
		public function addCompany($company, $table)
		{
			$id = ( !empty($company->company_id) ) ? intval($company->company_id) : 0;
			
			$account_type = DB::escape($company->account_type);
			$company_name = DB::escape($company->company_name);
			$company_address = DB::escape($company->company_address);
			$company_contactno = DB::escape($company->company_contactno);
			$username = DB::escape($company->username);
			$password = DB::escape($company->password);
			$repeat_password = DB::escape($company->repeat_password);
			$city = DB::escape($company->city);
			$state = DB::escape($company->state);
			$zip = DB::escape($company->zip);
			$category = DB::escape($company->category);
			$capacity = DB::escape($company->capacity);
			$company_type = DB::escape($company->company_type);
			$other_type = DB::escape($company->other_type);
			$phone_number = DB::escape($company->phone_number);
			$fax_number = DB::escape($company->fax_number);
			$website = DB::escape($company->website);
			$email_address = DB::escape($company->email_address);
			$alz = DB::escape($company->alz);
			$other = DB::escape($company->other);
			$description = DB::escape($company->description);
			$private1 = DB::escape($company->private1);
			$ssi = DB::escape($comapny->ssi);
			$medi_cal = DB::escape($company->medi_cal);
			$medicare = DB::escape($company->medicare);
			$other2 = DB::escape($company->other2);
			$services = DB::escape($company->services);
			$status = DB::escape($company->status);
			$payment_status = DB::escape($company->payment_status);
			
			if(!empty($id) && $id > 0 ) {
				$sql  = "INSERT INTO $table(company_id, account_type, company_name, company_address, company_contactno, username, password, repeat_password, city, state, zip, category, capacity, company_type, other_type, phone_number, fax_number, website, email_address, alz, other, description, private1, ssi, medi_cal, medicare, other2,  services, status, payment_status) " .
					"VALUES($id,'$account_type','$company_name','$company_address','$company_contactno','$username','$password','$repeat_password','$city','$state','$zip','$category','$capacity','$company_type','$other_type','$phone_number','$fax_number','$website','$email_address','$alz','$other','$description','$private1','$ssi','$medi_cal','$medicare','$other2','$services','$status','$payment_status')";
			}
			else {
				$sql  = "INSERT INTO $table(account_type, company_name, company_address, company_contactno, username, password, repeat_password, city, state, zip, category, capacity, company_type, phone_number, fax_number, website, email_address, alz, other, description, private1, ssi, medi_cal, medicare, other2, services, status, payment_status) " .
					"VALUES('$account_type','$company_name','$company_address','$company_contactno','$username','$password','$repeat_password','$city','$state','$zip','$category','$capacity','$company_type','$phone_number','$fax_number','$website','$email_address','$alz','$other','$description','$private1','$ssi','$medi_cal','$medicare','$other2','$services','$status','$payment_status')";
			}
			// echo $sql; exit;
			
			 // echo exit("You have been registered!");
			$result = DB::query($sql);
			return DB::count($result) > 0;
		}
		
		//adding mailing-list
		public function addMail($mailing_list, $table)
		{
			
			$company_name = DB::escape($mailing_list->company_name);
			$company_contactno = DB::escape($mailing_list->company_contactno);
			$email_address = DB::escape($mailing_list->email_address);
			$contact_person = DB::escape($mailing_list->contact_person);
			
	
				$sql  = "INSERT INTO $table(company_name, company_contactno, email_address, contact_person) " .
					"VALUES('$company_name','$company_contactno','$email_address','$contact_person')";
			
			// echo $sql; exit;
			
			 // echo exit("You have been registered!");
			$result = DB::query($sql);
			return DB::count($result) > 0;
		}
		
			//adding event function
		public function addEvent($company, $table)
		{
			$event_name = DB::escape($company->event_name);
			$sponsor = DB::escape($company->sponsor);
			$event_date = DB::escape($company->event_date);
			$event_time = DB::escape($company->event_time);
			$event_location = DB::escape($company->event_location);
			$price = DB::escape($company->price);
			$event_contact = DB::escape($company->event_contact);
			
			
			$sql  = "INSERT INTO $table(event_name, sponsor, event_date, event_time, event_location, price, event_contact) " .
					"VALUES('$event_name','$sponsor','$event_date','$event_time','$event_location','$price','$event_contact')";
			
			
			// echo $sql; exit;
			
			 // echo exit("You have been registered!");
			$result = DB::query($sql);
			return DB::count($result) > 0;
		}
		
		//add facility
		public function addfacility($table, $fields)
		{
			$facility = DB::escape($fields->facility);
			$companyid = intval($fields->companyid);
			
			$sql  = "INSERT INTO {$table} (facility, companyid) ".
					"VALUES('$facility', $companyid)";
				
				// echo $sql; exit();
				
			$result = DB::query($sql);
			return DB::count($result) > 0;
		}
		
		//add company
		public function addads($table, $fields)
		{
			$ads = DB::escape($fields->ads);
			$companyid = intval($fields->companyid);
			
			$sql  = "INSERT INTO {$table} (ads, companyid) ".
					"VALUES('$ads','$companyid')";
				
				// echo $sql; exit();
				
			$result = DB::query($sql);
			return DB::count($result) > 0;
		}
		
		//add company
		public function addpicture($table, $fields)
		{
			$pic = DB::escape($fields->pic);
			$companyid = intval($fields->companyid);
			
			$sql  = "INSERT INTO {$table} (pic, companyid) ".
					"VALUES('$pic','$companyid')";
			
			$result = DB::query($sql);
			return DB::count($result) > 0;
		}
		
		//add company
		public function addautobiography($table, $fields)
		{
			$autobiography = DB::escape($fields->autobiography);
			$companyid = intval($fields->companyid);
			
			$sql  = "INSERT INTO {$table} (autobiography, companyid) ".
					"VALUES('$autobiography','$companyid')";
				// echo $sql; exit;
			$result = DB::query($sql);
			return DB::count($result) > 0;
		}

		public function save($table, $key, $vals)
		{
			$result = 0;
			$val = DB::escape($vals->companyid);
			if($this->checkExisting($table, $val, $key)) {
				//$table = 'pictures';	
				$result = $this->update($table, $vals);
			} else {
				if($table == 'pictures')
					$result = $this->addpicture($table, $vals);
				elseif($table == 'ads')
					$result = $this->addads($table, $vals);
				elseif($table == 'autobiography')
					$result = $this->addautobiography($table, $vals);
			}
			return $result;
		}
		
		//function to edit a record
		public function update($table, $vals, $action='')
		{			
			$id = intval($vals->companyid);
			if($table=="pictures") {
				$field = "pic";
				$pic = DB::escape($vals->pic);
			} elseif($table=="ads") {
				$field = "ads";
				$pic = DB::escape($vals->ads);
			} elseif($table=="autobiography") {
				$field = "autobiography";
				$pic = DB::escape($vals->autobiography);
			} 
			$sql = "UPDATE {$table} SET {$field}='{$pic}' ".
					"WHERE companyid={$id}";
			// echo $sql.' '.$pic; exit;
			$result = DB::query($sql);			
			return DB::count($result) > 0;
		}
		
		public function get($table, $where='', $orderby='', $order='DESC', $groupby='', $startpoint=0, $limit=0, $fields='*'){
			$sql = "SELECT {$fields} FROM {$table} ";
			if(!empty($where))
				$sql = $sql . "WHERE $where ";
			
			if(!empty($orderby))
				$sql = $sql . "ORDER BY $orderby $order ";
			
			if(!empty($groupby))
				$sql = $sql . "GROUP BY $groupby ";
				
			if(!empty($startpoint) && !empty($limit))
				$sql = $sql . "LIMIT $startpoint, $limit";
			
			// echo $sql; exit();
			
			$result = DB::query($sql);
			$list = Array();
			if(DB::count($result)>0){
				while($row = DB::fetchArray($result)){
					$list[] = $this->createForm($row);
				}
			}
			return $list;
		}
		
		// Function for joing temp table to original table
		public function getJoin($fields='*', $table, $where='', $orderby='', $order='DESC', $groupby='', $startpoint=0, $limit=0){
			$sql = "SELECT $fields FROM $table c LEFT JOIN temp_companies tc ON c.company_id=tc.company_id ";
			if(!empty($where))
				$sql = $sql . "WHERE $where ";
			
			if(!empty($orderby))
				$sql = $sql . "ORDER BY $orderby $order ";
			
			if(!empty($groupby))
				$sql = $sql . "GROUP BY $groupby ";
				
			if(!empty($startpoint) && !empty($limit))
				$sql = $sql . "LIMIT $startpoint, $limit";
			
			// echo $sql; exit();
			
			$result = DB::query($sql);
			$list = Array();
			if(DB::count($result)>0){
				while($row = DB::fetchArray($result)){
					$list[] = $this->createForm($row);
				}
			}
			return $list;
		}
		
		//pinaka last nga g.add nga funtion, delete sa sql
		public function delete($table, $where){
			$sql = "DELETE FROM $table {$where}";
			// echo exit($sql);
			$result = DB::query($sql);
			return DB::count($result) > 0;
		}

		public function getRow($table, $where){
			$sql = "SELECT * FROM $table ".
				   "WHERE $where";
			  // echo $sql;exit();
			$result = DB::query($sql);
			if(DB::count($result)>0)
			{
				$row = DB::fetchArray($result);
				return $this->createForm($row);
			}
			return false;
		}
			
			
		//function to check if the email address exists
		public function checkExistingToOtherUsers($table, $val, $field, $id)
		{
			$sql = "SELECT * FROM $table WHERE $field='$val' AND company_id!=$id";		
			// echo exit($sql);
			$result = DB::query($sql);
			return DB::count($result) > 0;
		}
		
		//function to check if the email address exists
		public function checkExisting($table,$val,$field)
		{
			$sql = "SELECT * FROM $table WHERE $field='$val'";
			// echo $sql; exit;
			$result = DB::query($sql);
			return DB::count($result) > 0;
		}

		
		//function to update sql database
		public function updateProfile($company, $table)
		{
			$company_id = intval($company->company_id);
			$account_type = DB::escape($company->account_type);
			$company_name = DB::escape($company->company_name);
			$company_address = DB::escape($company->company_address);
			$company_contactno = DB::escape($company->company_contactno);
			$username = DB::escape($company->username);
			$password = DB::escape($company->password);
			$repeat_password = DB::escape($company->repeat_password);
			$city = DB::escape($company->city);
			$state = DB::escape($company->state);
			$zip = DB::escape($company->zip);
			$category = DB::escape($company->category);
			$capacity = DB::escape($company->capacity);
			$company_type = DB::escape($company->company_type);
			$other_type = DB::escape($company->other_type);
			$phone_number = DB::escape($company->phone_number);
			$fax_number = DB::escape($company->fax_number);
			$website = DB::escape($company->website);
			$email_address = DB::escape($company->email_address);
			$alz = DB::escape($company->alz);
			$other = DB::escape($company->other);
			$description = DB::escape($company->description);
			$private1 = DB::escape($company->private1);
			$ssi = DB::escape($comapny->ssi);
			$medi_cal = DB::escape($company->medi_cal);
			$medicare = DB::escape($company->medicare);
			$other2 = DB::escape($company->other2);
			// echo '<pre>'; print_r($company); echo '</pre>';
			// exit();
			
			$sql  = "UPDATE $table 
						SET account_type = '$account_type',
							company_name = '$company_name',
							company_address = '$company_address',
							company_contactno = '$company_contactno',
							username = '$username',
							password = '$password',
							repeat_password = '$repeat_password',
							city = '$city',
							state = '$state',
							zip = '$zip',
							category = '$category',
							capacity = '$capacity',
							company_type = '$company_type',
							other_type = '$other_type',
							phone_number = '$phone_number',
							fax_number = '$fax_number',
							website = '$website',
							email_address = '$email_address',
							alz = '$alz',
							other = '$other',
							description = '$description',
							private1 = '$private1',
							ssi = '$ssi',
							medi_cal = '$medi_cal',
							medicare = '$medicare',
							other2 = '$other2'
					  WHERE company_id = $company_id";
			// echo $sql; exit();
			$result = DB::query($sql);
			return DB::count($result) > 0;
		}
		
		public function updateStatus($company, $table)
		{
			$company_id = intval($company->company_id);
			$status = DB::escape($company->status);
			
			// echo '<pre>'; print_r($company); echo '</pre>';
			// exit();
			
			$sql  = "UPDATE $table 
						SET status = '$status'
					  WHERE company_id = $company_id";
			// echo $sql; exit();
			$result = DB::query($sql);
			return DB::count($result) > 0;
		}
		
		public function updateServices($company, $table)
		{
			$company_id = intval($company->company_id);
			$services = DB::escape($company->services);
			
			// echo '<pre>'; print_r($company); echo '</pre>';
			// exit();
			
			$sql  = "UPDATE $table 
						SET services = '$services'
					  WHERE company_id = $company_id";
			// echo $sql; exit();
			$result = DB::query($sql);
			return DB::count($result) > 0;
		} 
		
		//Updating temporary files 
		public function updateTemp($company, $table, $key='')
		{
			$id = intval($company->company_id);
			
			if($this->checkExisting($table, $id, $key)) {
				$result = $this->updateProfile($company, $table);
			} else {
				$result = $this->addCompany($company, $table);
			}

			return $result;
		}
		
		
		public function getEvent($table, $where='', $orderby='', $order='DESC', $groupby='', $startpoint=0, $limit=0){
			$sql = "SELECT * FROM $table ";
			if(!empty($where))
				$sql = $sql . "WHERE $where ";
			
			if(!empty($orderby))
				$sql = $sql . "ORDER BY $orderby $order ";
			
			if(!empty($groupby))
				$sql = $sql . "GROUP BY $groupby ";
				
			if($startpoint >= 0 && $limit > 0)
				$sql = $sql . "LIMIT $startpoint, $limit";
			
			// echo $sql; exit();
			
			$result = DB::query($sql);
			$list = Array();
			if(DB::count($result)>0){
				while($row = DB::fetchArray($result)){
					$list[] = $this->createForm($row);
				}
			}
			return $list;
		}
		
		public function updateLogin($admin, $table) {
			$id = intval($admin->id);
			$name = DB::escape($admin->name);
			$username = DB::escape($admin->username);
			$password = DB::escape($admin->password);
			$repeat_password = DB::escape($admin->repeat_password);
			$email_address = DB::escape($admin->email_address);
			
			$sql = "UPDATE	$table
					   SET	name = '$name',
							username = '$username',
							password = '$password',
							repeat_password = '$repeat_password',
							email_address = '$email_address'
					WHERE	id = $id";
				
			// echo $sql; exit();
			$result = DB::query($sql);
			return DB::count($result) > 0;
		}

	}
?>	