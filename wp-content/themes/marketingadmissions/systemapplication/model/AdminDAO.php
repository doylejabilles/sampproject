<?php
	class AdminDAO
	{

		private function createForm($row) {
			$a = new Admin();
			$a->id = ( !empty($row['id']) ) ? $row['id'] : '';
			$a->name = ( !empty($row['name']) ) ? $row['name'] : '';
			$a->username = ( !empty($row['username']) ) ? $row['username'] : '';
			$a->password = ( !empty($row['password']) ) ? $row['password'] : '';
			$a->repeat_password = ( !empty($row['repeat_password']) ) ? $row['repeat_password'] : '';
			$a->email_address = ( !empty($row['email_address']) ) ? $row['email_address'] : '';
		}
		
		//update login
		public function updateLogin($admin, $table) {
			$id = intval($admin->id);
			$name = DB::escape($admin->name);
			$username = DB::escape($admin->username);
			$password = DB::escape($admin->password);
			$repeat_password = DB::escape($admin->repeat_password);
			$email_address = DB::escape($admin->email_address);
			
			$sql = "UPDATE	$table
					   SET	name = '$name',
							username = '$username',
							password = '$password',
							repeat_password = '$repeat_password',
							email_address = '$email_address'
					WHERE	id = $id"; 
				
			// echo $sql; exit();
			$result = DB::query($sql);
			return DB::count($result) > 0;
		}
		
		public function getRow($table, $where){
			$sql = "SELECT * FROM $table ".
				   "WHERE $where";
			  // echo $sql;exit();
			$result = DB::query($sql);
			if(DB::count($result)>0)
			{
				$row = DB::fetchArray($result);
				return $this->createForm($row);
			}
			return false;
		}
		
		public function get($table, $where='', $orderby='', $order='DESC', $groupby='', $startpoint=0, $limit=0){
			$sql = "SELECT * FROM $table ";
			if(!empty($where))
				$sql = $sql . "WHERE $where ";
			
			if(!empty($orderby))
				$sql = $sql . "ORDER BY $orderby $order ";
			
			if(!empty($groupby))
				$sql = $sql . "GROUP BY $groupby ";
				
			if(!empty($startpoint) && !empty($limit))
				$sql = $sql . "LIMIT $startpoint, $limit";
			
			// echo $sql; exit();
			
			$result = DB::query($sql);
			$list = Array();
			if(DB::count($result)>0){
				while($row = DB::fetchArray($result)){
					$list[] = $this->createForm($row);
				}
			}
			return $list;
		}
	
	}
?>
