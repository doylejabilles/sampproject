<?php
	class MemberDAO 
	{
		//creating the data in the table
		private function createForm($row) {
			$a = new Member();
			$a->member_id =  ( !empty($row['member_id']) ) ? $row['member_id'] : '';
			$a->company_id = ( !empty($row['company_id']) ) ? $row['company_id'] : '';
			$a->fname = ( !empty($row['fname']) ) ? $row['fname'] : '';
			$a->mname = ( !empty($row['mname']) ) ? $row['mname'] : '';
			$a->lname = ( !empty($row['lname']) ) ? $row['lname'] : '';
			$a->title = ( !empty($row['title']) ) ? $row['title'] : '';
			$a->cemail = ( !empty($row['cemail']) ) ? $row['cemail'] : '';
			$a->contact_no = ( !empty($row['contact_no']) ) ? $row['contact_no'] : '';
			return $a;
		}	
	
		public function get($table, $where='', $orderby='', $order='DESC', $groupby='', $startpoint=0, $limit=0){
			$sql = "SELECT * FROM $table ";
			if(!empty($where))
				$sql = $sql . "WHERE $where ";
			
			if(!empty($orderby))
				$sql = $sql . "ORDER BY $orderby $order ";
			
			if(!empty($groupby))
				$sql = $sql . "GROUP BY $groupby ";
				
			if(!empty($startpoint) && !empty($limit))
				$sql = $sql . "LIMIT $startpoint, $limit";
			// echo $sql; exit();
			$result = DB::query($sql);
			$list = Array();
			if(DB::count($result)>0){
				while($row = DB::fetchArray($result)){
					$list[] = $this->createForm($row);
				}
			}
			return $list;
		}		
		
		
		public function getRow($table, $where){
			$sql = "SELECT * FROM $table ".
				   "WHERE $where";
			// echo $sql; exit();
			$result = DB::query($sql);
			if(DB::count($result)>0)
			{
				$row = DB::fetchArray($result);
				return $this->createForm($row);
			}
			return $list;
		}

	public function addMember($member, $table)
		{
			$company_id = DB::escape($member->company_id);
			$fname = DB::escape($member->fname);
			$mname = DB::escape($member->mname);
			$lname = DB::escape($member->lname);
			$title = DB::escape($member->title);
			$cemail = DB::escape($member->cemail);
			
			
			$sql  = "INSERT INTO $table(company_id, fname, mname, lname, title, cemail) " .
					"VALUES('$company_id','$fname','$mname','$lname', '$title', '$cemail')";
			
			// echo $sql; exit;
			
			$result = DB::query($sql);
			return DB::count($result) > 0;
		}
		
	public function updateMember($member, $table)
		{
			$member_id = DB::escape($member->member_id);
			$fname = DB::escape($member->fname);
			$mname = DB::escape($member->mname);
			$lname = DB::escape($member->lname);
			$title = DB::escape($member->title);
			$cemail = DB::escape($member->cemail);
			
			$sql = "UPDATE $table
						SET fname = '$fname',
							mname = '$mname',
							lname = '$lname',
							title = '$title',
							cemail = '$cemail'
						WHERE member_id = $member_id";
			
			//echo $sql; exit();
			$result = DB::query($sql);
			return DB::count($result) > 0;		
		}
		
		//pinaka last nga g.add nga funtion, delete sa sql
		public function delete($table, $where){
			$sql = "DELETE FROM $table {$where}";
			$result = DB::query($sql);
			return DB::count($result) > 0;
		}
		
	}
?>	