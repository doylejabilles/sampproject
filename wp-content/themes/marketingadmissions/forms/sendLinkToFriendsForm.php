<?php
@session_start();
require_once 'FormsClass.php';
$input = new FormsClass();

include 'form_details.php';
if($smtp)
include 'phpmailer/sendmail.php';

$link = 'http://link2newsite.com/yourity/marketingadmissions'; 

$formname = 'Send Link to Friends Form';
$prompt_message = '<span style="color:#ff0000;">* = Required Information</span>';
$message = 'Take a look at this website:';
if ($_POST){

	if(empty($_POST['To_Email']) ||
		empty($_POST['To_Name']) ||
		empty($_POST['From_Name']) ||				
		empty($_POST['From_Email']) ||	
		empty($_POST['secode'])) {	
	
	$asterisk = '<span style="color:#FF0000; font-weight:bold;">*&nbsp;</span>';
	$asteriskEmail = '<span style="color:#FF0000;">Please enter a valid email address</span>';
	$prompt_message = $asterisk . '<span style="color:#FF0000;"> Required Fields are empty</span>';
	}
	else if((!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i",stripslashes(trim($_POST['To_Email'])))) || (!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i",stripslashes(trim($_POST['From_Email'])))))
		{ $prompt_message = '<span style="color:#FF0000;">Please enter a valid email address</span>';}
	else if($_SESSION['security_code'] != htmlspecialchars(trim($_POST['secode']), ENT_QUOTES)){
		$prompt_message = '<span style="color:#CC0000;">Invalid Security Code</span>';
	}else{
	
		
		$body = '<div align="left" style="width:700px; height:auto; font-size:12px; color:#333333; letter-spacing:1px; line-height:20px;">
			<div style="border:8px double #c3c3d0; padding:12px;">
			<div align="center" style="font-size:22px; font-family:Times New Roman, Times, serif; color:#051d38;">'.$dcomp.'</div>
			<div align="center" style="color:#990000; font-style:italic; font-size:13px; font-family:Arial;">('.$formname.')</div>
			<p>&nbsp;</p>
			<table width="90%" cellspacing="2" cellpadding="5" align="center" style="font-family:Verdana; font-size:13px">
				';
			$body .= '<tr><td colspan="2" style="background:#F0F0F0; line-height:30px"><b>'.$message.' <a href="'.$link.'">'.$link.'</a></b></td></tr>';
			$body .= '			
			</table>

			</div>
			</div>';	
	
		$subject = $dcomp . " [" . $formname . "]";

		/************** for smtp ***********/
		$to_email = $_POST['To_Email'];	
		$to_name = $_POST['To_Name'];
		$from_email = $_POST['From_Email'];
		$from_name = $_POST['From_Name'];
		$cc2 = '';
		if($smtp) { 
			$mail = new SendMail($host, $username, $password);
			$trysend = $mail->sendNow($to_email, $to_name, $cc2, $bcc, $from_email, $from_name, $subject, $body);
			if ($trysend == 'ok')
				$sent = true;
			else
				$sent = false;
		}else {
		/************** for mail function ***********/
			$headers= 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= "From: ".$from_name." <".$from_email.">\n";
			$headers .= "Cc: ".$cc2."\n";
			if(!empty($bcc)){
    $headers .= "Bcc: ".$bcc;
}
			if(mail($to_email, $subject, $body, $headers)) {
				$sent = true;				
			}else {
				$sent = false;
			}
		}
		
		if($sent) {
				$success_msg = 'Your message has been submitted. <br />Thank you for your time.';
				$prompt_message ="<div style=\"width:auto; height:auto; padding-top:15x; padding-right:20px; padding-left:20px; margin:0 auto 0 auto; font-family:Times New Roman, Times, serif; font-weight:bold; border:6px #BABABA ridge; background:#F2F5F7\">
								<p align=\"center\" style=\"color:green; font-size:16px; font-style:italic;\">{$success_msg}</p></div>";
				unset($_POST);
		}else {
				$success_msg = 'Failed to send email. Please try again.';
				$prompt_message ="<div style=\"width:auto; height:auto; padding-top:15x; padding-right:20px; padding-left:20px; margin:0 auto 0 auto; font-family:Times New Roman, Times, serif; font-weight:bold; border:6px #BABABA ridge; background:#F2F5F7\">
								<p align=\"center\" style=\"color:#FF0000; font-size:16px; font-style:italic;\">{$success_msg}</p></div>";
		}
	}
		
		
}
/*************declaration starts here************/

?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title><?php echo $formname; ?></title>
<link rel="stylesheet" href="css/style.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {	
	// validate signup form on keyup and submit
	$("#submitform").validate({
		rules: {
			To_Name: "required",
			To_Email: {
				required: true,
				email: true
			},
			From_Name: "required",
			From_Email: {
				required: true,
				email: true
			},
			secode: "required"		
		},
		messages: {
			To_Name: "Required",
			To_Email: "Enter a valid Email",
			From_Name: "Required",
			From_Email: "Enter a valid Email",
			secode: ""
		}
	});
	$("#submitform").submit(function(){
		if($(this).valid()){
			self.parent.$('html, body').animate(
				{ scrollTop: self.parent.$('#myframe').offset().top },
				500
			);
		}
	});	
});
</script>
</head>
<body>
	<div id="container" class="rounded-corners">
		<div id="content" class="rounded-corners">
			<form id="submitform" name="contact" method="post" action="">				
				<?php echo $prompt_message; ?>
				<hr />
				<div class="field">
					<div class="input">
						<label for="To_Name">To (Name) <span>*</span></label>
						<?php 
							// @param field name, class, id and attribute
							$input->fields('To_Name', 'text','To_Name','placeholder="Enter recipient\'s name here"'); 
						?>						
					</div>	
					<div class="input f-right">
						<label for="To_Email">To (Email) <span>*</span></label>
						<?php 
							// @param field name, class, id and attribute
							$input->fields('To_Email', 'text','To_Email','placeholder="Enter recipient\'s email here"'); 
						?>						
					</div>						
				</div>
				<div class="field">
					<div class="input">
						<label for="From_Name">From (Name) <span>*</span></label>
						<?php 
							// @param field name, class, id and attribute
							$input->fields('From_Name', 'text','From_Name','placeholder="Enter sender\'s name here"'); 
						?>						
					</div>	
					<div class="input f-right">
						<label for="From_Email">From (Email) <span>*</span></label>
						<?php 
							// @param field name, class, id and attribute
							$input->fields('From_Email', 'text','From_Email','placeholder="Enter sender\'s email here"'); 
						?>						
					</div>						
				</div>	
				<div class="field">
					<div class="input">
						<label for="Subject">Subject</label>
						<?php 
							// @param field name, class, id and attribute
							$input->fields('Subject', 'text','Subject','placeholder="Enter subject here"'); 
						?>						
					</div>					
				</div>
				<div class="field">	
					<div class="input textarea">	
						<label for="Message">Message</label>	
						<?php 
							// @param field name, class, id and attribute
							$input->textarea('Message','','Message','readonly="readonly" cols="88"','Take a look at this website: '.$link); 
						?>
					</div>		
				</div>
				<div class="field">	
					<div class="verification">
						<img src="../forms/securitycode/SecurityImages.php?characters=5" border="0" id ="securiryimage" alt="Security code" />
						<?php 
							// @param field name, class, id and attribute
							$input->fields('secode', 'text','secode','placeholder="Enter security code here" title="This confirms you are a human user and not a spam-bot." maxlength="5"'); 
						?>	
						<button type='submit' class="button">Submit</button>						
					</div>	
				</div>
			</form>	
			<div class="clearfix"></div>			
		</div>
	</div>
</body>	
</html>