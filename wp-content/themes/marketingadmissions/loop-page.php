<?php
 if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
			<!-- logout -->
		<?php
			// $active = (isset($_SESSION['ACTIVE'])) ? $_SESSION['ACTIVE'] : '';
			
			// if( (isset($active) && $active == 1) ) {
				// echo '<div class="container row clearfix" style=" width: 100%; display: block; margin-left: 500px;">
					// <div class="col-md-12">Welcome! <a href="wp-content/themes/marketingadmissions/systemapplication/logout.php?logout=true">Logout</a></div>
				// </div>';
			// }
			
			$active = ( isset($_SESSION['ACTIVE']) && ($_SESSION['ACTIVE'] == 1) ) ? true : false;
			$type = ( isset($_SESSION['TYPE']) && ($_SESSION['TYPE'] == "member") ) ? 'member' : 'admin';
			if ( $active == true && $type == 'member' ) {
				echo '<div class="container row clearfix" style=" width: 100%; display: block; margin-left: 450px;">
					 <div class="col-md-12">Welcome! <a href="charity-organization-my-profile" style="color: #084D52;"> Profile </a>&nbsp;<a href="wp-content/themes/marketingadmissions/systemapplication/logout.php?logout=true" style="color: #084D52;"> Logout </a></div>
					</div>';
			} if ( $active == true && $type == 'admin' ) {
				echo '<div class="container row clearfix" style=" width: 100%; display: block; margin-left: 450px;">
					 <div class="col-md-12">Welcome! <a href="charity-organization-admin" style="color: #084D52;"> Profile </a>&nbsp;<a href="wp-content/themes/marketingadmissions/systemapplication/logout.php?logout=true" style="color: #084D52;"> Logout </a></div>
					</div>';
			} if (is_page('charity-organization-my-profile') && !$active) {
				echo '<script>alert("You are not logged in, unable to access this page, please login first!");window.location = "charity-organization-login";</script>';
			} elseif(is_page('charity-organization-admin') && !$active) {
				echo '<script>alert("Please login before you can access this page!"); window.location = "charity-organization-login"; </script>';
			} else {
			
			}
			
			
		?>
		
			<?php if ( is_front_page() ) { ?>
				<h1>Welcome to <span class="comp block">Marketing &amp; Admissions Professionals for Seniors</span></h1>
			<?php } else { ?>
				<h1 class="entry-title mark"><span><?php the_title(); ?></span></h1>
			<?php } ?>
			
			<div class="content">
			<?php the_content();?>
			<?php if(is_page('12')): ?>
			<p>
				<iframe id="myframe" src="<?php bloginfo('template_url'); ?>/forms/contactForm.php">Your browser does not support inline frames or is currently configured not to display inline frames. Content can be viewed at actual source page:<?php bloginfo('template_url'); ?>forms/contactForm.php</iframe>
				<script type="text/javascript">
					document.getElementById('myframe').onload = function(){
						calcHeight(0);
					};
				</script>
			</p>
			<?php elseif(is_page('34')): ?>
			<p>
				<iframe id="myframe" src="<?php bloginfo('template_url'); ?>/forms/sendLinkToFriendsForm.php">Your browser does not support inline frames or is currently configured not to display inline frames. Content can be viewed at actual source page:<?php bloginfo('template_url'); ?>forms/sendLinkToFriendsForm.php</iframe>				
				<script type="text/javascript">
					document.getElementById('myframe').onload = function(){
						calcHeight(0);
					};
				</script>
			</p>
						
			<?php elseif(is_page('35')): ?>  
			<p>
				<iframe src="https://www.google.com/calendar/embed?src=kuu0qe6s278vmr66j0pfr880lk%40group.calendar.google.com&ctz=America/Los_Angeles" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
			</p>
	
			<?//php elseif(is_page('8')): ?>
			<!--<p>
				<iframe id="myframe" src="<?php bloginfo('template_url'); ?>/forms/becomeAmember.php">Your browser does not support inline frames or is currently configured not to display inline frames. Content can be viewed at actual source page:<?php bloginfo('template_url'); ?>forms/becomeAmember.php</iframe>				
				<script type="text/javascript">
					document.getElementById('myframe').onload = function(){
						calcHeight(0);
					};
				</script>
			</p>-->
			
			<?php 
				elseif(is_page('charity-organization-members-directory')): 
						get_subpage('members-directory');					
				elseif(is_page('charity-organization-login')): 
						get_subpage('login');
				elseif(is_page('charity-organization-registration')): 
						get_subpage('registration');
				elseif(is_page('charity-organization-become-a-member')): 
						get_subpage('registration');						
				elseif(is_page('charity-organization-forgot-password')):
						get_subpage('forgot-password');
				elseif(is_page('charity-organization-my-profile')):
						get_subpage('myprofile');
				elseif(is_page('charity-organization-admin')):
						get_subpage('admin');
				elseif(is_page('charity-organization-admin-edit-user')):
						get_subpage('adminedit');
				elseif(is_page('charity-organization-event-registration')):
						get_subpage('eventregistration');
				elseif(is_page('charity-organization-payment-option')):
						get_subpage('paypal');
				elseif(is_page('charity-organization-event-details')):
						get_subpage('eventdetails');
				elseif(is_page('charity-organization-mailing-list')):
						get_subpage('mailinglist');
				elseif(is_page('charity-organization-member-profile')):
						get_subpage('memberdetails');
				elseif(is_page('charity-organization-senior-services')):
						get_subpage('resources_list');
			?>
			
			<?php endif; ?>			
			<?php
			wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) );
			edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
			</div><!-- .entry-content -->
		</div><!-- #post-## -->

<?php endwhile; // end of the loop. ?>
